﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace GumshoeSPLG
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
            versionLabel.Text = @"Version | " + Application.ProductVersion;
            directoryLabel.Text = Properties.Settings.Default.SavePath + @"\GumshoeSPLG\HistoryDB.s3db";
        }

        private void hiddenPannel_MouseHover(object sender, EventArgs e)
        {
            directoryLabel.Visible = true;
            faceLabel.Visible = true;
        }

        private void hiddenPannel_MouseLeave(object sender, EventArgs e)
        {
            directoryLabel.Visible = false;
            faceLabel.Visible = false;
        }

        private void emailLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("mailto:Gumshoe_SPLG,");
        }
    }
}