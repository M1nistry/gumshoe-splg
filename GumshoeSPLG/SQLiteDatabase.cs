﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;

namespace GumshoeSPLG
{
    public class SqLiteDatabase
    {
        private readonly SQLiteConnection _dbConnection;

        /// <summary>
        ///     Default Constructor for SQLiteDatabase Class.
        /// </summary>
        public SqLiteDatabase()
        {
            _dbConnection = new SQLiteConnection("Data Source=\\GumshoeSPLG\\HistoryDB.s3db");
            
        }

        /// <summary>
        ///     Single Param Constructor for specifying the DB file.
        /// </summary>
        /// <param name="inputFile">The File containing the DB</param>
        public SqLiteDatabase(String inputFile)
        {
            _dbConnection = new SQLiteConnection(String.Format(@"Data Source={0}", inputFile))
            {
                ParseViaFramework = true
            };
            SetupTables();
        }

        public bool SetupTables()
        {
            try
            {
                using (var connection = new SQLiteConnection(_dbConnection))
                {
                    const string createQuery =
                        @"CREATE TABLE IF NOT EXISTS tabs ('id' INTEGER PRIMARY KEY, 'name' VARCHAR(40), 'content' TEXT);";
                    connection.Open();
                    using (var cmd = new SQLiteCommand(connection))
                    {
                        cmd.CommandText = createQuery;
                        cmd.ExecuteNonQuery();
                        return true;
                    }
                }
            }
            catch (SQLiteException sqEx)
            {
                return false;
            }
        }

        /// <summary>
        ///     Allows the programmer to run a query against the Database.
        /// </summary>
        /// <param name="sql">The SQL to run</param>
        /// <returns>A DataTable containing the result set.</returns>
        public DataTable GetDataTable(string sql)
        {
            var dt = new DataTable();
            _dbConnection.Open();
            using (var mycommand = new SQLiteCommand(_dbConnection))
            {
                mycommand.CommandText = sql;
                using (SQLiteDataReader reader = mycommand.ExecuteReader())
                {
                    dt.Load(reader);
                    _dbConnection.Close();
                    return dt;
                }
            }
        }

        /// <summary>
        ///     Allows the programmer to interact with the database for purposes other than a query.
        /// </summary>
        /// <param name="sql">The SQL to be run.</param>
        /// <returns>An Integer containing the number of rows updated.</returns>
        public int ExecuteNonQuery(string sql)
        {
            using (var connection = new SQLiteConnection(_dbConnection))
            {
                connection.Open();
                using (var mycommand = new SQLiteCommand(connection))
                {
                    mycommand.CommandText = sql;
                    int rowsUpdated = mycommand.ExecuteNonQuery();
                    return rowsUpdated;
                }
            }
        }

        public bool ColumnExists(string sql)
        {
            _dbConnection.Open();
            using (var cmd = new SQLiteCommand(_dbConnection))
            {
                cmd.CommandText = sql;
                try
                {
                    cmd.ExecuteScalar();
                    _dbConnection.Close();
                    return true;
                }
                catch (Exception)
                {
                    _dbConnection.Close();
                    return false;
                }
            }
        }

        public bool TasExist(string sqlTas)
        {
            _dbConnection.Open();
            using (var mycommand = new SQLiteCommand(_dbConnection))
            {
                mycommand.CommandText = "SELECT count(*) FROM HISTORY WHERE TAS='" + sqlTas + "'";
                bool value = Convert.ToInt32(mycommand.ExecuteScalar()) == 1;
                _dbConnection.Close();
                return value;
            }
        }

        public bool WorkorderExist(string sqlWorkorder)
        {
            _dbConnection.Open();
            using (var mycommand = new SQLiteCommand(_dbConnection))
            {
                mycommand.CommandText = "SELECT count(*) FROM HISTORY WHERE WORKORDER='" + sqlWorkorder + "'";
                bool value = Convert.ToInt32(mycommand.ExecuteScalar()) == 1;
                _dbConnection.Close();
                return value;
            }
        }

        public bool TicketExist(string sqlTicket)
        {
            _dbConnection.Open();
            using (var mycommand = new SQLiteCommand(_dbConnection))
            {
                mycommand.CommandText = "SELECT count(*) FROM SYSLOGS WHERE TICKET='" + sqlTicket + "'";
                bool value = Convert.ToInt32(mycommand.ExecuteScalar()) == 1;
                _dbConnection.Close();
                return value;
            }
        }

        public int SavedToday()
        {
            if (_dbConnection == null) return -1;
            try
            {
                _dbConnection.Open();
                int count = -1;
                using (var mycommand = new SQLiteCommand(_dbConnection))
                {
                    string dbTable = _dbConnection.DataSource == "HistoryDB" ? "HISTORY" : "SYSLOGS";
                    mycommand.CommandText = String.Format("SELECT count(*) FROM {0} WHERE DATE='{1}'"
                        , dbTable, DateTime.Now.ToShortDateString());
                    count = Convert.ToInt32(mycommand.ExecuteScalar());
                    _dbConnection.Close();
                }
                return count;
            }
            catch (InvalidOperationException ex)
            {
                if (ex.Message == @"Operation is not valid due to the current state of the object.")
                {
                    MessageBox.Show(@"Disk Write error, I don't know why this happens.");
                }
            }
            return -1;
        }

        /// <summary>
        ///     Allows the programmer to retrieve single items from the DB.
        /// </summary>
        /// <param name="sql">The query to run.</param>
        /// <returns>A string.</returns>
        public string ExecuteScalar(string sql)
        {
            //if (sql.EndsWith("0")) return "";
            _dbConnection.Open();
            using (var mycommand = new SQLiteCommand(_dbConnection))
            {
                mycommand.CommandText = sql;
                object value = mycommand.ExecuteScalar();
                _dbConnection.Close();
                return value != null ? value.ToString() : "";
            }
        }

        public int table_version()
        {
            _dbConnection.Open();
            using (var mycommand = new SQLiteCommand(_dbConnection))
            {
                mycommand.CommandText = "PRAGMA user_version";
                using (SQLiteDataReader rdr = mycommand.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        int vers = rdr.GetInt32(0);
                        _dbConnection.Close();
                        return vers;
                    }
                }
            }
            return -1;
        }

        /// <summary>
        ///     Allows the programmer to easily update rows in the DB.
        /// </summary>
        /// <param name="tableName">The table to update.</param>
        /// <param name="data">A dictionary containing Column names and their new values.</param>
        /// <param name="where">The where clause for the update statement.</param>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool Update(String tableName, Dictionary<String, String> data, String where)
        {
            string vals = "";
            bool returnCode = true;
            if (data.Count >= 1)
            {
                foreach (var val in data)
                {
                    vals += String.Format(" {0} = '{1}',", val.Key, val.Value);
                }
                vals = vals.Substring(0, vals.Length - 1);
            }
            try
            {
                ExecuteNonQuery(String.Format("update {0} set {1} where {2};", tableName, vals, where));
            }
            catch
            {
                returnCode = false;
            }
            return returnCode;
        }

        /// <summary>
        ///     Allows the programmer to easily delete rows from the DB.
        /// </summary>
        /// <param name="tableName">The table from which to delete.</param>
        /// <param name="where">The where clause for the delete.</param>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool Delete(String tableName, String where)
        {
            bool returnCode = true;
            try
            {
                ExecuteNonQuery(String.Format("delete from {0} where {1};", tableName, where));
            }
            catch (Exception fail)
            {
                MessageBox.Show(fail.Message);
                returnCode = false;
            }
            return returnCode;
        }

        /// <summary>
        ///     Allows the programmer to easily insert into the DB
        /// </summary>
        /// <param name="tableName">The table into which we insert the data.</param>
        /// <param name="data">A dictionary containing the column names and data for the insert.</param>
        /// <returns>A boolean true or false to signify success or failure.</returns>
        public bool Insert(String tableName, Dictionary<String, String> data)
        {
            string columns = "";
            string values = "";
            bool returnCode = true;
            foreach (var val in data)
            {
                columns += String.Format(" {0},", val.Key);
                values += String.Format(" '{0}',", val.Value);
            }
            columns = columns.Substring(0, columns.Length - 1);
            values = values.Substring(0, values.Length - 1);
            try
            {
                ExecuteNonQuery(String.Format("insert into {0}({1}) values({2});", tableName, columns, values));
            }
            catch (Exception fail)
            {
                MessageBox.Show(fail.Message);
                returnCode = false;
            }
            return returnCode;
        }

        public string CreateTab(string tabName, string content = "")
        {
            using (var connection = new SQLiteConnection(_dbConnection))
            {
                try
                {
                    const string createQuery = @"INSERT INTO tabs (name, content) VALUES (@name, @content);";
                    connection.Open();
                    using (var cmd = new SQLiteCommand(createQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("name", tabName);
                        cmd.Parameters.AddWithValue("content", content);
                        cmd.ExecuteNonQuery();
                    }
                    using (var cmd = new SQLiteCommand("SELECT last_insert_rowid()", connection))
                    {
                        var lastId = cmd.ExecuteScalar();
                        return lastId.ToString();
                    }
                }
                catch (SQLiteException sqEx)
                {
                    return "-1";
                }
            }
            
        }

        public void RenameTab(int id, string tabName)
        {
            try
            {
                using (var connection = new SQLiteConnection(_dbConnection))
                {
                    const string createQuery = @"UPDATE tabs SET name=@name WHERE id=@id;";
                    connection.Open();
                    using (var cmd = new SQLiteCommand(createQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("id", id);
                        cmd.Parameters.AddWithValue("name", tabName);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (SQLiteException sqEx)
            {
                Console.WriteLine(@"Error: " + sqEx);
            }
        }

        public void RemoveTab(int id)
        {
            try
            {
                using (var connection = new SQLiteConnection(_dbConnection))
                {
                    const string deleteQuery = @"DELETE FROM tabs WHERE id=@id;";
                    connection.Open();
                    using (var cmd = new SQLiteCommand(deleteQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("id", id);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (SQLiteException sqEx)
            {
                Console.WriteLine(@"Error: " + sqEx);
            }
        }

        public Dictionary<int, KeyValuePair<string, string>> PopulateTabs()
        {
            var tabDictionary = new Dictionary<int, KeyValuePair<string, string>>();
            try
            {
                using (var connection = new SQLiteConnection(_dbConnection))
                {
                    const string tabsQuery = @"SELECT * FROM tabs;";
                    connection.Open();
                    using (var cmd = new SQLiteCommand(tabsQuery, connection))
                    {
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                tabDictionary.Add(reader.GetInt32(0), new KeyValuePair<string, string>(reader.GetString(1), reader.GetString(2)));
                            }
                        }
                        return tabDictionary;
                    }
                }
            }
            catch (SQLiteException sqEx)
            {
                return tabDictionary;
            }
        }

        public void UpdateTab(int id, string content)
        {
            try
            {
                using (var connection = new SQLiteConnection(_dbConnection))
                {
                    const string updateQuery = @"UPDATE tabs SET content=@content WHERE id=@id;";
                    connection.Open();
                    using (var cmd = new SQLiteCommand(updateQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("content", content);
                        cmd.Parameters.AddWithValue("id", id);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (SQLiteException sqEx)
            {
                Console.WriteLine(@"Error: " + sqEx);
            }
        }
    }
}