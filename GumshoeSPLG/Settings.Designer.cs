﻿namespace GumshoeSPLG
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.greetingNameTextBox = new System.Windows.Forms.TextBox();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.greetingLabel = new System.Windows.Forms.Label();
            this.additionalPartLabel = new System.Windows.Forms.Label();
            this.oneLineRadio = new System.Windows.Forms.RadioButton();
            this.threeLineRadio = new System.Windows.Forms.RadioButton();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.bulkDeleteGroup = new System.Windows.Forms.GroupBox();
            this.riskCheckBox = new System.Windows.Forms.CheckBox();
            this.executeButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.sqlColumnLabel = new System.Windows.Forms.Label();
            this.sqlColumnsDropDown = new System.Windows.Forms.ComboBox();
            this.sqlDeleteQueryTextBox = new System.Windows.Forms.TextBox();
            this.hiddenCheck = new System.Windows.Forms.CheckBox();
            this.sysLogsLayoutRadio = new System.Windows.Forms.RadioButton();
            this.callsLayoutRadio = new System.Windows.Forms.RadioButton();
            this.oscCheckBox = new System.Windows.Forms.CheckBox();
            this.pathLabel = new System.Windows.Forms.Label();
            this.pathTextBox = new System.Windows.Forms.TextBox();
            this.browseLinkLabel = new System.Windows.Forms.LinkLabel();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.tabLayouts = new System.Windows.Forms.TabControl();
            this.tabSPLG = new System.Windows.Forms.TabPage();
            this.tabMMG = new System.Windows.Forms.TabPage();
            this.labelLayout = new System.Windows.Forms.Label();
            this.radioGeneral = new System.Windows.Forms.RadioButton();
            this.bulkDeleteGroup.SuspendLayout();
            this.tabLayouts.SuspendLayout();
            this.tabSPLG.SuspendLayout();
            this.tabMMG.SuspendLayout();
            this.SuspendLayout();
            // 
            // greetingNameTextBox
            // 
            this.greetingNameTextBox.Location = new System.Drawing.Point(97, 12);
            this.greetingNameTextBox.Name = "greetingNameTextBox";
            this.greetingNameTextBox.Size = new System.Drawing.Size(164, 20);
            this.greetingNameTextBox.TabIndex = 1;
            this.toolTip.SetToolTip(this.greetingNameTextBox, "The name displayed at the bottom when you open the program");
            // 
            // okButton
            // 
            this.okButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.okButton.Location = new System.Drawing.Point(50, 216);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 2;
            this.okButton.Text = "Save";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cancelButton.Location = new System.Drawing.Point(156, 216);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // greetingLabel
            // 
            this.greetingLabel.AutoSize = true;
            this.greetingLabel.Location = new System.Drawing.Point(12, 15);
            this.greetingLabel.Name = "greetingLabel";
            this.greetingLabel.Size = new System.Drawing.Size(79, 13);
            this.greetingLabel.TabIndex = 4;
            this.greetingLabel.Text = "Greeting name:";
            this.toolTip.SetToolTip(this.greetingLabel, "The name displayed at the bottom when you open the program");
            // 
            // additionalPartLabel
            // 
            this.additionalPartLabel.AutoSize = true;
            this.additionalPartLabel.Location = new System.Drawing.Point(12, 48);
            this.additionalPartLabel.Name = "additionalPartLabel";
            this.additionalPartLabel.Size = new System.Drawing.Size(113, 13);
            this.additionalPartLabel.TabIndex = 5;
            this.additionalPartLabel.Text = "Additional Part Layout:";
            this.toolTip.SetToolTip(this.additionalPartLabel, "Determine the layout when you add an additional spare part");
            // 
            // oneLineRadio
            // 
            this.oneLineRadio.AutoSize = true;
            this.oneLineRadio.Location = new System.Drawing.Point(134, 46);
            this.oneLineRadio.Name = "oneLineRadio";
            this.oneLineRadio.Size = new System.Drawing.Size(68, 17);
            this.oneLineRadio.TabIndex = 6;
            this.oneLineRadio.Text = "One Line";
            this.toolTip.SetToolTip(this.oneLineRadio, "Faulty 2   MPN:   Spare:");
            this.oneLineRadio.UseVisualStyleBackColor = true;
            // 
            // threeLineRadio
            // 
            this.threeLineRadio.AutoSize = true;
            this.threeLineRadio.Checked = true;
            this.threeLineRadio.Location = new System.Drawing.Point(134, 69);
            this.threeLineRadio.Name = "threeLineRadio";
            this.threeLineRadio.Size = new System.Drawing.Size(81, 17);
            this.threeLineRadio.TabIndex = 7;
            this.threeLineRadio.TabStop = true;
            this.threeLineRadio.Text = "Three Lines";
            this.toolTip.SetToolTip(this.threeLineRadio, "Fault 2\r\nMPN: \r\nSpare: ");
            this.threeLineRadio.UseVisualStyleBackColor = true;
            // 
            // bulkDeleteGroup
            // 
            this.bulkDeleteGroup.Controls.Add(this.riskCheckBox);
            this.bulkDeleteGroup.Controls.Add(this.executeButton);
            this.bulkDeleteGroup.Controls.Add(this.label1);
            this.bulkDeleteGroup.Controls.Add(this.sqlColumnLabel);
            this.bulkDeleteGroup.Controls.Add(this.sqlColumnsDropDown);
            this.bulkDeleteGroup.Controls.Add(this.sqlDeleteQueryTextBox);
            this.bulkDeleteGroup.Location = new System.Drawing.Point(12, 221);
            this.bulkDeleteGroup.Name = "bulkDeleteGroup";
            this.bulkDeleteGroup.Size = new System.Drawing.Size(249, 110);
            this.bulkDeleteGroup.TabIndex = 11;
            this.bulkDeleteGroup.TabStop = false;
            this.bulkDeleteGroup.Text = "Bulk Delete";
            this.toolTip.SetToolTip(this.bulkDeleteGroup, "Have caution whilst using this");
            this.bulkDeleteGroup.Visible = false;
            // 
            // riskCheckBox
            // 
            this.riskCheckBox.AutoSize = true;
            this.riskCheckBox.Location = new System.Drawing.Point(13, 81);
            this.riskCheckBox.Name = "riskCheckBox";
            this.riskCheckBox.Size = new System.Drawing.Size(133, 17);
            this.riskCheckBox.TabIndex = 14;
            this.riskCheckBox.Text = "I acknowledge the risk";
            this.toolTip.SetToolTip(this.riskCheckBox, "Check this to enable the execute button");
            this.riskCheckBox.UseVisualStyleBackColor = true;
            this.riskCheckBox.CheckedChanged += new System.EventHandler(this.riskCheckBox_CheckedChanged);
            // 
            // executeButton
            // 
            this.executeButton.Enabled = false;
            this.executeButton.Location = new System.Drawing.Point(159, 77);
            this.executeButton.Name = "executeButton";
            this.executeButton.Size = new System.Drawing.Size(75, 23);
            this.executeButton.TabIndex = 13;
            this.executeButton.Text = "Execute";
            this.executeButton.UseVisualStyleBackColor = true;
            this.executeButton.Click += new System.EventHandler(this.executeButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "SQL Value:";
            // 
            // sqlColumnLabel
            // 
            this.sqlColumnLabel.AutoSize = true;
            this.sqlColumnLabel.Location = new System.Drawing.Point(10, 22);
            this.sqlColumnLabel.Name = "sqlColumnLabel";
            this.sqlColumnLabel.Size = new System.Drawing.Size(69, 13);
            this.sqlColumnLabel.TabIndex = 11;
            this.sqlColumnLabel.Text = "SQL Column:";
            // 
            // sqlColumnsDropDown
            // 
            this.sqlColumnsDropDown.FormattingEnabled = true;
            this.sqlColumnsDropDown.Items.AddRange(new object[] {
            "TAS",
            "WORKORDER",
            "CT",
            "NODE",
            "PUDO",
            "FAULTYMPN",
            "FAULTYSERIAL",
            "SPAREMPN",
            "SPARESERIAL",
            "COURIER",
            "CONNOTE"});
            this.sqlColumnsDropDown.Location = new System.Drawing.Point(85, 19);
            this.sqlColumnsDropDown.Name = "sqlColumnsDropDown";
            this.sqlColumnsDropDown.Size = new System.Drawing.Size(149, 21);
            this.sqlColumnsDropDown.TabIndex = 9;
            // 
            // sqlDeleteQueryTextBox
            // 
            this.sqlDeleteQueryTextBox.Location = new System.Drawing.Point(85, 46);
            this.sqlDeleteQueryTextBox.Name = "sqlDeleteQueryTextBox";
            this.sqlDeleteQueryTextBox.Size = new System.Drawing.Size(149, 20);
            this.sqlDeleteQueryTextBox.TabIndex = 10;
            // 
            // hiddenCheck
            // 
            this.hiddenCheck.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.hiddenCheck.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.hiddenCheck.Location = new System.Drawing.Point(134, 222);
            this.hiddenCheck.Name = "hiddenCheck";
            this.hiddenCheck.Size = new System.Drawing.Size(13, 13);
            this.hiddenCheck.TabIndex = 12;
            this.toolTip.SetToolTip(this.hiddenCheck, "These are not the droids you\'re looking for...");
            this.hiddenCheck.UseVisualStyleBackColor = true;
            this.hiddenCheck.CheckedChanged += new System.EventHandler(this.hiddenCheck_CheckedChanged);
            this.hiddenCheck.KeyDown += new System.Windows.Forms.KeyEventHandler(this.hiddenCheck_KeyDown);
            // 
            // sysLogsLayoutRadio
            // 
            this.sysLogsLayoutRadio.AutoSize = true;
            this.sysLogsLayoutRadio.Location = new System.Drawing.Point(6, 26);
            this.sysLogsLayoutRadio.Name = "sysLogsLayoutRadio";
            this.sysLogsLayoutRadio.Size = new System.Drawing.Size(85, 17);
            this.sysLogsLayoutRadio.TabIndex = 1;
            this.sysLogsLayoutRadio.Text = "System Logs";
            this.sysLogsLayoutRadio.UseVisualStyleBackColor = true;
            this.sysLogsLayoutRadio.CheckedChanged += new System.EventHandler(this.sysLogsLayoutRadio_CheckedChanged);
            // 
            // callsLayoutRadio
            // 
            this.callsLayoutRadio.AutoSize = true;
            this.callsLayoutRadio.Checked = true;
            this.callsLayoutRadio.Location = new System.Drawing.Point(6, 3);
            this.callsLayoutRadio.Name = "callsLayoutRadio";
            this.callsLayoutRadio.Size = new System.Drawing.Size(47, 17);
            this.callsLayoutRadio.TabIndex = 0;
            this.callsLayoutRadio.TabStop = true;
            this.callsLayoutRadio.Text = "Calls";
            this.callsLayoutRadio.UseVisualStyleBackColor = true;
            this.callsLayoutRadio.CheckedChanged += new System.EventHandler(this.callsLayoutRadio_CheckedChanged);
            // 
            // oscCheckBox
            // 
            this.oscCheckBox.AutoSize = true;
            this.oscCheckBox.Location = new System.Drawing.Point(17, 185);
            this.oscCheckBox.Name = "oscCheckBox";
            this.oscCheckBox.Size = new System.Drawing.Size(48, 17);
            this.oscCheckBox.TabIndex = 14;
            this.oscCheckBox.Text = "OSC";
            this.oscCheckBox.UseVisualStyleBackColor = true;
            this.oscCheckBox.CheckedChanged += new System.EventHandler(this.oscCheckBox_CheckedChanged);
            // 
            // pathLabel
            // 
            this.pathLabel.AutoSize = true;
            this.pathLabel.Enabled = false;
            this.pathLabel.Location = new System.Drawing.Point(133, 186);
            this.pathLabel.Name = "pathLabel";
            this.pathLabel.Size = new System.Drawing.Size(32, 13);
            this.pathLabel.TabIndex = 15;
            this.pathLabel.Text = "Path:";
            // 
            // pathTextBox
            // 
            this.pathTextBox.Enabled = false;
            this.pathTextBox.Location = new System.Drawing.Point(171, 183);
            this.pathTextBox.Name = "pathTextBox";
            this.pathTextBox.Size = new System.Drawing.Size(75, 20);
            this.pathTextBox.TabIndex = 16;
            this.pathTextBox.Text = "U:\\";
            // 
            // browseLinkLabel
            // 
            this.browseLinkLabel.AutoSize = true;
            this.browseLinkLabel.Enabled = false;
            this.browseLinkLabel.Location = new System.Drawing.Point(248, 186);
            this.browseLinkLabel.Name = "browseLinkLabel";
            this.browseLinkLabel.Size = new System.Drawing.Size(16, 13);
            this.browseLinkLabel.TabIndex = 17;
            this.browseLinkLabel.TabStop = true;
            this.browseLinkLabel.Text = "...";
            this.browseLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.browseLinkLabel_LinkClicked);
            // 
            // tabLayouts
            // 
            this.tabLayouts.Controls.Add(this.tabSPLG);
            this.tabLayouts.Controls.Add(this.tabMMG);
            this.tabLayouts.Location = new System.Drawing.Point(122, 103);
            this.tabLayouts.Name = "tabLayouts";
            this.tabLayouts.SelectedIndex = 0;
            this.tabLayouts.Size = new System.Drawing.Size(139, 74);
            this.tabLayouts.TabIndex = 2;
            // 
            // tabSPLG
            // 
            this.tabSPLG.Controls.Add(this.sysLogsLayoutRadio);
            this.tabSPLG.Controls.Add(this.callsLayoutRadio);
            this.tabSPLG.Location = new System.Drawing.Point(4, 22);
            this.tabSPLG.Name = "tabSPLG";
            this.tabSPLG.Padding = new System.Windows.Forms.Padding(3);
            this.tabSPLG.Size = new System.Drawing.Size(131, 48);
            this.tabSPLG.TabIndex = 0;
            this.tabSPLG.Text = "SPLG";
            this.tabSPLG.UseVisualStyleBackColor = true;
            // 
            // tabMMG
            // 
            this.tabMMG.Controls.Add(this.radioGeneral);
            this.tabMMG.Location = new System.Drawing.Point(4, 22);
            this.tabMMG.Name = "tabMMG";
            this.tabMMG.Padding = new System.Windows.Forms.Padding(3);
            this.tabMMG.Size = new System.Drawing.Size(131, 48);
            this.tabMMG.TabIndex = 1;
            this.tabMMG.Text = "MMG";
            this.tabMMG.UseVisualStyleBackColor = true;
            // 
            // labelLayout
            // 
            this.labelLayout.AutoSize = true;
            this.labelLayout.Location = new System.Drawing.Point(12, 103);
            this.labelLayout.Name = "labelLayout";
            this.labelLayout.Size = new System.Drawing.Size(39, 13);
            this.labelLayout.TabIndex = 18;
            this.labelLayout.Text = "Layout";
            // 
            // radioGeneral
            // 
            this.radioGeneral.AutoSize = true;
            this.radioGeneral.Location = new System.Drawing.Point(6, 6);
            this.radioGeneral.Name = "radioGeneral";
            this.radioGeneral.Size = new System.Drawing.Size(62, 17);
            this.radioGeneral.TabIndex = 0;
            this.radioGeneral.TabStop = true;
            this.radioGeneral.Text = "General";
            this.radioGeneral.UseVisualStyleBackColor = true;
            this.radioGeneral.CheckedChanged += new System.EventHandler(this.radioGeneral_CheckedChanged);
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(273, 251);
            this.Controls.Add(this.labelLayout);
            this.Controls.Add(this.tabLayouts);
            this.Controls.Add(this.browseLinkLabel);
            this.Controls.Add(this.pathTextBox);
            this.Controls.Add(this.pathLabel);
            this.Controls.Add(this.oscCheckBox);
            this.Controls.Add(this.hiddenCheck);
            this.Controls.Add(this.threeLineRadio);
            this.Controls.Add(this.oneLineRadio);
            this.Controls.Add(this.additionalPartLabel);
            this.Controls.Add(this.greetingLabel);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.greetingNameTextBox);
            this.Controls.Add(this.bulkDeleteGroup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.bulkDeleteGroup.ResumeLayout(false);
            this.bulkDeleteGroup.PerformLayout();
            this.tabLayouts.ResumeLayout(false);
            this.tabSPLG.ResumeLayout(false);
            this.tabSPLG.PerformLayout();
            this.tabMMG.ResumeLayout(false);
            this.tabMMG.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox greetingNameTextBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label greetingLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Label additionalPartLabel;
        private System.Windows.Forms.RadioButton oneLineRadio;
        private System.Windows.Forms.RadioButton threeLineRadio;
        private System.Windows.Forms.ComboBox sqlColumnsDropDown;
        private System.Windows.Forms.TextBox sqlDeleteQueryTextBox;
        private System.Windows.Forms.GroupBox bulkDeleteGroup;
        private System.Windows.Forms.CheckBox riskCheckBox;
        private System.Windows.Forms.Button executeButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label sqlColumnLabel;
        private System.Windows.Forms.CheckBox hiddenCheck;
        private System.Windows.Forms.RadioButton sysLogsLayoutRadio;
        private System.Windows.Forms.RadioButton callsLayoutRadio;
        private System.Windows.Forms.CheckBox oscCheckBox;
        private System.Windows.Forms.Label pathLabel;
        private System.Windows.Forms.TextBox pathTextBox;
        private System.Windows.Forms.LinkLabel browseLinkLabel;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.TabControl tabLayouts;
        private System.Windows.Forms.TabPage tabSPLG;
        private System.Windows.Forms.TabPage tabMMG;
        private System.Windows.Forms.Label labelLayout;
        private System.Windows.Forms.RadioButton radioGeneral;
    }
}