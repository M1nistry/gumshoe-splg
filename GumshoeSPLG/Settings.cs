﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace GumshoeSPLG
{
    public partial class Settings : Form
    {
        private readonly Main _mainForm;

        public Settings(Main mf)
        {
            InitializeComponent();
            _mainForm = mf;
            greetingNameTextBox.Text = Properties.Settings.Default.greetingName == ""
                ? Environment.UserName
                : Properties.Settings.Default.greetingName;
            Icon = mf.Icon;
            callsLayoutRadio.Checked = Properties.Settings.Default.layout == 1;
            sysLogsLayoutRadio.Checked = Properties.Settings.Default.layout == 2;
            oscCheckBox.Checked = Properties.Settings.Default.oscEnabled;
            pathTextBox.Text = Properties.Settings.Default.oscPath;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.greetingName = greetingNameTextBox.Text;
            Properties.Settings.Default.additionalLines = oneLineRadio.Checked ? 1 : 3;
            Properties.Settings.Default.layout = callsLayoutRadio.Checked ? 1 : 2;
            if (oscCheckBox.Checked)
            {
                Properties.Settings.Default.oscPath = pathTextBox.Text;
                _mainForm.CheckFile();
                _mainForm.SetPath();
            }
            Properties.Settings.Default.Save();
            _mainForm.ChangeLayout(Properties.Settings.Default.layout);
            Dispose();
        }

        private void riskCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            executeButton.Enabled = riskCheckBox.Checked;
        }

        private void executeButton_Click(object sender, EventArgs e)
        {
            var confirm = MessageBox.Show(
                @"This can potentially remove data from your history that you may not want to remove, are you sure you want to execute the query?",
                @"Caution: Danger ahead!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Hand) == DialogResult.Yes;
            if (!confirm) return;
            try
            {
                _mainForm.Sql.Delete(sqlColumnsDropDown.Text, sqlDeleteQueryTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"An exception occured while deleting from the Database " + ex.Message);
            }
        }

        private void hiddenCheck_KeyDown(object sender, KeyEventArgs e)
        {
            if (!hiddenCheck.Checked) return;
            if (e.Modifiers != Keys.Control || e.KeyCode != Keys.F8) return;
            bulkDeleteGroup.Visible = true;
            Size = new Size(289, 408);
        }

        private void hiddenCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (hiddenCheck.Checked) return;
            bulkDeleteGroup.Visible = false;
            Size = new Size(289, 285);
        }

        private void callsLayoutRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (callsLayoutRadio.Checked)
            {
                UncheckLayouts(tabSPLG);
                Properties.Settings.Default.layout = 1;
            }
            Properties.Settings.Default.Save();
        }

        private void sysLogsLayoutRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (sysLogsLayoutRadio.Checked)
            {
                UncheckLayouts(tabSPLG);
                Properties.Settings.Default.layout = 2;
            }
            Properties.Settings.Default.Save();
        }

        private void browseLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            folderBrowserDialog.ShowDialog();
            pathTextBox.Text = folderBrowserDialog.SelectedPath;
        }

        private void oscCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (oscCheckBox.Checked)
            {
                pathLabel.Enabled = true;
                pathTextBox.Enabled = true;
                browseLinkLabel.Enabled = true;
            }
            else
            {
                pathLabel.Enabled = false;
                pathTextBox.Enabled = false;
                browseLinkLabel.Enabled = false;
            }
            Properties.Settings.Default.oscEnabled = oscCheckBox.Checked;
        }
        
        /// <summary> Unchecks all the radio buttons on each tab page except the active one </summary>
        /// <param name="activePage"> The active tab page when the radio button was clicked</param>
        private void UncheckLayouts(TabPage activePage)
        {
            foreach (TabPage tabPage in tabLayouts.TabPages)
            {
                if (tabPage == activePage) continue;
                foreach (RadioButton rb in tabPage.Controls)
                {
                    rb.Checked = false;
                }
            }
        }

        private void radioGeneral_CheckedChanged(object sender, EventArgs e)
        {
            if (radioGeneral.Checked)
            {
                UncheckLayouts(tabMMG);
            }
        }
    }
}