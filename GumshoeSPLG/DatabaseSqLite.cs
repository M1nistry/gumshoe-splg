﻿using System.Data.SQLite;
using System.IO;

namespace GumshoeSPLG
{
    public class DatabaseSqLite
    {
        private readonly Main _main;

        private readonly SQLiteConnectionStringBuilder _connection;
        private readonly string _contring;

        /// <summary> Initilizes the database before attempting to create it </summary>
        /// <param name="dbLocation"> Relative database location based on machine setup </param>
        public DatabaseSqLite(string dbLocation)
        {
            _connection = new SQLiteConnectionStringBuilder
            {
                DataSource = dbLocation + @"\HistoryDB.sqlite"
            };
            _contring = _connection.ToString();
            _main = Main.GetSingleton();
            SetupDb();
        }

        /// <summary> Initilizes the database and all it's fields</summary>
        private void SetupDb()
        {
            if (!File.Exists(_connection.DataSource))
            {
                Directory.CreateDirectory(_connection.DataSource);
                SQLiteConnection.CreateFile(@"HistoryDB.sqlite");
            }
            try
            {
                using (var connection = new SQLiteConnection(_contring))
                {
                    var createQuery = @"CREATE TABLE IF NOT EXISTS `splg` (`id` INT PRIMARY KEY AUTOINCREMENT, `ticket` TEXT, " +
                                      @"`workorder` TEXT, `tow` TEXT, `ags` TEXT, `fault_serial` TEXT, `fault_mpn` TEXT, " +
                                      @"`spare_serial` TEXT, `spare_mpn` TEXT, `fault_node` TEXT, `pudo` TEXT, `notes` TEXT, " +
                                      @"`connote` TEXT, `rtc` INT, `system` TEXT, `create_datetime` DATETIME, `updated_datetime` DATETIME);";
                    connection.Open();
                }
            }
            catch (SQLiteException ex)
            {
                _main.extendedStatusStrip.AddStatus(@"Failed while initilizing local database.");
                _main.ErrorOutput(ex.Message, "SetupDb", ex.Source);
            }
        }
    }
}
