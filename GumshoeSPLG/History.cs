﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace GumshoeSPLG
{
    public partial class History : Form
    {
        private readonly Main _mainForm;
        private readonly int layout;
        public SqLiteDatabase SqL;
        private BindingSource _bs;

        public History(Main main)
        {
            InitializeComponent();
            _mainForm = main;
            layout = main.Layout;
            PopulateTable();
            toolTip.SetToolTip(this, "Entry Count: " + historyGrid.RowCount);
            Icon = main.Icon;
            historyGrid.Visible = layout == 1;
            historySysLogGrid.Visible = layout == 2;
            if (historyGrid.RowCount > 40)
            {
                panelSurprise.Visible = true;
            }
        }

        //Opens the expanded history view
        private void historyGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            Common.rowID = Convert.ToInt32(historyGrid.Rows[e.RowIndex].Cells["ID"].Value);
            Common.sqlID = e.RowIndex;
            _mainForm.PopulateCall();
            Dispose(true);
        }

        private void historySysLogGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            Common.rowID = Convert.ToInt32(historySysLogGrid.Rows[e.RowIndex].Cells[12].Value);
            Common.sqlID = e.RowIndex;
            _mainForm.PopulateSysLog();
            Dispose(true);
        }

        //Method for right clicking cells
        private void historyGrid_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (!e.Button.Equals(MouseButtons.Right) || e.RowIndex < 0) return;
            Common.rowID = Convert.ToInt32(historyGrid.Rows[e.RowIndex].Cells["ID"].Value);
            Common.sqlID = e.RowIndex;
            contextMenu.Show(MousePosition.X, MousePosition.Y);
        }


        private void historySysLogGrid_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (!e.Button.Equals(MouseButtons.Right) || e.RowIndex < 0) return;
            Common.rowID = Convert.ToInt32(historySysLogGrid.Rows[e.RowIndex].Cells[12].Value);
            Common.sqlID = e.RowIndex;
            contextMenu.Show(MousePosition.X, MousePosition.Y);
        }

        //Resets the grid
        public void NewRefresh()
        {
            historyGrid.DataSource = null;
            historySysLogGrid.DataSource = null;
            PopulateTable();
        }

        //Populates the History table with information from the SQL database
        public void PopulateTable()
        {
            switch (_mainForm.Layout)
            {
                case (1):
                    SqL = new SqLiteDatabase(Properties.Settings.Default.SavePath + @"\GumshoeSPLG\HistoryDB.s3db");
                    _bs = new BindingSource();
                    try
                    {
                        const string query = "SELECT SYSTEM, TAS, WORKORDER, CT, NODE, PUDO," +
                                             "FAULTYMPN, FAULTYSERIAL, SPAREMPN, SPARESERIAL, " +
                                             "COURIER, CONNOTE, SUMMARY, TIME, DATE, ID, RTC FROM HISTORY";
                        var recipe = SqL.GetDataTable(query);

                        _bs.DataSource = recipe;
                        historyGrid.DataSource = _bs;
                        historyGrid.Sort(historyGrid.Columns["ID"], ListSortDirection.Descending);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(@"The following error has occurred: " + ex, @"Error!", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        Close();
                    }
                    break;
                case (2):
                    SqL = new SqLiteDatabase(Properties.Settings.Default.SavePath + @"\GumshoeSPLG\SystemLogsDB.s3db");
                    _bs = new BindingSource();
                    try
                    {
                        const string query = "SELECT TICKET, WORKORDER, ERROR, USERAGS, USERNAME, CTAGS, CTNAME, " +
                                             "RTC, TASKCATEGORY, NOTES, DATE, TIME, ID FROM SYSLOGS";
                        var datatable = SqL.GetDataTable(query);
                        _bs.DataSource = datatable;
                        historySysLogGrid.DataSource = _bs;
                        historySysLogGrid.Sort(historySysLogGrid.Columns[12], ListSortDirection.Descending);
                        
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(@"The following error has occurred: " + ex.Message, @"Error!",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Close();
                        throw;
                    }
                    break;
            }
            Text = @"History | " + historyGrid.RowCount + @" saved tickets | " + SqL.SavedToday() + @" saved today";
        }

        //Filter table by date selected
        private void dateFilter_ValueChanged(object sender, EventArgs e)
        {
            PopulateTable();
            _bs.Filter = "DATE = '" + dateFilter.Value.ToShortDateString() + "'";
            if (layout == 1) historyGrid.DataSource = _bs;
            if (layout == 2) historySysLogGrid.DataSource = _bs;
            toolTip.SetToolTip(this, "Entry Count: " + historyGrid.RowCount);
        }

        //Filter table by text entered
        private void filterBox_TextChanged(object sender, EventArgs e)
        {
            if (filterBox.Text.Contains("'"))
            {
                filterBox.Text = filterBox.Text.Replace("'", "`");
                filterBox.Select(filterBox.TextLength, 0);
            }
            PopulateTable();
            if (layout == 1)
            {
                _bs.Filter = "TAS like '%" + filterBox.Text +
                             "%' OR WORKORDER like '%" + filterBox.Text +
                             "%' OR CT like '%" + filterBox.Text +
                             "%' OR FAULTYSERIAL like '%" + filterBox.Text +
                             "%' OR SPARESERIAL like '%" + filterBox.Text +
                             "%' OR FAULTYMPN like '%" + filterBox.Text +
                             "%' OR SPAREMPN like '%" + filterBox.Text +
                             "%' OR NODE like '%" + filterBox.Text +
                             "%' OR PUDO like '%" + filterBox.Text +
                             "%' OR CONNOTE like '%" + filterBox.Text +
                             "%' OR RTC like '%" + filterBox.Text +
                             "%' OR SUMMARY like '%" + filterBox.Text + "%'";
                historyGrid.DataSource = _bs;
            }
            if (layout == 2)
            {
                _bs.Filter = "TICKET like '%" + filterBox.Text +
                             "%' OR WORKORDER like '%" + filterBox.Text +
                             "%' OR ERROR like '%" + filterBox.Text +
                             "%' OR USERAGS like '%" + filterBox.Text +
                             "%' OR USERNAME like '%" + filterBox.Text +
                             "%' OR CTAGS like '%" + filterBox.Text +
                             "%' OR CTNAME like '%" + filterBox.Text +
                             "%' OR RTC like '%" + filterBox.Text +
                             "%' OR TASKCATEGORY like '%" + filterBox.Text +
                             "%' OR NOTES like '%" + filterBox.Text + "%'";
                historySysLogGrid.DataSource = _bs;
            }
            toolTip.SetToolTip(this, "Entry Count: " + historyGrid.RowCount);
        }

        //Reset history grid to display all
        private void resetLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PopulateTable();
        }

        //View context menu click method - enables edit
        private void View_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Common.rowID) <= -1) return;
            if (layout == 1) _mainForm.PopulateCall();
            if (layout == 2) _mainForm.PopulateSysLog();
            Dispose(true);
            Close();
        }

        //Delete context menu click method
        private void Delete_Click(object sender, EventArgs e)
        {
            if (layout == 1)
            {
                var db = new SqLiteDatabase(Properties.Settings.Default.SavePath + @"\GumshoeSPLG\HistoryDB.s3db");
                if (
                    MessageBox.Show(@"Are you sure you want to delete this entry?", @"Confirm", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Information) != DialogResult.Yes) return;
                if (!db.Delete("HISTORY", "ID =" + Common.rowID))
                {
                    MessageBox.Show(@"Failed to delete this entry, captain!");
                    return;
                }
            }
            if (layout == 2)
            {
                var db = new SqLiteDatabase(Properties.Settings.Default.SavePath + @"\GumshoeSPLG\SystemLogsDB.s3db");
                if (
                    MessageBox.Show(@"Are you sure you want to delete this entry?", @"Confirm", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Information) != DialogResult.Yes) return;
                if (!db.Delete("SYSLOGS", "ID =" + Common.rowID))
                {
                    MessageBox.Show(@"Failed to delete this entry, captain!");
                    return;
                }
            }
            PopulateTable();
        }

        private void filterBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (Char) Keys.OemQuotes) return;
            filterBox.Text = filterBox.Text.Replace("'", "`");
        }

        private void panelSurprise_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"Hello World!");
        }
    }
}