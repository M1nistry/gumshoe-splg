﻿namespace GumshoeSPLG
{
    partial class History
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.historyGrid = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Node = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pudo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FaultyS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FaultyMPN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SpareS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SpareMPN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RTC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Courier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Connote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Summary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.filterBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dateFilter = new System.Windows.Forms.DateTimePicker();
            this.resetLabel = new System.Windows.Forms.LinkLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.historySysLogGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.View = new System.Windows.Forms.ToolStripMenuItem();
            this.Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.panelSurprise = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.historyGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.historySysLogGrid)).BeginInit();
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // historyGrid
            // 
            this.historyGrid.AllowUserToAddRows = false;
            this.historyGrid.AllowUserToDeleteRows = false;
            this.historyGrid.AllowUserToOrderColumns = true;
            this.historyGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.historyGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this.historyGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.historyGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.historyGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.TYPE,
            this.TAS,
            this.workOrder,
            this.CT,
            this.Node,
            this.Pudo,
            this.FaultyS,
            this.FaultyMPN,
            this.SpareS,
            this.SpareMPN,
            this.RTC,
            this.Courier,
            this.Connote,
            this.Summary,
            this.Time,
            this.Date});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.MenuHighlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.historyGrid.DefaultCellStyle = dataGridViewCellStyle14;
            this.historyGrid.GridColor = System.Drawing.SystemColors.ControlLight;
            this.historyGrid.Location = new System.Drawing.Point(1, 37);
            this.historyGrid.Name = "historyGrid";
            this.historyGrid.ReadOnly = true;
            this.historyGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.historyGrid.RowHeadersVisible = false;
            this.historyGrid.Size = new System.Drawing.Size(528, 233);
            this.historyGrid.TabIndex = 0;
            this.toolTip.SetToolTip(this.historyGrid, "Entry count:");
            this.historyGrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.historyGrid_CellDoubleClick);
            this.historyGrid.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.historyGrid_CellMouseClick);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            this.ID.DefaultCellStyle = dataGridViewCellStyle13;
            this.ID.HeaderText = "ID";
            this.ID.MaxInputLength = 20;
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ID.ToolTipText = "ID within the database";
            this.ID.Visible = false;
            this.ID.Width = 35;
            // 
            // TYPE
            // 
            this.TYPE.DataPropertyName = "SYSTEM";
            this.TYPE.HeaderText = "Ticket";
            this.TYPE.MaxInputLength = 10;
            this.TYPE.Name = "TYPE";
            this.TYPE.ReadOnly = true;
            this.TYPE.Width = 50;
            // 
            // TAS
            // 
            this.TAS.DataPropertyName = "TAS";
            this.TAS.HeaderText = "TAS";
            this.TAS.MaxInputLength = 25;
            this.TAS.Name = "TAS";
            this.TAS.ReadOnly = true;
            this.TAS.Width = 125;
            // 
            // workOrder
            // 
            this.workOrder.DataPropertyName = "WORKORDER";
            this.workOrder.HeaderText = "Work Order";
            this.workOrder.MaxInputLength = 20;
            this.workOrder.Name = "workOrder";
            this.workOrder.ReadOnly = true;
            // 
            // CT
            // 
            this.CT.DataPropertyName = "CT";
            this.CT.HeaderText = "CT";
            this.CT.MaxInputLength = 20;
            this.CT.Name = "CT";
            this.CT.ReadOnly = true;
            // 
            // Node
            // 
            this.Node.DataPropertyName = "NODE";
            this.Node.HeaderText = "Node";
            this.Node.MaxInputLength = 4;
            this.Node.Name = "Node";
            this.Node.ReadOnly = true;
            // 
            // Pudo
            // 
            this.Pudo.DataPropertyName = "PUDO";
            this.Pudo.HeaderText = "PuDo";
            this.Pudo.MaxInputLength = 4;
            this.Pudo.Name = "Pudo";
            this.Pudo.ReadOnly = true;
            // 
            // FaultyS
            // 
            this.FaultyS.DataPropertyName = "FAULTYSERIAL";
            this.FaultyS.HeaderText = "Faulty Serial";
            this.FaultyS.MaxInputLength = 50;
            this.FaultyS.Name = "FaultyS";
            this.FaultyS.ReadOnly = true;
            // 
            // FaultyMPN
            // 
            this.FaultyMPN.DataPropertyName = "FAULTYMPN";
            this.FaultyMPN.HeaderText = "Faulty MPN";
            this.FaultyMPN.MaxInputLength = 50;
            this.FaultyMPN.Name = "FaultyMPN";
            this.FaultyMPN.ReadOnly = true;
            // 
            // SpareS
            // 
            this.SpareS.DataPropertyName = "SPARESERIAL";
            this.SpareS.HeaderText = "Spare Serial";
            this.SpareS.MaxInputLength = 50;
            this.SpareS.Name = "SpareS";
            this.SpareS.ReadOnly = true;
            // 
            // SpareMPN
            // 
            this.SpareMPN.DataPropertyName = "SPAREMPN";
            this.SpareMPN.HeaderText = "Spare MPN";
            this.SpareMPN.MaxInputLength = 50;
            this.SpareMPN.Name = "SpareMPN";
            this.SpareMPN.ReadOnly = true;
            // 
            // RTC
            // 
            this.RTC.DataPropertyName = "RTC";
            this.RTC.HeaderText = "RTC";
            this.RTC.MaxInputLength = 40;
            this.RTC.Name = "RTC";
            this.RTC.ReadOnly = true;
            this.RTC.ToolTipText = "RTC Item Number";
            // 
            // Courier
            // 
            this.Courier.DataPropertyName = "COURIER";
            this.Courier.HeaderText = "Courier";
            this.Courier.MaxInputLength = 10;
            this.Courier.Name = "Courier";
            this.Courier.ReadOnly = true;
            // 
            // Connote
            // 
            this.Connote.DataPropertyName = "CONNOTE";
            this.Connote.HeaderText = "Connote";
            this.Connote.MaxInputLength = 20;
            this.Connote.Name = "Connote";
            this.Connote.ReadOnly = true;
            // 
            // Summary
            // 
            this.Summary.DataPropertyName = "SUMMARY";
            this.Summary.HeaderText = "Summary";
            this.Summary.MaxInputLength = 500;
            this.Summary.Name = "Summary";
            this.Summary.ReadOnly = true;
            // 
            // Time
            // 
            this.Time.DataPropertyName = "TIME";
            this.Time.HeaderText = "Time";
            this.Time.MaxInputLength = 50;
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            // 
            // Date
            // 
            this.Date.DataPropertyName = "DATE";
            this.Date.HeaderText = "Date";
            this.Date.MaxInputLength = 50;
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Filter:";
            // 
            // filterBox
            // 
            this.filterBox.Location = new System.Drawing.Point(50, 10);
            this.filterBox.MaxLength = 50;
            this.filterBox.Name = "filterBox";
            this.filterBox.Size = new System.Drawing.Size(151, 20);
            this.filterBox.TabIndex = 2;
            this.toolTip.SetToolTip(this.filterBox, "Filter table by TAS, Workorder or AGS");
            this.filterBox.TextChanged += new System.EventHandler(this.filterBox_TextChanged);
            this.filterBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.filterBox_KeyPress);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(215, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Date:";
            // 
            // dateFilter
            // 
            this.dateFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateFilter.Location = new System.Drawing.Point(254, 10);
            this.dateFilter.MinDate = new System.DateTime(2013, 1, 10, 0, 0, 0, 0);
            this.dateFilter.Name = "dateFilter";
            this.dateFilter.Size = new System.Drawing.Size(200, 20);
            this.dateFilter.TabIndex = 4;
            this.toolTip.SetToolTip(this.dateFilter, "Display entries by date");
            this.dateFilter.ValueChanged += new System.EventHandler(this.dateFilter_ValueChanged);
            // 
            // resetLabel
            // 
            this.resetLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.resetLabel.AutoSize = true;
            this.resetLabel.Location = new System.Drawing.Point(472, 13);
            this.resetLabel.Name = "resetLabel";
            this.resetLabel.Size = new System.Drawing.Size(35, 13);
            this.resetLabel.TabIndex = 5;
            this.resetLabel.TabStop = true;
            this.resetLabel.Text = "Reset";
            this.resetLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.resetLabel_LinkClicked);
            // 
            // historySysLogGrid
            // 
            this.historySysLogGrid.AllowUserToAddRows = false;
            this.historySysLogGrid.AllowUserToDeleteRows = false;
            this.historySysLogGrid.AllowUserToOrderColumns = true;
            this.historySysLogGrid.AllowUserToResizeRows = false;
            this.historySysLogGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.historySysLogGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this.historySysLogGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.historySysLogGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.historySysLogGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn15});
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.MenuHighlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.historySysLogGrid.DefaultCellStyle = dataGridViewCellStyle16;
            this.historySysLogGrid.GridColor = System.Drawing.SystemColors.ControlLight;
            this.historySysLogGrid.Location = new System.Drawing.Point(0, 37);
            this.historySysLogGrid.Name = "historySysLogGrid";
            this.historySysLogGrid.ReadOnly = true;
            this.historySysLogGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.historySysLogGrid.RowHeadersVisible = false;
            this.historySysLogGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.historySysLogGrid.Size = new System.Drawing.Size(528, 235);
            this.historySysLogGrid.TabIndex = 6;
            this.toolTip.SetToolTip(this.historySysLogGrid, "Entry count:");
            this.historySysLogGrid.Visible = false;
            this.historySysLogGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.historySysLogGrid_CellContentClick);
            this.historySysLogGrid.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.historySysLogGrid_CellMouseClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "TICKET";
            this.dataGridViewTextBoxColumn1.HeaderText = "Ticket";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 25;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 125;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "WORKORDER";
            this.dataGridViewTextBoxColumn2.HeaderText = "Work Order";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 20;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ERROR";
            this.dataGridViewTextBoxColumn3.HeaderText = "Error";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 20;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "USERAGS";
            this.dataGridViewTextBoxColumn4.HeaderText = "User AGS";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 4;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "USERNAME";
            this.dataGridViewTextBoxColumn5.HeaderText = "User name";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 4;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "CTAGS";
            this.dataGridViewTextBoxColumn6.HeaderText = "CT AGS";
            this.dataGridViewTextBoxColumn6.MaxInputLength = 50;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "CTNAME";
            this.dataGridViewTextBoxColumn7.HeaderText = "CT Name";
            this.dataGridViewTextBoxColumn7.MaxInputLength = 50;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "RTC";
            this.dataGridViewTextBoxColumn8.HeaderText = "RTC";
            this.dataGridViewTextBoxColumn8.MaxInputLength = 50;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "TASKCATEGORY";
            this.dataGridViewTextBoxColumn9.HeaderText = "Task";
            this.dataGridViewTextBoxColumn9.MaxInputLength = 50;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "NOTES";
            this.dataGridViewTextBoxColumn10.HeaderText = "Notes";
            this.dataGridViewTextBoxColumn10.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "DATE";
            this.dataGridViewTextBoxColumn11.HeaderText = "Date";
            this.dataGridViewTextBoxColumn11.MaxInputLength = 20;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "TIME";
            this.dataGridViewTextBoxColumn12.HeaderText = "Time";
            this.dataGridViewTextBoxColumn12.MaxInputLength = 500;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "ID";
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn15.HeaderText = "ID";
            this.dataGridViewTextBoxColumn15.MaxInputLength = 20;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn15.ToolTipText = "ID within the database";
            this.dataGridViewTextBoxColumn15.Visible = false;
            this.dataGridViewTextBoxColumn15.Width = 20;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.View,
            this.Delete});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(108, 48);
            this.contextMenu.Text = "Menu";
            // 
            // View
            // 
            this.View.AccessibleName = "contextView";
            this.View.Name = "View";
            this.View.Size = new System.Drawing.Size(107, 22);
            this.View.Text = "View";
            this.View.ToolTipText = "View this data entry";
            this.View.Click += new System.EventHandler(this.View_Click);
            // 
            // Delete
            // 
            this.Delete.AccessibleName = "contextDelete";
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(107, 22);
            this.Delete.Text = "Delete";
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // panelSurprise
            // 
            this.panelSurprise.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panelSurprise.Location = new System.Drawing.Point(512, 254);
            this.panelSurprise.Name = "panelSurprise";
            this.panelSurprise.Size = new System.Drawing.Size(17, 16);
            this.panelSurprise.TabIndex = 7;
            this.panelSurprise.Visible = false;
            this.panelSurprise.Click += new System.EventHandler(this.panelSurprise_Click);
            // 
            // History
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 270);
            this.Controls.Add(this.panelSurprise);
            this.Controls.Add(this.resetLabel);
            this.Controls.Add(this.dateFilter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.filterBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.historyGrid);
            this.Controls.Add(this.historySysLogGrid);
            this.MaximumSize = new System.Drawing.Size(1525, 1800);
            this.MinimumSize = new System.Drawing.Size(545, 38);
            this.Name = "History";
            this.Text = "History";
            this.toolTip.SetToolTip(this, "E;");
            ((System.ComponentModel.ISupportInitialize)(this.historyGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.historySysLogGrid)).EndInit();
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox filterBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateFilter;
        public System.Windows.Forms.DataGridView historyGrid;
        private System.Windows.Forms.LinkLabel resetLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem View;
        private System.Windows.Forms.ToolStripMenuItem Delete;
        public System.Windows.Forms.DataGridView historySysLogGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn TYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn workOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn CT;
        private System.Windows.Forms.DataGridViewTextBoxColumn Node;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pudo;
        private System.Windows.Forms.DataGridViewTextBoxColumn FaultyS;
        private System.Windows.Forms.DataGridViewTextBoxColumn FaultyMPN;
        private System.Windows.Forms.DataGridViewTextBoxColumn SpareS;
        private System.Windows.Forms.DataGridViewTextBoxColumn SpareMPN;
        private System.Windows.Forms.DataGridViewTextBoxColumn RTC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Courier;
        private System.Windows.Forms.DataGridViewTextBoxColumn Connote;
        private System.Windows.Forms.DataGridViewTextBoxColumn Summary;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.Panel panelSurprise;
    }
}