﻿using System;

namespace GumshoeSPLG
{
    class HistoryItem : IEquatable<HistoryItem>
    {
        public override int GetHashCode()
        {
            return Id;
        }

        public int Id { get; set; }
        public string Ticket { get; set; }
        public string Reference { get; set; }
        public string WorkOrder { get; set; }
        public string Ags { get; set; }
        public string Node { get; set; }
        public string Pudo { get; set; }
        public string FaultySerial { get; set; }
        public string FaultyMpn { get; set; }
        public string SpareSerial { get; set; }
        public string SpareMpn { get; set; }
        public string Rtc { get; set; }
        public string Connote { get; set; }
        public string Notes { get; set; }
        public DateTime SavedAt { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((HistoryItem) obj);
        }

        public bool Equals(HistoryItem other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id;
        }
    }
}
