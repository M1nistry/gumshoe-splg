﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace GumshoeSPLG
{
    public partial class Send : Form
    {
        private readonly string ticketString;
        private Dictionary<string, string> ticketDictionary;
        private bool timeChanged;

        public Send(string ticketObj)
        {
            InitializeComponent();

            timeValueLabel.Text = DateTime.Now.ToString();
            int notesIndex = ticketObj.IndexOf(";Notes=");
            ticketString = ticketObj.Remove(notesIndex, ticketObj.Length - notesIndex);
            ParseString(ticketObj);
            timeChanged = false;
        }

        /// <summary> Splits the string passed from the Main Form and places the values respectively</summary>
        /// <param name="ticket">Job ticket as a combined string</param>
        public void ParseString(string ticket)
        {
            ticketDictionary = ticket.Split(';').Select(x => x.Split('=')).ToDictionary(y => y[0], y => y[1]);


            foreach (var dir in ticketDictionary)
            {
                if (dir.Key == "Tas") ticketListView.Items.Add(dir.Key).SubItems.Add(dir.Value);
                if (dir.Key == "Workorder") ticketListView.Items.Add(dir.Key).SubItems.Add(dir.Value);
                if (dir.Key == "AGS") ticketListView.Items.Add(dir.Key).SubItems.Add(dir.Value);
                if (dir.Key == "Node") ticketListView.Items.Add(dir.Key).SubItems.Add(dir.Value);
                if (dir.Key == "Pudo") ticketListView.Items.Add(dir.Key).SubItems.Add(dir.Value);
                if (dir.Key == "Faultyser") ticketListView.Items.Add(dir.Key).SubItems.Add(dir.Value);
                if (dir.Key == "Faultympn") ticketListView.Items.Add(dir.Key).SubItems.Add(dir.Value);
                if (dir.Key == "Spareser") ticketListView.Items.Add(dir.Key).SubItems.Add(dir.Value);
                if (dir.Key == "Sparempn") ticketListView.Items.Add(dir.Key).SubItems.Add(dir.Value);
                if (dir.Key == "Connote") ticketListView.Items.Add(dir.Key).SubItems.Add(dir.Value);
                if (dir.Key == "Notes")
                {
                    notesRTB.Text = dir.Value;
                    ticketListView.Items.Add(dir.Key).SubItems.Add("Click Here");
                }
            }
        }

        /// <summary> Toggles the display of the notes RTB, when you click on the correct row </summary>
        private void ticketListView_MouseClick(object sender, MouseEventArgs e)
        {
            if (ticketListView.SelectedItems.Count <= 0) return;

            if (ticketListView.SelectedItems[0].SubItems[1].Text == @"Click Here")
            {
                notesRTB.Visible = true;
            }
            else
            {
                if (notesRTB.Visible) notesRTB.Visible = false;
            }
        }

        /// <summary> Displays the context menu to change the listviews value </summary>
        private void ticketListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (ticketListView.SelectedItems[0].SubItems[0].Text == @"Notes") return;

            editContextMenu.Show(MousePosition.X, MousePosition.Y);
            toolStripItemKey.Text = ticketListView.SelectedItems[0].SubItems[0].Text;
            toolStripTextBox.Text = ticketListView.SelectedItems[0].SubItems[1].Text;
            toolStripTextBox.Focus();
        }

        /// <summary> Changes the value on keypress [enter] </summary>
        private void toolStripTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (char) Keys.Enter) return;
            foreach (
                ListViewItem lvItem in
                    ticketListView.Items.Cast<ListViewItem>().Where(lvItem => lvItem.Text == toolStripItemKey.Text))
            {
                lvItem.SubItems[1].Text = toolStripTextBox.Text;
                editContextMenu.Hide();
            }
        }

        /// <summary> Hides the notes RTB if the Listview loses focus </summary>
        private void ticketListView_Leave(object sender, EventArgs e)
        {
            if (notesRTB.Visible && !notesRTB.Focused) notesRTB.Visible = false;
        }

        /// <summary> Hides the notes RTB if itself loses focus </summary>
        private void notesRTB_Leave(object sender, EventArgs e)
        {
            notesRTB.Visible = false;
        }

        /// <summary> Replaces the placeholder text if it's currently empty and you leave it </summary>
        private void commentsRTB_Leave(object sender, EventArgs e)
        {
            if (commentsRTB.Text == "") commentsRTB.Text = @"Enter your comments here...";
        }

        /// <summary> Hides the placeholder text </summary>
        private void commentsRTB_Enter(object sender, EventArgs e)
        {
            if (commentsRTB.Text == @"Enter your comments here...") commentsRTB.Text = "";
        }

        /// <summary> Compiles the information, checks for wiki id and time values changed, then sends </summary>
        private void sendButton_Click(object sender, EventArgs e)
        {
            // Updates the ticketDictionary incase the values have been edited in the listview.
            for (int i = 0; i < ticketListView.Items.Count; i++)
            {
                ticketDictionary[ticketListView.Items[i].Text] = ticketListView.Items[i].SubItems[1].Text;
                if (i == 10) ticketDictionary[ticketListView.Items[i].Text] = notesRTB.Text;
            }
            if (ticketListView.Items[2].SubItems[1].Text == "")
            {
                MessageBox.Show(@"Please enter the AGS of the person who called and then retry");
                return;
            }

            if (timeChanged && wikiTextBox1.Text != "")
            {
                var mapi = new Mapi();

                mapi.AddRecipientTo("splgrsc@au1.ibm.com");
                mapi.SendMailPopup(emailTextBox.Text, Body());
            }
            else
            {
                MessageBox.Show(@"Please change the call back time and verify a Wiki page ID has been entered");
            }
        }

        /// <summary> Structures the body of the e-mail to be used for sending </summary>
        private string Body()
        {
            string comments = commentsRTB.Text != @"Enter your comments here..." ? commentsRTB.Text : "";
            string body = String.Format("Time\t\t: " + timeValueLabel.Text + "\n" +
                                        "Job Ticket\t: {0} \n" +
                                        "Workorder\t: {1} \n" +
                                        "CT AGS\t: {2} \n" +
                                        "Fault Node\t: {3} \n" +
                                        "Return PuDo\t: {4} \n" +
                                        "Faulty Serial\t: {5} \n" +
                                        "Faulty MPN\t: {6} \n" +
                                        "Spare Serial\t: {7} \n" +
                                        "Spare MPN\t: {8} \n" +
                                        "Return Connote\t: {9} \n" +
                                        "Ticket Notes\t:\n{10} \n" +
                                        "---------------------------- \n" +
                                        "Wiki Page\t: {11} \n" +
                                        "Call back Time\t: {14} \n" +
                                        "Comments\t: {12} \n\n" +
                                        "Gumshoe\t: {13}",
                ticketDictionary["Tas"], ticketDictionary["Workorder"],
                ticketDictionary["AGS"], ticketDictionary["Node"],
                ticketDictionary["Pudo"], ticketDictionary["Faultyser"],
                ticketDictionary["Faultympn"], ticketDictionary["Spareser"],
                ticketDictionary["Sparempn"], ticketDictionary["Connote"],
                ticketDictionary["Notes"], wikiTextBox1.Text, comments, ticketString,
                timePicker1.Value.ToShortTimeString() + " - " + timePicker2.Value.ToShortTimeString());
            return body;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Dispose(true);
        }

        private void timePicker1_ValueChanged(object sender, EventArgs e)
        {
            timeChanged = true;
        }

        private void timePicker2_ValueChanged(object sender, EventArgs e)
        {
            timeChanged = true;
        }
    }
}