﻿namespace GumshoeSPLG
{
    partial class Send
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ticketListView = new System.Windows.Forms.ListView();
            this.lvKeyColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvValueColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timeLabel = new System.Windows.Forms.Label();
            this.timeValueLabel = new System.Windows.Forms.Label();
            this.commentsRTB = new System.Windows.Forms.RichTextBox();
            this.sendButton = new System.Windows.Forms.Button();
            this.notesRTB = new System.Windows.Forms.RichTextBox();
            this.editContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripItemKey = new System.Windows.Forms.ToolStripMenuItem();
            this.wikiLabel = new System.Windows.Forms.Label();
            this.wikiTextBox1 = new System.Windows.Forms.TextBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.timePicker1 = new System.Windows.Forms.DateTimePicker();
            this.timePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.timeNoteLabel = new System.Windows.Forms.Label();
            this.editContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // ticketListView
            // 
            this.ticketListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ticketListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvKeyColumn,
            this.lvValueColumn});
            this.ticketListView.FullRowSelect = true;
            this.ticketListView.GridLines = true;
            this.ticketListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.ticketListView.Location = new System.Drawing.Point(12, 12);
            this.ticketListView.MultiSelect = false;
            this.ticketListView.Name = "ticketListView";
            this.ticketListView.Scrollable = false;
            this.ticketListView.ShowGroups = false;
            this.ticketListView.Size = new System.Drawing.Size(183, 266);
            this.ticketListView.TabIndex = 0;
            this.ticketListView.UseCompatibleStateImageBehavior = false;
            this.ticketListView.View = System.Windows.Forms.View.Details;
            this.ticketListView.Leave += new System.EventHandler(this.ticketListView_Leave);
            this.ticketListView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ticketListView_MouseClick);
            this.ticketListView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ticketListView_MouseDoubleClick);
            // 
            // lvKeyColumn
            // 
            this.lvKeyColumn.Text = "";
            this.lvKeyColumn.Width = 65;
            // 
            // lvValueColumn
            // 
            this.lvValueColumn.Text = "";
            this.lvValueColumn.Width = 110;
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Location = new System.Drawing.Point(214, 27);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(33, 13);
            this.timeLabel.TabIndex = 1;
            this.timeLabel.Text = "Time:";
            // 
            // timeValueLabel
            // 
            this.timeValueLabel.AutoSize = true;
            this.timeValueLabel.Location = new System.Drawing.Point(262, 27);
            this.timeValueLabel.Name = "timeValueLabel";
            this.timeValueLabel.Size = new System.Drawing.Size(13, 13);
            this.timeValueLabel.TabIndex = 2;
            this.timeValueLabel.Text = "1";
            // 
            // commentsRTB
            // 
            this.commentsRTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.commentsRTB.Location = new System.Drawing.Point(212, 147);
            this.commentsRTB.Name = "commentsRTB";
            this.commentsRTB.Size = new System.Drawing.Size(197, 102);
            this.commentsRTB.TabIndex = 3;
            this.commentsRTB.Text = "Enter your comments here...";
            this.commentsRTB.Enter += new System.EventHandler(this.commentsRTB_Enter);
            this.commentsRTB.Leave += new System.EventHandler(this.commentsRTB_Leave);
            // 
            // sendButton
            // 
            this.sendButton.Location = new System.Drawing.Point(334, 255);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(75, 23);
            this.sendButton.TabIndex = 4;
            this.sendButton.Text = "Send";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // notesRTB
            // 
            this.notesRTB.Location = new System.Drawing.Point(212, 147);
            this.notesRTB.Name = "notesRTB";
            this.notesRTB.Size = new System.Drawing.Size(197, 102);
            this.notesRTB.TabIndex = 5;
            this.notesRTB.Text = "";
            this.notesRTB.Visible = false;
            this.notesRTB.Leave += new System.EventHandler(this.notesRTB_Leave);
            // 
            // editContextMenu
            // 
            this.editContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox,
            this.toolStripItemKey});
            this.editContextMenu.Name = "editContextMenu";
            this.editContextMenu.Size = new System.Drawing.Size(161, 51);
            // 
            // toolStripTextBox
            // 
            this.toolStripTextBox.Name = "toolStripTextBox";
            this.toolStripTextBox.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox_KeyPress);
            // 
            // toolStripItemKey
            // 
            this.toolStripItemKey.Name = "toolStripItemKey";
            this.toolStripItemKey.Size = new System.Drawing.Size(160, 22);
            this.toolStripItemKey.Visible = false;
            // 
            // wikiLabel
            // 
            this.wikiLabel.AutoSize = true;
            this.wikiLabel.Location = new System.Drawing.Point(216, 46);
            this.wikiLabel.Name = "wikiLabel";
            this.wikiLabel.Size = new System.Drawing.Size(45, 13);
            this.wikiLabel.TabIndex = 8;
            this.wikiLabel.Text = "Wiki ID:";
            this.toolTip.SetToolTip(this.wikiLabel, "Wiki page used");
            // 
            // wikiTextBox1
            // 
            this.wikiTextBox1.Location = new System.Drawing.Point(262, 43);
            this.wikiTextBox1.MaxLength = 20;
            this.wikiTextBox1.Name = "wikiTextBox1";
            this.wikiTextBox1.Size = new System.Drawing.Size(147, 20);
            this.wikiTextBox1.TabIndex = 9;
            this.toolTip.SetToolTip(this.wikiTextBox1, "Wiki page used");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(216, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Subject:";
            this.toolTip.SetToolTip(this.label1, "E-mail subject line");
            // 
            // emailTextBox
            // 
            this.emailTextBox.Location = new System.Drawing.Point(262, 69);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(147, 20);
            this.emailTextBox.TabIndex = 12;
            this.toolTip.SetToolTip(this.emailTextBox, "Subject for the e-mail");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(215, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Times:";
            this.toolTip.SetToolTip(this.label2, "Time frame to call the technician back if needed (from - to)");
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(217, 255);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // timePicker1
            // 
            this.timePicker1.CalendarForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.timePicker1.CustomFormat = "HH:mm tt";
            this.timePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timePicker1.Location = new System.Drawing.Point(260, 116);
            this.timePicker1.Name = "timePicker1";
            this.timePicker1.ShowUpDown = true;
            this.timePicker1.Size = new System.Drawing.Size(67, 20);
            this.timePicker1.TabIndex = 15;
            this.toolTip.SetToolTip(this.timePicker1, "Time frame to call the technician back if needed (from - to)");
            this.timePicker1.ValueChanged += new System.EventHandler(this.timePicker1_ValueChanged);
            // 
            // timePicker2
            // 
            this.timePicker2.CustomFormat = "HH:mm tt";
            this.timePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timePicker2.Location = new System.Drawing.Point(340, 116);
            this.timePicker2.Name = "timePicker2";
            this.timePicker2.ShowUpDown = true;
            this.timePicker2.Size = new System.Drawing.Size(69, 20);
            this.timePicker2.TabIndex = 16;
            this.toolTip.SetToolTip(this.timePicker2, "Time frame to call the technician back if needed (from - to)");
            this.timePicker2.ValueChanged += new System.EventHandler(this.timePicker2_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(327, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = ">";
            // 
            // timeNoteLabel
            // 
            this.timeNoteLabel.AutoSize = true;
            this.timeNoteLabel.Location = new System.Drawing.Point(216, 97);
            this.timeNoteLabel.Name = "timeNoteLabel";
            this.timeNoteLabel.Size = new System.Drawing.Size(196, 13);
            this.timeNoteLabel.TabIndex = 18;
            this.timeNoteLabel.Text = "Timeframe to call the CT back if needed";
            // 
            // Send
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 290);
            this.Controls.Add(this.timeNoteLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.timePicker2);
            this.Controls.Add(this.timePicker1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.wikiTextBox1);
            this.Controls.Add(this.wikiLabel);
            this.Controls.Add(this.sendButton);
            this.Controls.Add(this.timeValueLabel);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.ticketListView);
            this.Controls.Add(this.notesRTB);
            this.Controls.Add(this.commentsRTB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Send";
            this.ShowIcon = false;
            this.Text = "Send to Business Hours";
            this.editContextMenu.ResumeLayout(false);
            this.editContextMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView ticketListView;
        private System.Windows.Forms.ColumnHeader lvKeyColumn;
        private System.Windows.Forms.ColumnHeader lvValueColumn;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Label timeValueLabel;
        private System.Windows.Forms.RichTextBox commentsRTB;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.RichTextBox notesRTB;
        private System.Windows.Forms.ContextMenuStrip editContextMenu;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox;
        private System.Windows.Forms.ToolStripMenuItem toolStripItemKey;
        private System.Windows.Forms.Label wikiLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.TextBox wikiTextBox1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker timePicker1;
        private System.Windows.Forms.DateTimePicker timePicker2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label timeNoteLabel;
    }
}