﻿namespace GumshoeSPLG
{
    partial class RFSReminder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelQuestion = new System.Windows.Forms.Label();
            this.buttonNow = new System.Windows.Forms.Button();
            this.buttonLater = new System.Windows.Forms.Button();
            this.buttonIgnore = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelQuestion
            // 
            this.labelQuestion.AutoSize = true;
            this.labelQuestion.Location = new System.Drawing.Point(13, 15);
            this.labelQuestion.Name = "labelQuestion";
            this.labelQuestion.Size = new System.Drawing.Size(322, 13);
            this.labelQuestion.TabIndex = 0;
            this.labelQuestion.Text = "RFS Reports are due for {0} would you like to complete them now?";
            // 
            // buttonNow
            // 
            this.buttonNow.Location = new System.Drawing.Point(47, 41);
            this.buttonNow.Name = "buttonNow";
            this.buttonNow.Size = new System.Drawing.Size(75, 23);
            this.buttonNow.TabIndex = 1;
            this.buttonNow.Text = "Now";
            this.buttonNow.UseVisualStyleBackColor = true;
            this.buttonNow.Click += new System.EventHandler(this.buttonNow_Click);
            // 
            // buttonLater
            // 
            this.buttonLater.Location = new System.Drawing.Point(142, 41);
            this.buttonLater.Name = "buttonLater";
            this.buttonLater.Size = new System.Drawing.Size(75, 23);
            this.buttonLater.TabIndex = 2;
            this.buttonLater.Text = "Later";
            this.buttonLater.UseVisualStyleBackColor = true;
            this.buttonLater.Click += new System.EventHandler(this.buttonLater_Click);
            // 
            // buttonIgnore
            // 
            this.buttonIgnore.Location = new System.Drawing.Point(240, 41);
            this.buttonIgnore.Name = "buttonIgnore";
            this.buttonIgnore.Size = new System.Drawing.Size(75, 23);
            this.buttonIgnore.TabIndex = 3;
            this.buttonIgnore.Text = "Ignore";
            this.buttonIgnore.UseVisualStyleBackColor = true;
            this.buttonIgnore.Click += new System.EventHandler(this.buttonIgnore_Click);
            // 
            // RFSReminder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 82);
            this.Controls.Add(this.buttonIgnore);
            this.Controls.Add(this.buttonLater);
            this.Controls.Add(this.buttonNow);
            this.Controls.Add(this.labelQuestion);
            this.Name = "RFSReminder";
            this.Text = "RFS Reminder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelQuestion;
        private System.Windows.Forms.Button buttonNow;
        private System.Windows.Forms.Button buttonLater;
        private System.Windows.Forms.Button buttonIgnore;
    }
}