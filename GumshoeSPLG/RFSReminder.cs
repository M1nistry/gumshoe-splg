﻿using System;
using System.Windows.Forms;

namespace GumshoeSPLG
{
    public partial class RFSReminder : Form
    {
        public RFSReminder()
        {
            InitializeComponent();
            labelQuestion.Text = String.Format(labelQuestion.Text, DateTime.Now.AddMonths(-1).ToString("MMMM"));
        }

        private void buttonNow_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Yes;
            Dispose();
        }

        private void buttonLater_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
            Dispose();
        }

        private void buttonIgnore_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Ignore;
            Dispose();
        }
    }
}
