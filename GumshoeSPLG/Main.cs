﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Deployment.Application;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using WindowsInput;
using Excel;
using GumshoeRFSTracker;
using GumshoeIssueTracker;
using HtmlAgilityPack;
using Microsoft.Win32;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace GumshoeSPLG
{
    public partial class Main : Form
    {
        private static Main _main;
        public History History;
        private About _about;
        public SqLiteDatabase Sql;
        private Settings _settings;
        private EditTemplate _template;
        private GumshoeComplexConfig.Main _complexConfig;
        private RFSTracker _rfsTracker;
        private IssueTracker _issueTracker;
        private DataTable _restrictedSLOC;
        private Send _send;
        private Disaster _disaster;
        private GumshoePuDo.GumshoePuDo _pudo;
        private BindingSource _filterBSource = new BindingSource();
        private int _extraSpare = 2, _extraFaulty = 2, _preChangeWidth;
#pragma warning disable 108,114
        public int Layout;
        //private readonly Dictionary<string, string> _agsDictionary;
        private readonly List<KeyValuePair<string, string>> _agsList;

        private const string POD_TEXT = @"\par
        \par
        \b\fs17 Investigation Step\b0\fs17\tab\tab\b\fs17 Action Taken\b0\fs17\par
        Work Order Status Checked?\tab\tab\par
        Enhancement Tab Checked?\tab\tab\par
        Delivery Location Confirmed?\tab\tab\par
        Courier Website Checked?\tab\tab\par
        Part Status Checked?\tab\tab\par
        Was Part Found?\tab\tab\tab\par
        Do you still require a part?\tab\tab\par";

        #region DllImport

        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vlc);

        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(IntPtr hWnd, int id);   

        #endregion

        public Main()
        {
            InitializeComponent();
            _main = this;
            SetPath();
            CheckFile();
            BackupHistory();
            PopulateLocations();
            PopulateKits();
            PopulateAddresses();
            PopulateConsumables();

            //Registers CTRL+0-9, CTRL+NUM0-NUM9, Insert and Tab globally while the program is running.

            #region Register HotKeys

            if (!RegisterHotKey(Handle, 1, 0x0002, (int) Keys.D1))
            {
                MessageBox.Show(@"We were unable to bind the hotkeys. Verify you have not already got an instance of Gumshoe SPLG running.", @"Hotkey Fail!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            RegisterHotKey(Handle, 2, 0x0002, (int) Keys.D2);
            RegisterHotKey(Handle, 3, 0x0002, (int) Keys.D8);
            RegisterHotKey(Handle, 4, 0x0002, (int) Keys.D3);
            RegisterHotKey(Handle, 5, 0x0002, (int) Keys.D9);
            RegisterHotKey(Handle, 6, 0x0002, (int) Keys.D4);
            RegisterHotKey(Handle, 7, 0x0002, (int) Keys.D5);
            RegisterHotKey(Handle, 8, 0x0002, (int) Keys.D6);
            RegisterHotKey(Handle, 9, 0x0002, (int) Keys.D7);
            RegisterHotKey(Handle, 0, 0x0002, (int) Keys.D0);
            RegisterHotKey(Handle, 1, 0x0002, (int) Keys.NumPad1);
            RegisterHotKey(Handle, 2, 0x0002, (int) Keys.NumPad2);
            RegisterHotKey(Handle, 3, 0x0002, (int) Keys.NumPad8);
            RegisterHotKey(Handle, 4, 0x0002, (int) Keys.NumPad3);
            RegisterHotKey(Handle, 5, 0x0002, (int) Keys.NumPad9);
            RegisterHotKey(Handle, 6, 0x0002, (int) Keys.NumPad4);
            RegisterHotKey(Handle, 7, 0x0002, (int) Keys.NumPad5);
            RegisterHotKey(Handle, 8, 0x0002, (int) Keys.NumPad6);
            RegisterHotKey(Handle, 9, 0x0002, (int) Keys.NumPad7);
            RegisterHotKey(Handle, 0, 0x0002, (int) Keys.NumPad0);
            RegisterHotKey(Handle, 10, 0x0002, (int) Keys.Tab);
            RegisterHotKey(Handle, 11, 0x0002, (int) Keys.Insert);
            RegisterHotKey(Handle, 12, 0x0001, (int) Keys.D1);

            #endregion

            hotkeysToolStripMenuItem.Checked = Properties.Settings.Default.Hotkeys;
            hotkeyCheckBox.Checked = Properties.Settings.Default.Hotkeys;
            templateToolStripMenuItem.Checked = Properties.Settings.Default.Template;
            hotkeyLabel.Visible = hotkeysToolStripMenuItem.Checked;
            Layout = Properties.Settings.Default.layout;
            statusListBox.Size = new Size(392, 91);
            statusListBox.Location = new Point(0, 608);
            //_agsDictionary = new Dictionary<string, string>();
            _agsList = new List<KeyValuePair<string, string>>();
            ChangeLayout(Layout);
            string name = Properties.Settings.Default.greetingName == ""
                ? Environment.UserName
                : Properties.Settings.Default.greetingName;
            extendedStatusStrip.AddStatus(@"Welcome back " + name);
            Sql = new SqLiteDatabase(Properties.Settings.Default.SavePath + @"\GumshoeSPLG\HistoryDB.s3db");
            PopulateMisc();
            PopulateMiscTabs();
            comboDataSelect.SelectedIndex = 0;
            if (!Sql.ColumnExists("SELECT RTC FROM HISTORY"))
                Sql.ExecuteScalar("ALTER TABLE HISTORY ADD COLUMN RTC TEXT;");
            if (!Sql.ColumnExists("SELECT SYSTEM FROM HISTORY"))
                Sql.ExecuteScalar("ALTER TABLE HISTORY ADD COLUMN SYSTEM TEXT");
            if (templateToolStripMenuItem.Checked)
            {
                LoadTemplate();
            }
            PopulateAgsHistory();
            RandomJudgement();
            //StartupAdobe();
            RestrictedExcel();
            CheckRfsEmail();
        }

        public static Main GetSingleton()
        {
            return _main;
        }

        #region Plug-ins

        private void CheckRfsEmail()
        {
            if (Properties.Settings.Default.lastRFSEmail.Year < 2000)
            {
                Properties.Settings.Default.lastRFSEmail = DateTime.Now;
                Properties.Settings.Default.Save();
            }
            if (Properties.Settings.Default.lastRFSEmail.Year < DateTime.Now.Year ||
                Properties.Settings.Default.lastRFSEmail.Month < DateTime.Now.Month)
            {
                var reminder = new RFSReminder();
                reminder.ShowDialog();
                if (reminder.DialogResult == DialogResult.Yes)
                {
                    rFSTrackerToolStripMenuItem_Click(this, new EventArgs());
                }
                if (reminder.DialogResult == DialogResult.Ignore)
                {
                    Properties.Settings.Default.lastRFSEmail = DateTime.Now;
                    Properties.Settings.Default.Save();
                }
            }
        }

        #region RFS Tracker

        private void rFSTrackerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.lastRFSEmail = DateTime.Now;
            Properties.Settings.Default.Save();
            if (_rfsTracker != null && _rfsTracker.Visible)
            {
                _rfsTracker.Focus();
                SystemSounds.Exclamation.Play();
            }
            else
            {
                _rfsTracker = new RFSTracker();
                _rfsTracker.Show();
                _rfsTracker.FormClosed += RfsTrackerOnFormClosed;
            }
        }

        private void RfsTrackerOnFormClosed(object sender, FormClosedEventArgs formClosedEventArgs)
        {
            _rfsTracker = null;
        }

        #endregion

        #region Issue Tracker
        private void issueTrackerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_issueTracker != null && _issueTracker.Visible)
            {
                _issueTracker.Focus();
                SystemSounds.Exclamation.Play();
            }
            else
            {
                _issueTracker = new IssueTracker();
                _issueTracker.RemoveRFSLinks();
                _issueTracker.Show();
                _issueTracker.FormClosed += _issueTracker_FormClosed;
            }
        }

        void _issueTracker_FormClosed(object sender, FormClosedEventArgs e)
        {
            _issueTracker = null;
        }
        #endregion

        #region Complex Config

        // Loads the complex config plugin
        private void complexConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_complexConfig != null && _complexConfig.Visible)
            {
                _complexConfig.Focus();
                SystemSounds.Exclamation.Play();
            }
            else
            {
                _complexConfig = new GumshoeComplexConfig.Main();
                _complexConfig.Show();
                _complexConfig.ApplicationQuit += _complexConfig_ApplicationQuit;
                //_complexConfig.FormClosed += (o, ea) => _complexConfig = null;
            }
        }

        private void _complexConfig_ApplicationQuit()
        {
            _complexConfig = null;
        }

        #endregion

        #region Disaster Recovery

        private void disasterRecoveryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show(@"This interface is to be used when SPS is down for an extended period of time. " +
                            @"Please do not use it in other situations as the data is only updated when it's " +
                            @"required and thus is not a reliable source for information.", @"Disaster Recovery",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Hand);
            if (result == DialogResult.Cancel) return;
            if (_disaster != null && _disaster.Visible)
            {
                _disaster.Focus();
                SystemSounds.Exclamation.Play();
            }
            else
            {
                _disaster = new Disaster(this);
                _disaster.Show();
                _disaster.FormClosed += (o, ea) => _disaster = null;
            }
        }

        #endregion

        #region PuDo Audits

        private void puDoAuditsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_pudo != null && _pudo.Visible)
            {
                _pudo.Focus();
                SystemSounds.Exclamation.Play();
            }
            else
            {
                _pudo = new GumshoePuDo.GumshoePuDo();
                _pudo.Show();
                _pudo.ApplicationQuit += _pudo_ApplicationQuit;
                //_pudo.FormClosed += (o, ea) => _pudo = null;
            }
        }

        private void _pudo_ApplicationQuit()
        {
            _pudo = null;
        }

        #endregion

        #endregion

        /// <summary> Adds Adobe Reader to the registry as a startup program to avoid PDFs not loading </summary>
        public void StartupAdobe()
        {
            var registryKey = Registry.CurrentUser.OpenSubKey(@"Software\Adobe\Acrobat Reader\10.0\InstallPath");
            if (registryKey == null) return;
            var adobePath = registryKey.GetValue(null) as string;
            var startupKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
            if (startupKey != null && adobePath != null) startupKey.SetValue("Adobe Reader", adobePath + @"\AcroRd32.exe");  
        }

        private void RestrictedExcel()
        {
            string filePath = Directory.GetCurrentDirectory() + "\\datafiles\\restrictedSLOC.xlsx";
            try
            {
                var stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
                var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                excelReader.IsFirstRowAsColumnNames = true;
                var ds = excelReader.AsDataSet();
                _restrictedSLOC = ds.Tables[0];
                excelReader.Close();
            }
            catch (Exception ex)
            {
                extendedStatusStrip.AddStatus(@"Cannot find the locations file");
                ErrorOutput(ex.Message, @"PopulateLocations", ex.Source);
            }
        }

        /// <summary> Changes the layout of the forms objects </summary>
        /// <param name="layoutInt">the layout to change to</param>
        public void ChangeLayout(int layoutInt)
        {
            switch (layoutInt)
            {
                case (1):
                    level1GroupBox.Visible = true;
                    sysLogsGroupBox.Visible = false;
                    hotkeysSysLog.Visible = false;
                    Layout = layoutInt;
                    break;
                case (2):
                    sysLogsGroupBox.Visible = true;
                    level1GroupBox.Visible = false;
                    hotkeysSysLog.Visible = true;
                    Layout = layoutInt;
                    break;
                case (3):
                    break;
            }
        }

        /// <summary> Sets the path for the History database and Template text file </summary>
        public void SetPath()
        {
            if (!Properties.Settings.Default.oscEnabled)
                Properties.Settings.Default.SavePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (Properties.Settings.Default.oscEnabled)
                Properties.Settings.Default.SavePath = Properties.Settings.Default.oscPath;
            Properties.Settings.Default.Save();
        }

        private static void BackupHistory()
        {
            string historyPath = Properties.Settings.Default.SavePath + "\\GumshoeSPLG";
            const string historyFile = "HistoryDB.s3db";
            string destinationPath = historyPath + @"\Backups";
            string destinationFile = "HistoryDB" + DateTime.Now.ToShortDateString().Replace("/", "") + ".s3db";

            if (File.Exists(Path.Combine(historyPath, historyFile)) &&
                !File.Exists(Path.Combine(destinationPath, destinationFile)))
            {
                File.Copy(Path.Combine(historyPath, historyFile), Path.Combine(destinationPath, destinationFile));
            }
            if (Directory.GetFiles(destinationPath).Count() <= 5) return;
            foreach (var fi in new DirectoryInfo(destinationPath).GetFiles().OrderByDescending(x => x.LastWriteTime).Skip(5))
                fi.Delete();
        }

        /// <summary> Sets the forms text boxes to a 'new' status </summary>
        private void SetDefaultValues()
        {
            if (Layout == 1)
            {
                saveLabel.Text = @"Saved at:";
                ticketTypeCombo.Text = @"TAS";
                tasTextBox.Text = @"TAS00000";
                workOrderTextBox.Text = "";
                ctTextBox.Text = "";
                pudoTextBox.Text = "";
                nodeTextBox.Text = "";
                faultyMPNBox.Text = "";
                faultySerialBox.Text = "";
                spareMPNBox.Text = "";
                spareSerialBox.Text = "";
                courierCombo.Text = @"Toll";
                rtcTextBox.Text = "";
                connoteTextBox.Text = "";
                LoadTemplate();
            }
            if (Layout == 2)
            {
                saveLabel.Text = @"Saved at:";
                ticketTextBoxSys.Text = "";
                workOrderTextBoxSys.Text = "";
                errorMessageTextBox.Text = "";
                userAGSBoxSys.Text = "";
                agsTextBoxSys.Text = "";
                nameTextBoxSys.Text = "";
                ctNameTextBox.Text = "";
                rtcTextBoxSys.Text = "";
                taskComboBoxSys.SelectedIndex = -1;
                LoadTemplate();
            }

        }

        /// <summary> Outputs the errors to the error log file with succinct detail </summary>
        /// <param name="error"> The error.message </param>
        /// <param name="method"> The method the error occured in</param>
        /// <param name="code"> the error.source </param>
        internal void ErrorOutput(string error, string method, string code)
        {
            var errorMsg = @"[" + DateTime.Now + @"] Error occured from method: " + method + @" on version: " + Text +
                              @" with the message of: " +
                              error + @"Code: " + code;
            if (!File.Exists(Properties.Settings.Default.SavePath + @"\GumshoeSPLG\ErrorLog.txt")) return;
            File.AppendAllText(Properties.Settings.Default.SavePath + @"\GumshoeSPLG\ErrorLog.txt",
                Environment.NewLine + Environment.NewLine + errorMsg);
        }

        /// <summary> Checks if the necesarry files exist at the path, if not, create them and their directories </summary>
        public void CheckFile()
        {
            string directory = Properties.Settings.Default.SavePath;
            if (Properties.Settings.Default.oscEnabled) directory = Properties.Settings.Default.oscPath;
            if (!File.Exists(directory + "\\GumshoeSPLG")) Directory.CreateDirectory(directory + "\\GumshoeSPLG");
            if (!File.Exists(directory + "\\GumshoeSPLG\\Backups"))
                Directory.CreateDirectory(directory + "\\GumshoeSPLG\\Backups");
            if (!File.Exists(directory + "\\GumshoeSPLG\\HistoryDB.s3db"))
            {
                File.Copy(Directory.GetCurrentDirectory() + "\\datafiles\\HistoryDB.s3db",
                    directory + "\\GumshoeSPLG\\HistoryDB.s3db");
            }
            if (!File.Exists(directory + "\\GumshoeSPLG\\SystemLogsDB.s3db"))
            {
                File.Copy(Directory.GetCurrentDirectory() + "\\datafiles\\SystemLogsDB.s3db",
                    directory + "\\GumshoeSPLG\\SystemLogsDB.s3db");
            }
            if (!File.Exists(directory + "\\GumshoeSPLG\\Template.txt"))
            {
                File.Copy(Directory.GetCurrentDirectory() + "\\datafiles\\Template.txt",
                    directory + "\\GumshoeSPLG\\Template.txt");
            }
            if (!File.Exists(directory + "\\GumshoeSPLG\\ErrorLog.txt"))
            {
                File.Copy(Directory.GetCurrentDirectory() + "\\datafiles\\ErrorLog.txt",
                    directory + "\\GumshoeSPLG\\ErrorLog.txt");
            }
        }

        /// <summary> Constructor to determine if there's unsaved data </summary>
        /// <returns>true/false</returns>
        private bool UnsavedData()
        {
            if (Layout == 1)
                return tasTextBox.Text.Equals("TAS00000") && workOrderTextBox.Text.Equals("") &&
                       ctTextBox.Text.Equals("") && pudoTextBox.Text.Equals("") && nodeTextBox.Text.Equals("") &&
                       faultySerialBox.Text.Equals("") && faultyMPNBox.Text.Equals("") && spareSerialBox.Text.Equals("") &&
                       spareMPNBox.Text.Equals("") && connoteTextBox.Text.Equals("") && rtcTextBox.Text.Equals("");
            if (Layout == 2)
                return ticketTextBoxSys.Text.Equals("") && workOrderTextBoxSys.Text.Equals("") &&
                       errorMessageTextBox.Text.Equals("") && rtcTextBoxSys.Text.Equals("");
            return false;
        }

        #region ToolStrip Methods

        #region save, new, history, exit

        private void historyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (History != null && History.Visible)
            {
                History.Focus();
                SystemSounds.Exclamation.Play();
            }
            else
            {
                History = new History(this);
                History.FormClosed += (o, ea) => History = null;
                History.Show();
            }
        }

        /// <summary> Saves the current forms data to the database, checking if they already exist or not </summary>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!SaveTicket())
            {
                extendedStatusStrip.AddStatus("Failed to save the ticket.");
            }
            toolTip.SetToolTip(menuStrip1, @"Saved today: " + Sql.SavedToday());
        }

        /// <summary> Checks if the curreny form is empty or unsaved before creating a new one </summary>
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!UnsavedData() && saveLabel.Text == @"Saved at:")
            {
                if (MessageBox.Show(@"There is unsaved information, are you sure you want to create a new entry?",
                    @"Confirm",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) ==
                    DialogResult.Yes)
                {
                    SetDefaultValues();
                    _extraSpare = 2;
                    _extraFaulty = 2;
                }
            }
            else
            {
                SetDefaultValues();
                _extraSpare = 2;
                _extraFaulty = 2;
            }
            rtbExtended.RichTextBox.Select(rtbExtended.RichTextBox.TextLength, 0);
        }

        //Closes the program and disposes all information
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (
                MessageBox.Show(@"Are you sure you want to exit? All unsaved information will be lost.", @"Confirm",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) ==
                DialogResult.Yes)
            {
                Dispose(true);
            }
        }

        #endregion

        #region Template, logs and wiki

        private void wikiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("http://csckm.au.ibm.com/display/CSCSPL/Gumshoe+SPLG");
        }

        private void changeLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("http://csckm.au.ibm.com/display/CSCSPL/Change+log");
        }

        private void errorLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(Properties.Settings.Default.SavePath + @"\GumshoeSPLG\ErrorLog.txt");
        }

        /// <summary> Opens the Template.txt in the environments default text editing application </summary>
        private void editTemplateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_template != null && _template.Visible)
            {
                _template.Focus();
                SystemSounds.Exclamation.Play();
            }
            else
            {
                _template = new EditTemplate(this);
                _template.FormClosed += (o, ea) => _template = null;
                _template.Show();
            }
        }

        //Toggle the template setting and load the template if required, checks for potential data loss.
        private void templateToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            string templateText =
                File.ReadAllText(Properties.Settings.Default.SavePath + "\\GumshoeSPLG\\Template.txt").Replace("\r", "");
            if (templateToolStripMenuItem.Checked)
            {
                if (rtbExtended.RichTextBox.Text != templateText && rtbExtended.RichTextBox.Text != "")
                {
                    if (MessageBox.Show(
                        @"Data may be lost if you continue, are you sure?",
                        @"Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Hand) == DialogResult.OK)
                    {
                        LoadTemplate();
                    }
                }
                else
                {
                    LoadTemplate();
                }
            }
            else
            {
                if (rtbExtended.RichTextBox.Text != templateText)
                {
                    if (MessageBox.Show(
                        @"Data may be lost if you continue, are you sure?",
                        @"Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Hand) == DialogResult.OK)
                        rtbExtended.RichTextBox.Text = "";
                    return;
                }
                rtbExtended.RichTextBox.Text = "";
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_about != null && _about.Visible)
            {
                _about.Focus();
                SystemSounds.Exclamation.Play();
            }
            else
            {
                _about = new About();
                _about.FormClosed += (o, ea) => _about = null;
                _about.Show();
            }
        }

        #endregion

        #region Tools/Settings

        //Toggles the display of the hotkeys menu
        private void hotkeysToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Hotkeys = hotkeysToolStripMenuItem.Checked;
            hotkeyLabel.Visible = Properties.Settings.Default.Hotkeys;
            Properties.Settings.Default.Save();
        }

        //Toggles always on top setting
        private void alwaysOnTopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.AlwaysOnTop = alwaysOnTopToolStripMenuItem.Checked;
            TopMost = Properties.Settings.Default.AlwaysOnTop;
        }

        private void settingsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (_settings != null && _settings.Visible)
            {
                _settings.Focus();
                SystemSounds.Exclamation.Play();
            }
            else
            {
                _settings = new Settings(this);
                _settings.FormClosed += (o, ea) => _settings = null;
                _settings.Show();
            }
        }

        #endregion

        #region Check for updates

        //Manual check for updates
        private void checkForUpdatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!ApplicationDeployment.IsNetworkDeployed) return;
            ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;

            UpdateCheckInfo info;
            try
            {
                info = ad.CheckForDetailedUpdate();
            }
            catch (DeploymentDownloadException dde)
            {
                MessageBox.Show(@"The new version of the application cannot be downloaded at this time.");
                ErrorOutput(dde.Message, @"CheckUpdates", dde.Source);
                return;
            }
            catch (InvalidDeploymentException ide)
            {
                MessageBox.Show(@"Cannot check for a new version of the application.");
                ErrorOutput(ide.Message, @"CheckUpdates", ide.Source);
                return;
            }
            catch (InvalidOperationException ioe)
            {
                MessageBox.Show(
                    @"This application cannot be updated. It is likely not a ClickOnce application. Error: " +
                    ioe.Message);
                return;
            }

            if (info.UpdateAvailable)
            {
                bool doUpdate = true;

                if (!info.IsUpdateRequired)
                {
                    DialogResult dr =
                        MessageBox.Show(@"An update is available. Would you like to update the application now?",
                            @"Update Available", MessageBoxButtons.OKCancel);
                    if (DialogResult.OK != dr)
                    {
                        doUpdate = false;
                    }
                }
                else
                {
                    // Display a message that the app MUST reboot. Display the minimum required version.
                    MessageBox.Show(@"This application has detected a mandatory update from your current " +
                                    @"version to version " + info.MinimumRequiredVersion +
                                    @". The application will now install the update and restart.",
                        @"Update Available", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }

                if (doUpdate)
                {
                    try
                    {
                        ad.Update();
                        MessageBox.Show(@"The application has been upgraded, and will now restart.");
                        Application.Restart();
                    }
                    catch (DeploymentDownloadException dde)
                    {
                        MessageBox.Show(@"Cannot install the latest version of the application.");
                        ErrorOutput(dde.Message, @"CheckUpdates", dde.Source);
                    }
                }
            }
            else
            {
                MessageBox.Show(@"No updates found.");
            }
        }

        #endregion

        #region Import/Export

        /// <summary> Handles the menu button to copy the current tickets details to the clipboard </summary>
        private void sendToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string val = "tas=" + tasTextBox.Text + ";workorder=" + workOrderTextBox.Text +
                         ";ags=" + ctTextBox.Text + ";node=" + nodeTextBox.Text +
                         ";pudo=" + pudoTextBox.Text + ";faultyser=" + faultySerialBox.Text +
                         ";faultympn=" + faultyMPNBox.Text + ";spareser=" + spareSerialBox.Text +
                         ";sparempn=" + spareMPNBox.Text + ";connote=" + connoteTextBox.Text;
            extendedStatusStrip.AddStatus(@"Current job copied to your clipboard");
            Clipboard.SetText(val);
        }

        // Imports the ticket from the input within the toolstrip textbox
        // prompts to save current open ticket if it isn't saved already.
        private void importToolStripTextBox_TextChanged(object sender, EventArgs e)
        {
            if (importToolStripTextBox.Text == "" || importToolStripTextBox.Text == @"Paste here...") return;
            if (Layout != 1) ChangeLayout(1);
            if (importToolStripTextBox.Text.Length < 10) return;

            try
            {
                string unzipped = importToolStripTextBox.Text;
                Dictionary<string, string> objDic = unzipped.Split(';')
                    .Select(x => x.Split('='))
                    .ToDictionary(y => y[0], y => y[1]);
                var result = DialogResult.Yes;
                if (!UnsavedData())
                    result = MessageBox.Show(
                        @"There is unsaved information, are you sure you want to load this entry?", @"Confirm",
                        MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    foreach (var dir in objDic)
                    {
                        if (dir.Key.ToLower() == "tas") tasTextBox.Text = dir.Value;
                        if (dir.Key.ToLower() == "workorder") workOrderTextBox.Text = dir.Value;
                        if (dir.Key.ToLower() == "ags") ctTextBox.Text = dir.Value;
                        if (dir.Key.ToLower() == "node") nodeTextBox.Text = dir.Value;
                        if (dir.Key.ToLower() == "pudo") pudoTextBox.Text = dir.Value;
                        if (dir.Key.ToLower() == "faultyser") faultySerialBox.Text = dir.Value;
                        if (dir.Key.ToLower() == "faultympn") faultyMPNBox.Text = dir.Value;
                        if (dir.Key.ToLower() == "spareser") spareSerialBox.Text = dir.Value;
                        if (dir.Key.ToLower() == "sparempn") spareMPNBox.Text = dir.Value;
                        if (dir.Key.ToLower() == "connote") connoteTextBox.Text = dir.Value;
                    }
                    fileToolStripMenuItem.HideDropDown();
                }
                importToolStripTextBox.Text = @"Paste here...";
            }
            catch (FormatException fe)
            {
                importToolStripTextBox.Text = "";
                ErrorOutput(fe.Message, @"Import", fe.Source);
            }
            catch (Exception ex)
            {
                extendedStatusStrip.AddStatus(@"Failed to import the data, check the error log for more details.");
                ErrorOutput(ex.Message, @"Import", ex.Source);
            }
        }

        //Pastes the clipboard contents if it's length is a possible result of having an export
        private void importToolStripTextBox_Click(object sender, EventArgs e)
        {
            if (Clipboard.GetText().Length > 20) importToolStripTextBox.Text = Clipboard.GetText(TextDataFormat.Text);
        }

        /// <summary> Sets the text/focus according to the mouse behaviour </summary>
        private void importToolStripTextBox_MouseEnter(object sender, EventArgs e)
        {
            if (importToolStripTextBox.Text != @"Paste here...") return;
            importToolStripTextBox.Text = "";
            importToolStripTextBox.Focus();
        }

        #endregion

        #region Send to business hours

        private void sendToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string val = "Tas=" + tasTextBox.Text + ";Workorder=" + workOrderTextBox.Text +
                         ";AGS=" + ctTextBox.Text + ";Node=" + nodeTextBox.Text +
                         ";Pudo=" + pudoTextBox.Text + ";Faultyser=" + faultySerialBox.Text +
                         ";Faultympn=" + faultyMPNBox.Text + ";Spareser=" + spareSerialBox.Text +
                         ";Sparempn=" + spareMPNBox.Text + ";Connote=" + connoteTextBox.Text +
                         ";Notes=" + rtbExtended.RichTextBox.Text;
            if (_send != null && _send.Visible)
            {
                _send.Focus();
                SystemSounds.Exclamation.Play();
            }
            else
            {
                _send = new Send(val);
                _send.Show();
                _send.FormClosed += (o, ea) => _send = null;
            }
        }

        #endregion

        #region AddressDGV

        private void copyFullAddressLineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowNum = addressDGV.SelectedRows[0].Index;

            string address = String.Format("{1}{0}{2} {3}{0}{4}{0}{5} - {6}", ", ", addressDGV["NameCol", rowNum].Value,
                addressDGV["HouseNo", rowNum].Value, addressDGV["StreetCol", rowNum].Value,
                addressDGV["CityCol", rowNum].Value, addressDGV["Rg", rowNum].Value,
                addressDGV["PostlCodeCol", rowNum].Value);

            Clipboard.SetText(address);
        }

        private void copyFullAddressFormattedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowNum = addressDGV.SelectedRows[0].Index;

            string address = String.Format("{1}{0}{2} {3}{0}{4}{0}{5}\t\t{6}", Environment.NewLine,
                addressDGV["NameCol", rowNum].Value, addressDGV["HouseNo", rowNum].Value,
                addressDGV["StreetCol", rowNum].Value, addressDGV["CityCol", rowNum].Value,
                addressDGV["Rg", rowNum].Value, addressDGV["PostlCodeCol", rowNum].Value);

            Clipboard.SetText(address);
        }

        #endregion

        #endregion

        /// <summary> Saves the current ticket to the SQLiteDB </summary>
        private bool SaveTicket()
        {
            rtbExtended.RichTextBox.Rtf = rtbExtended.RichTextBox.Rtf.Replace("'", "`");
            if (tasTextBox.Text == "") tasTextBox.Text = @"TAS00000";

            #region SaveCall

            if (Layout == 1)
            {
                Sql = new SqLiteDatabase(Properties.Settings.Default.SavePath + @"\GumshoeSPLG\HistoryDB.s3db");
                var saveData = new Dictionary<string, string>
                {
                    {"SYSTEM", ticketTypeCombo.Text},
                    {"TAS", tasTextBox.Text},
                    {"WORKORDER", workOrderTextBox.Text},
                    {"CT", ctTextBox.Text},
                    {"PUDO", pudoTextBox.Text},
                    {"NODE", nodeTextBox.Text},
                    {"FAULTYSERIAL", faultySerialBox.Text},
                    {"FAULTYMPN", faultyMPNBox.Text},
                    {"SPARESERIAL", spareSerialBox.Text},
                    {"SPAREMPN", spareMPNBox.Text},
                    {"RTC", rtcTextBox.Text},
                    {"COURIER", courierCombo.Text},
                    {"CONNOTE", connoteTextBox.Text},
                    {"SUMMARY", rtbExtended.RichTextBox.Text},
                    {"TIME", DateTime.Now.ToShortTimeString()},
                    {"DATE", DateTime.Now.Date.ToShortDateString()}
                };
                var keys = new List<string>(saveData.Keys);
                foreach (var key in keys)
                {
                    if (!saveData[key].Contains("'")) continue;
                    saveData[key] = saveData[key].Replace(@"'", @"`");
                }

                //Checks if you've clicked a ticket from history and to update it via it's ID or not.
                if (Common.rowID > 0 &&
                    tasTextBox.Text.Equals(Sql.ExecuteScalar("SELECT TAS FROM HISTORY WHERE ID=" + Common.rowID)))
                {
                    try
                    {
                        Sql.Update("HISTORY", saveData, String.Format("ID = {0}", Common.rowID));
                        saveLabel.Text = @"Updated at: " + DateTime.Now.ToShortTimeString();
                        extendedStatusStrip.AddStatus(@"Successfully updated " + tasTextBox.Text + @" in your history.");
                        return true;
                    }
                    catch (Exception ex)
                    {
                        extendedStatusStrip.AddStatus(@"Failed to update " + tasTextBox.Text + @" in your history.");
                        ErrorOutput(ex.Message, @"SaveTicket", ex.Source);
                    }
                }
                else
                {
                    try
                    {
                        #region SaveMethods

                        //Neither exist and TAS isn't blank? Insert new one!
                        if ((tasTextBox.Text != @"TAS00000" && !Sql.TasExist(tasTextBox.Text)) &&
                            !Sql.WorkorderExist(workOrderTextBox.Text))
                        {
                            Sql.Insert("HISTORY", saveData);
                            saveLabel.Text = @"Saved at:" + DateTime.Now.ToShortTimeString();
                            extendedStatusStrip.AddStatus(@"Successfully saved " + tasTextBox.Text + @" to your history.");
                            return true;
                        }
                        //Work order exists, tas isn't blank but tas doesn't exist? Insert new one
                        if (tasTextBox.Text != @"TAS00000" && Sql.WorkorderExist(workOrderTextBox.Text) &&
                            !Sql.TasExist(tasTextBox.Text))
                        {
                            Sql.Insert("HISTORY", saveData);
                            saveLabel.Text = @"Updated at:" + DateTime.Now.ToShortTimeString();
                            extendedStatusStrip.AddStatus(@"Successfully inserted " + workOrderTextBox.Text + @" in your history.");
                            return true;
                        }
                        //Tas Exists, work order doesn't but is blank. Update using TAS
                        if (tasTextBox.Text != @"TAS00000" && !Sql.WorkorderExist(workOrderTextBox.Text) &&
                            Sql.TasExist(tasTextBox.Text) && workOrderTextBox.Text == "")
                        {
                            Sql.Update("HISTORY", saveData, String.Format("TAS ='{0}'", tasTextBox.Text));
                            extendedStatusStrip.AddStatus(@"Successfully updated " + tasTextBox.Text + @" in your history.");
                            return true;
                        }
                        //Tas Exists, work order doesn't and tas isn't blank? insert new one.
                        if (tasTextBox.Text != @"TAS00000" && !Sql.WorkorderExist(workOrderTextBox.Text) &&
                            Sql.TasExist(tasTextBox.Text))
                        {
                            Sql.Insert("HISTORY", saveData);
                            saveLabel.Text = @"Updated at:" + DateTime.Now.ToShortTimeString();
                            extendedStatusStrip.AddStatus(@"Successfully inserted " + tasTextBox.Text + @" in your history.");
                            return true;
                        }
                        //Both exist? Update them.
                        if (tasTextBox.Text != @"TAS00000" && Sql.TasExist(tasTextBox.Text) &&
                            Sql.WorkorderExist(workOrderTextBox.Text))
                        {
                            Sql.Update("HISTORY", saveData,
                                String.Format("TAS ='{0}' AND WORKORDER ='{1}'", tasTextBox.Text, workOrderTextBox.Text));
                            saveLabel.Text = @"Updated at:" + DateTime.Now.ToShortTimeString();
                            extendedStatusStrip.AddStatus(@"Successfully updated " + tasTextBox.Text + @" in your history.");
                            return true;
                        }
                        //Tas and workorder blank? Increment the count and insert a new one.
                        if (tasTextBox.Text == @"TAS00000" && workOrderTextBox.Text == @"")
                        {
                            saveData["TAS"] = saveData["TAS"] +
                                              CheckTas(saveData["TAS"], Properties.Settings.Default.SaveCount);
                            tasTextBox.Text = tasTextBox.Text + Properties.Settings.Default.SaveCount;
                            Sql.Insert("HISTORY", saveData);
                            saveLabel.Text = @"Saved at:" + DateTime.Now.ToShortTimeString();
                            extendedStatusStrip.AddStatus(@"Successfully inserted " + tasTextBox.Text + @" in your history.");
                            Properties.Settings.Default.SaveCount = Properties.Settings.Default.SaveCount + 1;
                            Properties.Settings.Default.Save();
                            return true;
                        }
                        if (tasTextBox.Text == @"TAS00000" && workOrderTextBox.Text != @"")
                        {
                            saveData["TAS"] = saveData["TAS"] +
                                              CheckTas(saveData["TAS"], Properties.Settings.Default.SaveCount);
                            tasTextBox.Text = tasTextBox.Text + Properties.Settings.Default.SaveCount;
                            Sql.Insert("HISTORY", saveData);
                            saveLabel.Text = @"Saved at:" + DateTime.Now.ToShortTimeString();
                            extendedStatusStrip.AddStatus(@"Successfully inserted " + tasTextBox.Text + @" in your history.");
                            Properties.Settings.Default.SaveCount = Properties.Settings.Default.SaveCount + 1;
                            Properties.Settings.Default.Save();
                            return true;
                        }

                        #endregion
                    }
                    catch (Exception ex)
                    {
                        extendedStatusStrip.AddStatus(@"Failed to save the data, refer to error log.");
                        ErrorOutput(ex.Message, @"SaveTicket", ex.Source);
                        return false;
                    }
                }
            }

            #endregion

            #region SaveSysLog

            if (Layout == 2)
            {
                Sql = new SqLiteDatabase(Properties.Settings.Default.SavePath + @"\GumshoeSPLG\SystemLogsDB.s3db");
                var saveData = new Dictionary<string, string>
                {
                    {"TICKET", ticketTextBoxSys.Text},
                    {"WORKORDER", workOrderTextBoxSys.Text},
                    {"ERROR", errorMessageTextBox.Text},
                    {"USERAGS", userAGSBoxSys.Text},
                    {"USERNAME", nameTextBoxSys.Text.Replace("'", "")},
                    {"CTAGS", agsTextBoxSys.Text},
                    {"CTNAME", ctNameTextBox.Text.Replace("'", "")},
                    {"RTC", rtcTextBoxSys.Text},
                    {"TASKCATEGORY", taskComboBoxSys.Text},
                    {"NOTES", rtbExtended.RichTextBox.Text},
                    {"DATE", DateTime.Now.Date.ToShortDateString()},
                    {"TIME", DateTime.Now.ToShortTimeString()}
                };
                if (
                    ticketTextBoxSys.Text.Equals(Sql.ExecuteScalar("SELECT TICKET FROM SYSLOGS WHERE ID=" + Common.rowID)))
                {
                    try
                    {
                        Sql.Update("SYSLOGS", saveData, String.Format("ID={0}", Common.rowID));
                        saveLabel.Text = @"Updated at: " + DateTime.Now.ToShortTimeString();
                        extendedStatusStrip.AddStatus(@"Successfully updated " + ticketTextBoxSys.Text + @"in your history.");
                        return true;
                    }
                    catch (Exception ex)
                    {
                        extendedStatusStrip.AddStatus(@"Failed to update " + ticketTextBoxSys.Text + @" in your history.");
                        ErrorOutput(ex.Message, @"SysLogSave", ex.Source);
                    }
                }
                else
                {
                    try
                    {
                        if (ticketTextBoxSys.Text != "" && !Sql.TicketExist(ticketTextBoxSys.Text))
                        {
                            Sql.Insert("SYSLOGS", saveData);
                            saveLabel.Text = @"Saved at:" + DateTime.Now.ToShortTimeString();
                            extendedStatusStrip.AddStatus(@"Successfully saved " + ticketTextBoxSys.Text + @" to your history.");
                            return true;
                        }
                        if (ticketTextBoxSys.Text != "" && Sql.TicketExist(ticketTextBoxSys.Text))
                        {
                            Sql.Update("SYSLOGS", saveData, "TICKET='" + ticketTextBoxSys.Text + "'");
                        }
                        if (ticketTextBoxSys.Text == "")
                        {
                            saveData["TICKET"] = Properties.Settings.Default.sysLogCount.ToString();
                            Sql.Insert("SYSLOGS", saveData);
                            Properties.Settings.Default.sysLogCount = Properties.Settings.Default.sysLogCount + 1;
                            Properties.Settings.Default.Save();
                            extendedStatusStrip.AddStatus(@"Successfully saved your ticket under TICKET: " + saveData["TICKET"]);
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        extendedStatusStrip.AddStatus(@"Failed to save your ticket: " + ticketTextBoxSys.Text);
                        ErrorOutput(ex.Message, @"SysLogSave", ex.Source);
                    }
                }
            }

            #endregion

            return false;
        }

        /// <summary> Finds the first avaliable TAS number </summary>
        private int CheckTas(string tas, int count)
        {
            int addedInt = count;
            while (true)
            {
                if (Sql.TasExist(tas + addedInt))
                {
                    Properties.Settings.Default.SaveCount++;
                    addedInt = Properties.Settings.Default.SaveCount;
                    continue;
                }
                break;
            }
            return addedInt;
        }

        /// <summary>  Registers all the hotkeys necesarry to run </summary>

        #region Hotkey Hook
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x0312)
            {
                switch (Properties.Settings.Default.layout)
                {
                    case (1):

                        #region CallsKeybinds

                        switch (m.WParam.ToInt32())
                        {
                            case (0):
                                if (connoteTextBox.Text != "")
                                {
                                    Clipboard.SetText(connoteTextBox.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (1):
                                if (tasTextBox.Text != "")
                                {
                                    Clipboard.SetText(tasTextBox.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (2):
                                if (workOrderTextBox.Text != "")
                                {
                                    Clipboard.SetText(workOrderTextBox.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (3):
                                if (ctTextBox.Text != "")
                                {
                                    Clipboard.SetText(ctTextBox.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (4):
                                if (nodeTextBox.Text != "")
                                {
                                    Clipboard.SetText(nodeTextBox.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (5):
                                if (pudoTextBox.Text != "")
                                {
                                    Clipboard.SetText(pudoTextBox.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (6):
                                if (faultySerialBox.Text != "")
                                {
                                    Clipboard.SetText(faultySerialBox.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (7):
                                if (faultyMPNBox.Text != "")
                                {
                                    Clipboard.SetText(faultyMPNBox.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (8):
                                if (spareSerialBox.Text != "")
                                {
                                    Clipboard.SetText(spareSerialBox.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (9):
                                if (spareMPNBox.Text != "")
                                {
                                    Clipboard.SetText(spareMPNBox.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (12):
                                return;
                            case (11):
                                CompleteRtc();
                                return;
                        }
                        break;

                        #endregion

                    case (2):

                        #region SysLogs

                        switch (m.WParam.ToInt32())
                        {
                            case (0):
                                if (taskComboBoxSys.SelectedIndex != -1)
                                {
                                    Clipboard.SetText(taskComboBoxSys.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (1):
                                if (ticketTextBoxSys.Text != "")
                                {
                                    Clipboard.SetText(ticketTextBoxSys.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (2):
                                if (workOrderTextBoxSys.Text != "")
                                {
                                    Clipboard.SetText(workOrderTextBoxSys.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (4):
                                if (errorMessageTextBox.Text != "")
                                {
                                    Clipboard.SetText(errorMessageTextBox.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (6):
                                if (userAGSBoxSys.Text != "")
                                {
                                    Clipboard.SetText(userAGSBoxSys.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (7):
                                if (nameTextBoxSys.Text != "")
                                {
                                    Clipboard.SetText(nameTextBoxSys.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (8):
                                if (agsTextBoxSys.Text != "")
                                {
                                    Clipboard.SetText(agsTextBoxSys.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (9):
                                if (ctNameTextBox.Text != "")
                                {
                                    Clipboard.SetText(ctNameTextBox.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (3):
                                if (rtcTextBoxSys.Text != "")
                                {
                                    Clipboard.SetText(rtcTextBoxSys.Text);
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (5):
                                if (dateTimePickerSys.Value.ToShortDateString() != "")
                                {
                                    Clipboard.SetText(dateTimePickerSys.Value.ToShortDateString());
                                    SendKeys.Send("^v");
                                }
                                return;
                            case (11):
                                CompleteRtc();
                                return;
                        }
                        break;
                }

                #endregion
            }
            base.WndProc(ref m);
        }

        #endregion

        #region Clipboard Methods

        //Highlights all the text within a textbox
        private static void Highlight_Text(TextBoxBase control)
        {
            control.SelectionStart = 0;
            control.SelectionLength = control.Text.Length;
        }

        // These methods are used for ctrl+clicking a text box to select it's values

        #region Click Methods

        private void tasTextBox_Click(object sender, MouseEventArgs e)
        {
            if (ModifierKeys != Keys.Control || tasTextBox.Text == "") return;
            if (tasTextBox.Text != null) Clipboard.SetText(tasTextBox.Text);
            Highlight_Text(tasTextBox);
        }

        private void workOrderTextBox_Click(object sender, EventArgs e)
        {
            if (ModifierKeys != Keys.Control || workOrderTextBox.Text == "") return;
            Clipboard.SetText(workOrderTextBox.Text);
            Highlight_Text(workOrderTextBox);
        }

        private void ctTextBox_Click(object sender, EventArgs e)
        {
            if (ModifierKeys != Keys.Control || ctTextBox.Text == "") return;
            Clipboard.SetText(ctTextBox.Text);
            Highlight_Text(ctTextBox);
        }

        private void nodeTextBox_Click(object sender, EventArgs e)
        {
            if (ModifierKeys != Keys.Control || nodeTextBox.Text == "") return;
            Clipboard.SetText(nodeTextBox.Text);
            Highlight_Text(nodeTextBox);
        }

        private void pudoTextBox_Click(object sender, EventArgs e)
        {
            if (ModifierKeys != Keys.Control || pudoTextBox.Text == "") return;
            Clipboard.SetText(pudoTextBox.Text);
            Highlight_Text(pudoTextBox);
        }

        private void faultySerialBox_Click(object sender, EventArgs e)
        {
            if (ModifierKeys != Keys.Control || faultySerialBox.Text == "") return;
            Clipboard.SetText(faultySerialBox.Text);
            Highlight_Text(faultySerialBox);
        }

        private void faultyMPNBox_Click(object sender, EventArgs e)
        {
            if (ModifierKeys != Keys.Control || faultyMPNBox.Text == "") return;
            Clipboard.SetText(faultyMPNBox.Text);
            Highlight_Text(faultyMPNBox);
        }

        private void spareSerialBox_Click(object sender, EventArgs e)
        {
            if (ModifierKeys != Keys.Control || spareSerialBox.Text == "") return;
            Clipboard.SetText(spareSerialBox.Text);
            Highlight_Text(spareSerialBox);
        }

        private void spareMPNBox_Click(object sender, EventArgs e)
        {
            if (ModifierKeys != Keys.Control || spareMPNBox.Text == "") return;
            Clipboard.SetText(spareMPNBox.Text);
            Highlight_Text(spareMPNBox);
        }

        private void connoteTextBox_Click(object sender, EventArgs e)
        {
            if (ModifierKeys != Keys.Control || connoteTextBox.Text == "") return;
            Clipboard.SetText(connoteTextBox.Text);
            Highlight_Text(connoteTextBox);
        }


        private void rtcTextBox_Click(object sender, EventArgs e)
        {
            if (ModifierKeys != Keys.Control || rtcTextBox.Text == "") return;
            Clipboard.SetText(rtcTextBox.Text);
            Highlight_Text(rtcTextBox);
        }

        #endregion

        #endregion

        #region StatusStrip

        private void statusStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            statusListBox.Visible = true;
            statusListBox.Focus();
        }

        private void statusListBox_MouseLeave(object sender, EventArgs e)
        {
            statusListBox.Visible = false;
        }

        private void statusStrip_MouseHover(object sender, EventArgs e)
        {
            statusListBox.Visible = true;
            statusListBox.Focus();
        }


        private void statusListBox_Leave(object sender, EventArgs e)
        {
            statusListBox.Visible = false;
        }

        #endregion

        #region RTC

        //Completes an RTC form using the avaliable details, this can get messy.
        private void CompleteRtc()
        {
            #region CallsRTC

            if (Layout == 1)
            {
                SendKeys.SendWait("^2");
                Thread.Sleep(300);
                SendTab(10);
                SendKeys.SendWait("{DOWN}");
                SendTab(2);
                SendKeys.SendWait("^2");
                Thread.Sleep(300);
                SendTab(3);
                SendKeys.Send("{DOWN}");
                SendKeys.Send("{ENTER}");
                Thread.Sleep(300);
                SendTab(2);
                SendKeys.SendWait("^1");
                Thread.Sleep(300);
                SendTab(3);
                SendKeys.SendWait("^8");
                Thread.Sleep(300);
                SendTab(2);
                SendKeys.SendWait("^7");
                SendKeys.SendWait("{TAB}");
                Thread.Sleep(300);
                SendKeys.SendWait("^6");
                SendKeys.SendWait("{TAB}");
                Thread.Sleep(300);
                SendKeys.SendWait("^5");
                SendKeys.SendWait("{TAB}");
                Thread.Sleep(300);
                SendKeys.SendWait("^4");
                SendKeys.SendWait("{TAB}");
                Thread.Sleep(300);
                SendKeys.SendWait("^3");
                SendTab(2);
                Thread.Sleep(300);
                SendKeys.SendWait("^9");
                SendTab(1);
                Thread.Sleep(400);
                Clipboard.SetText(courierCombo.Text);
                SendKeys.SendWait("^v");
                SendTab(1);
                SendKeys.SendWait("^0");
                SendTab(12);
                Thread.Sleep(500);
                SendKeys.SendWait("{ENTER}");
                Thread.Sleep(500);
                Clipboard.SetText("=====RAW NOTES=====" + Environment.NewLine + rtbExtended.RichTextBox.Text);
                Thread.Sleep(1500);
                SendKeys.SendWait("^v");
                for (int i = 0; i < 7; i++) SendKeys.SendWait("+{TAB}");
                SendKeys.SendWait("{ENTER}");
                Thread.Sleep(500);
                InputSimulator.SimulateKeyDown(VirtualKeyCode.CONTROL);
                InputSimulator.SimulateKeyPress(VirtualKeyCode.VK_S);
                InputSimulator.SimulateKeyUp(VirtualKeyCode.CONTROL);
                Thread.Sleep(5000);
                if (rtcTextBox.Text == "") rtcTextBox.Text = GetRTCId();
            }

            #endregion

            #region SysLogsRTC

            if (Layout == 2)
            {
                SendKeys.SendWait("^3");
                Thread.Sleep(200);
                SendTab(8);
                SendKeys.SendWait("{DOWN}");
                SendTab(2);
                SendKeys.SendWait("^2");
                Thread.Sleep(200);
                SendTab(11);
                Thread.Sleep(200);
                SendKeys.SendWait("^2");
                SendTab(1);
                Thread.Sleep(200);
                SendKeys.SendWait("^1");
                SendTab(2);
                switch (taskComboBoxSys.SelectedIndex)
                {
                    case 0:
                        SendKeys.SendWait("P");
                        break;
                    case 1:
                        SendKeys.SendWait("P");
                        Thread.Sleep(200);
                        SendKeys.SendWait("U");
                        break;
                    case 2:
                        break;
                }
                Thread.Sleep(400);
                SendTab(1);
                SendKeys.SendWait("^5");
                Thread.Sleep(200);
                SendTab(1);
                SendKeys.SendWait("^6");
                Thread.Sleep(200);
                SendTab(7);
                Thread.Sleep(500);
                SendKeys.SendWait("Workorder: ");
                Thread.Sleep(200);
                SendKeys.SendWait("^2");
                SendKeys.SendWait(Environment.NewLine);
                Thread.Sleep(200);
                SendKeys.SendWait("Date: ");
                Thread.Sleep(200);
                SendKeys.SendWait("^9");
                SendKeys.SendWait(Environment.NewLine);
                Thread.Sleep(200);
                SendKeys.SendWait("User: ");
                SendKeys.SendWait("^4");
                SendKeys.SendWait(" ");
                Thread.Sleep(200);
                SendKeys.SendWait("^5");
                Thread.Sleep(200);
                SendKeys.SendWait(Environment.NewLine);
                SendKeys.SendWait("CT: ");
                SendKeys.SendWait("^6");
                SendKeys.SendWait(" ");
                Thread.Sleep(200);
                SendKeys.SendWait("^7");
                Thread.Sleep(200);
                SendKeys.SendWait(Environment.NewLine);
                SendKeys.SendWait("RTC Call log ");
                Thread.Sleep(400);
                SendKeys.SendWait("^8");
                SendKeys.SendWait(Environment.NewLine);
                Clipboard.SetText(rtbExtended.RichTextBox.Text);
                Thread.Sleep(500);
                SendKeys.SendWait("^v");
            }

            #endregion
        }

        private string GetRTCId()
        {
            foreach (Process process in Process.GetProcesses())
            {
                if (!process.MainWindowTitle.Contains("Call Log")) continue;
                return process.MainWindowTitle.Split(' ')[2].Replace(":", "");
            }
            return "";
        }

        private void LoadRTC(string log)
        {
            Process.Start(
                "https://b23aciwas10.au.ibm.com:19443/jazz/web/projects/SPLG%20BAU#action=com.ibm.team.workitem.viewWorkItem&id=" +
                log);
        }


        private void rtcLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (rtcTextBox.Text != "") LoadRTC(rtcTextBox.Text);
        }


        private void rtcTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (Char) Keys.Enter) return;
            if (rtcTextBox.Text != "") LoadRTC(rtcTextBox.Text);
        }

        private static void SendTab(int amount)
        {
            for (int i = 0; i < amount; i++) SendKeys.SendWait("{TAB}");
        }

        // Call ags link clicked
        private void userLinkLabelSys_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SearchAgs(userAGSBoxSys.Text);
        }


        // Syslogs link clicked
        private void agsLinkSys_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                agsLookup.Show(MousePosition.X, MousePosition.Y);
                agsContextTextBox.Focus();
            }
            else SearchAgs(agsTextBoxSys.Text);
        }

        // If the syslogs AGS box length = 7 then automatically try search the name
        private void userAGSBoxSys_Leave(object sender, EventArgs e)
        {
            if (userAGSBoxSys.Text.Length == 7) SearchAgs(userAGSBoxSys.Text);
        }

        // If the syslogs AGS box length = 7 then automatically try search the name
        private void agsTextBoxSys_Leave(object sender, EventArgs e)
        {
            if (agsTextBoxSys.Text.Length == 7) SearchAgs(agsTextBoxSys.Text);
        }

        #endregion

        #region Misc Tab
        private void tabControlMisc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControlMisc.SelectedTab == tabAdder)
            {
                var newTab = new TabPage("New Tab") { Name = "newTab" };
                tabControlMisc.TabPages.Insert(tabControlMisc.TabCount - 1, newTab);
                tabAdder.TabIndex = tabControlMisc.TabCount;
                tabControlMisc.SelectTab(newTab);
                NewTab(newTab, Sql.CreateTab(newTab.Name), "");
            }
        }


        /// <summary> Constructor to create the controls in a new tab </summary>
        /// <param name="newTab">Tab to create in</param>
        /// <param name="id">Id of tab within SQLiteDB</param>
        /// <param name="rtbText">Text to fill the RTB with</param>
        private void NewTab(Control newTab, string id, string rtbText)
        {
            var newRtb = new RichTextBox
            {
                Name = newTab.Text,
                Dock = DockStyle.Fill,
                BorderStyle = BorderStyle.None,
                AcceptsTab = true,
                Text = rtbText
            };
            newTab.Controls.Add(newRtb);
            var idLabel = new Label
            {
                Text = id,
                Visible = false
            };
            newTab.Controls.Add(idLabel);
            newRtb.Leave += delegate
            {
                UpdateTab(id, newRtb.Text);
            };
        }

        /// <summary> Updates the stored text for the given tab </summary>
        /// <param name="id">ID of tab page</param>
        /// <param name="rtbText">Text to update with</param>
        private void UpdateTab(string id, string rtbText)
        {
            Sql.UpdateTab(int.Parse(id), rtbText);
        }

        /// <summary> Collects the tabs from the SQlite table and adds them and their content </summary>
        private void PopulateMiscTabs()
        {
            var tabDictionary = Sql.PopulateTabs();
            tabControlMisc.TabPages.Remove(tabAdder);
            foreach (var entry in tabDictionary)
            {
                var newTab = new TabPage(entry.Value.Key);
                tabControlMisc.TabPages.Add(newTab);
                NewTab(newTab, entry.Key.ToString(), entry.Value.Value);
            }
            tabControlMisc.TabPages.Add(tabAdder);
        }

        private void tabControlMisc_MouseUp(object sender, MouseEventArgs e)
        {
            var skipevent = false;
            if (e.Button != MouseButtons.Right) return;
            for (var i = 0; i < tabControlMisc.TabCount; i++)
            {
                if (tabControlMisc.TabPages[i].Text == @"    +") continue;
                var r = tabControlMisc.GetTabRect(i);
                if (!r.Contains(e.Location)) continue;
                tabControlMisc.SelectTab(i);
                var textBox = new TextBox
                {
                    Text = tabControlMisc.TabPages[i].Text,
                    Location = new Point(r.X + 4, r.Y + 5),
                    Width = r.Width,
                    BorderStyle = BorderStyle.None
                };
                tabMisc.Controls.Add(textBox);
                textBox.Focus();
                textBox.BringToFront();
                int i1 = i;
                textBox.Leave += delegate
                {
                    if (skipevent) return;
                    RenameTab(textBox, i1);
                };
                textBox.KeyPress += delegate(object o, KeyPressEventArgs args)
                {
                    if (args.KeyChar == (Char)Keys.Enter) RenameTab(textBox, i1);
                    if (args.KeyChar == (Char)Keys.Escape)
                    {
                        if (MessageBox.Show(@"This will delete this tab and all it's contents, are you sure?",
                            @"Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No) return;
                        skipevent = true;
                        Sql.RemoveTab(int.Parse(tabControlMisc.TabPages[i1].Controls[1].Text));
                        tabControlMisc.TabPages.Remove(tabControlMisc.TabPages[i1]);
                        tabMisc.Controls.Remove(textBox);
                    }
                };
            }
        }

        /// <summary> Renames the tab and removes the textbox </summary>
        /// <param name="textBox">Text box containing new name</param>
        /// <param name="index">Tab index to be renaming</param>
        private void RenameTab(Control textBox, int index)
        {
            tabControlMisc.TabPages[index].Text = textBox.Text;
            tabControlMisc.TabPages[index].Controls[0].Name = textBox.Text;
            Sql.RenameTab(int.Parse(tabControlMisc.TabPages[index].Controls[1].Text), textBox.Text);
            tabMisc.Controls.Remove(textBox);
        }
        #endregion

        //Autofill the TAS box when TAS is selected with TAS00000
        private void ticketTypeCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tasTextBox.Text == @"TAS00000" || tasTextBox.Text == "")
                tasTextBox.Text = ticketTypeCombo.Text == @"TAS" ? "TAS00000" : "";
        }

        /// <summary> Loads the template from file </summary>
        public void LoadTemplate()
        {
            try
            {
                rtbExtended.RichTextBox.LoadFile(Properties.Settings.Default.SavePath + "\\GumshoeSPLG\\Template.txt",
                    RichTextBoxStreamType.PlainText);
            }
            catch (Exception ex)
            {
                extendedStatusStrip.AddStatus(@"An error occured: could not load template");
                ErrorOutput(ex.Message, @"LoadTemplate", ex.Source);
            }
        }

        #region Locations, Kits, Addresses & Consumables

        //Populate locations from local file plants and SLOCs.xlsx
        private void PopulateLocations()
        {
            addressDGV.AutoGenerateColumns = false;
            //Sets the path to the local temp version of the spreadsheet, this is re-published
            //each time incase I update the spreadsheets
            var filePath = Directory.GetCurrentDirectory() + "\\datafiles\\plants and SLOCs.xlsx";
            try
            {
                var stream = File.Open(filePath, FileMode.Open, FileAccess.Read);

                var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                excelReader.IsFirstRowAsColumnNames = true;
                var ds = excelReader.AsDataSet();

                var sheet = ds.Tables[0];
                dgvLocations.DataSource = sheet;
                excelReader.Close();
            }
            catch (Exception ex)
            {
                extendedStatusStrip.AddStatus(@"Cannot find the locations file");
                ErrorOutput(ex.Message, @"PopulateLocations", ex.Source);
            }
        }

        private void PopulateKits()
        {
            //Sets the path to the local temp version of the spreadsheet, this is re-published
            //each time incase I update the spreadsheets
            string filePath = Directory.GetCurrentDirectory() + "\\datafiles\\warehouse in PSE.xlsx";
            try
            {
                FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
                IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                excelReader.IsFirstRowAsColumnNames = true;

                DataSet ds = excelReader.AsDataSet();

                DataTable sheet = ds.Tables[0];
                kitGrid.DataSource = sheet;
                excelReader.Close();
            }
            catch (Exception ex)
            {
                extendedStatusStrip.AddStatus(@"Cannot populate kits");
                ErrorOutput(ex.Message, @"PopulateKits", ex.Source);
            }
        }

        private void PopulateAddresses()
        {
            //Sets the path to the local temp version of the spreadsheet, this is re-published
            //each time incase I update the spreadsheets
            string filePath = Directory.GetCurrentDirectory() + "\\datafiles\\PUDOaddresses.XLSX";
            try
            {
                FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
                IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                excelReader.IsFirstRowAsColumnNames = true;
                DataSet ds = excelReader.AsDataSet();
                DataTable sheet = ds.Tables[0];
                addressDGV.DataSource = sheet;
                excelReader.Close();
            }
            catch (Exception ex)
            {
                extendedStatusStrip.AddStatus(@"Cannot populate the addresses");
                ErrorOutput(ex.Message, @"PopulateAddresses", ex.Source);
            }
        }

        private void PopulateConsumables()
        {
            string filePath = Directory.GetCurrentDirectory() + "\\datafiles\\Consumables.xlsx";
            try
            {
                FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
                IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                excelReader.IsFirstRowAsColumnNames = true;
                DataSet ds = excelReader.AsDataSet();
                DataTable sheet = ds.Tables[0];
                consumableDGV.DataSource = sheet;
                excelReader.Close();
            }
            catch (Exception ex)
            {
                extendedStatusStrip.AddStatus(@"Cannot populate the consumables");
                ErrorOutput(ex.Message, @"PopulateConsumables", ex.Source);
            }
        }

        #region SuppressKeys


        private void searchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (Suppress(e))
            {
                e.SuppressKeyPress = true;
            }
        }

        private static bool Suppress(KeyEventArgs e)
        {
            return e.KeyCode.Equals(Keys.OemQuotes) ||
                   e.KeyCode.Equals(Keys.Multiply) ||
                   e.KeyCode.Equals(Keys.OemOpenBrackets) ||
                   e.KeyCode.Equals(Keys.OemCloseBrackets) ||
                   e.Modifiers == Keys.Shift && e.KeyCode.Equals(Keys.D8);
        }

        #endregion

        #region CleanSearch

        private string CleanSearch(string input)
        {
            if (input.Contains("'")) return input.Replace("'", "");
            if (input.Contains("*")) return input.Replace("*", "");
            if (input.Contains("[")) return input.Replace("[", "");
            if (input.Contains("]")) return input.Replace("]", "");
            return input;
        }

        #endregion

        private void addressDGV_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right) return;
            foreach (DataGridViewCell cell in addressDGV.SelectedCells)
                cell.Selected = false;

            addressDGV.Rows[e.RowIndex].Selected = true;

            addressesContextMenu.Show(MousePosition);
        }

        #endregion

        //Populates the objects with data from the SQLDB according to the ID
        public void PopulateCall()
        {
            if (!UnsavedData() && saveLabel.Text == @"Saved at:")
            {
                var result = MessageBox.Show(@"You have unsaved changes, would you like to save?", @"Save?",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button3);
                if (result == DialogResult.Yes)
                {
                    SaveTicket();
                    toolTip.SetToolTip(menuStrip1, @"Saved today: " + Sql.SavedToday());
                }
                if (result == DialogResult.Cancel)
                {
                    return;
                }
            }
            Sql = new SqLiteDatabase(Properties.Settings.Default.SavePath + @"\GumshoeSPLG\HistoryDB.s3db");
            int i = Common.rowID;
            ticketTypeCombo.Text = Sql.ExecuteScalar("SELECT SYSTEM FROM HISTORY WHERE ID=" + i);
            tasTextBox.Text = Sql.ExecuteScalar("SELECT TAS FROM HISTORY WHERE ID=" + i);
            workOrderTextBox.Text = Sql.ExecuteScalar("SELECT WORKORDER FROM HISTORY WHERE ID=" + i);
            ctTextBox.Text = Sql.ExecuteScalar("SELECT CT FROM HISTORY WHERE ID=" + i);
            nodeTextBox.Text = Sql.ExecuteScalar("SELECT NODE FROM HISTORY WHERE ID=" + i);
            pudoTextBox.Text = Sql.ExecuteScalar("SELECT PUDO FROM HISTORY WHERE ID=" + i);
            faultyMPNBox.Text = Sql.ExecuteScalar("SELECT FAULTYMPN FROM HISTORY WHERE ID=" + i);
            faultySerialBox.Text = Sql.ExecuteScalar("SELECT FAULTYSERIAL FROM HISTORY WHERE ID=" + i);
            spareMPNBox.Text = Sql.ExecuteScalar("SELECT SPAREMPN FROM HISTORY WHERE ID=" + i);
            spareSerialBox.Text = Sql.ExecuteScalar("SELECT SPARESERIAL FROM HISTORY WHERE ID=" + i);
            courierCombo.Text = Sql.ExecuteScalar("SELECT COURIER FROM HISTORY WHERE ID=" + i);
            rtcTextBox.Text = Sql.ExecuteScalar("SELECT RTC FROM HISTORY WHERE ID=" + i);
            connoteTextBox.Text = Sql.ExecuteScalar("SELECT CONNOTE FROM HISTORY WHERE ID=" + i);
            rtbExtended.RichTextBox.Text = Sql.ExecuteScalar("SELECT SUMMARY FROM HISTORY WHERE ID=" + i);
            saveLabel.Text = @"Saved at:" + Sql.ExecuteScalar("SELECT TIME FROM HISTORY WHERE ID=" + i);
            rtbExtended.RichTextBox.Rtf = rtbExtended.RichTextBox.Rtf.Replace("`", "'");
            foreach (var gb in Controls.OfType<GroupBox>())
            {
                foreach (var tb in gb.Controls.Cast<Control>().Where(tb => tb is TextBox && tb.Text.Contains("`")))
                {
                    tb.Text = tb.Text.Replace("`", "'");
                }
                foreach (var tb in from gb2 in gb.Controls.OfType<GroupBox>() from Control tb in gb2.Controls where tb is TextBox && tb.Text.Contains("`") select tb)
                {
                    tb.Text = tb.Text.Replace("`", "'");
                }
            }
            if (ticketTypeCombo.Text == "") ticketTypeCombo.Text = @"TAS";
        }

        //Populates the objects with data from the SQLDB according to the ID
        public void PopulateSysLog()
        {
            Sql = new SqLiteDatabase(Properties.Settings.Default.SavePath + @"\GumshoeSPLG\SystemLogsDB.s3db");
            int i = Common.rowID;
            ticketTextBoxSys.Text = Sql.ExecuteScalar("SELECT TICKET FROM SYSLOGS WHERE ID=" + i);
            workOrderTextBoxSys.Text = Sql.ExecuteScalar("SELECT WORKORDER FROM SYSLOGS WHERE ID=" + i);
            errorMessageTextBox.Text = Sql.ExecuteScalar("SELECT ERROR FROM SYSLOGS WHERE ID=" + i);
            userAGSBoxSys.Text = Sql.ExecuteScalar("SELECT USERAGS FROM SYSLOGS WHERE ID=" + i);
            nameTextBoxSys.Text = Sql.ExecuteScalar("SELECT USERNAME FROM SYSLOGS WHERE ID=" + i);
            agsTextBoxSys.Text = Sql.ExecuteScalar("SELECT CTAGS FROM SYSLOGS WHERE ID=" + i);
            ctNameTextBox.Text = Sql.ExecuteScalar("SELECT CTNAME FROM SYSLOGS WHERE ID=" + i);
            rtcTextBoxSys.Text = Sql.ExecuteScalar("SELECT RTC FROM SYSLOGS WHERE ID=" + i);
            switch (Sql.ExecuteScalar("SELECT TASKCATEGORY FROM SYSLOGS WHERE ID=" + i))
            {
                case ("Parts Actual"):
                    taskComboBoxSys.SelectedIndex = 0;
                    break;
                case ("Putaway"):
                    taskComboBoxSys.SelectedIndex = 1;
                    break;
                case ("Unassigned"):
                    taskComboBoxSys.SelectedIndex = 2;
                    break;
            }
            rtbExtended.RichTextBox.Text = Sql.ExecuteScalar("SELECT NOTES FROM SYSLOGS WHERE ID=" + i);
            saveLabel.Text = @"Saved at: " + Sql.ExecuteScalar("SELECT TIME FROM SYSLOGS WHERE ID=" + i);
            rtbExtended.RichTextBox.Rtf = rtbExtended.RichTextBox.Rtf.Replace("`", "'");
        }

        /// <summary> Handles the context menu when right clicking the Notes RTB </summary>

        #region rtbExtended
        private void rtbExtended_RichTextBox_MouseDown(object sender, MouseEventArgs e)
        {
            bool enableItems = (rtbExtended.RichTextBox.SelectionLength != 0);
            if (e.Button == MouseButtons.Right)
            {
                rtbRightClick.Items["copyToolStripMenuItem"].Enabled = enableItems;
                rtbRightClick.Items["cutToolStripMenuItem"].Enabled = enableItems;
                rtbRightClick.Items["deleteToolStripMenuItem"].Enabled = enableItems;
                rtbRightClick.Items["pasteToolStripMenuItem"].Enabled = Clipboard.ContainsText();
                rtbRightClick.Items["undoToolStripMenuItem"].Enabled = rtbExtended.RichTextBox.CanUndo;
                rtbRightClick.Show(MousePosition);
            }
        }

        //Prevent pasting custom text formats
        private void richTextBoxExtended1_RichTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (rtbExtended.RichTextBox.Text.Contains(@"mario") || rtbExtended.RichTextBox.Text.Contains(@"Mario"))
            {
                if (e.KeyCode != Keys.Back) _preChangeWidth = Width;
                Width = 615;
                hiddenMarioBox.Visible = true;
            }
            else
            {
                if (_preChangeWidth > 0) Width = _preChangeWidth;
                hiddenMarioBox.Visible = false;
            }
            if (!e.Control || e.KeyCode != Keys.V) return;
            ((RichTextBox) sender).Paste(DataFormats.GetFormat("Text"));
            e.Handled = true;
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbExtended.RichTextBox.Undo();
            rtbExtended.RichTextBox.ClearUndo();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (rtbExtended.RichTextBox.SelectionLength > 0)
            {
                rtbExtended.RichTextBox.Cut();
            }
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (rtbExtended.RichTextBox.SelectionLength > 0)
            {
                rtbExtended.RichTextBox.Copy();
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int start = rtbExtended.RichTextBox.SelectionStart;
            int end = rtbExtended.RichTextBox.SelectionLength;
            if (rtbExtended.RichTextBox.SelectionLength > 0)
            {
                rtbExtended.RichTextBox.Text = rtbExtended.RichTextBox.Text.Remove(start, end);
            }
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IDataObject dataObject = Clipboard.GetDataObject();
            if (dataObject != null && dataObject.GetDataPresent(DataFormats.Text))
            {
                rtbExtended.RichTextBox.Paste();
            }
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (rtbExtended.RichTextBox.Text.Length > 0)
            {
                rtbExtended.RichTextBox.SelectAll();
            }
        }

        #endregion

        #region AGSLookup

        //Method to replace the first instance of a string
        private static string ReplaceFirst(string text, string search, string replace)
        {
            int pos = text.IndexOf(search, StringComparison.Ordinal);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }

        /// <summary> Populates the listobject with the dictionary keys and values. </summary>
        public void PopulateAgsHistory()
        {
            agsListView.Items.Clear();

            foreach (var dict in _agsList)
            {
                string name = "", surname = "";
                string[] index = dict.Value.Split(' ');
                if (index.Length > 0)
                {
                    name = index[0];
                    surname = index[1];
                }
                agsListView.Items.Insert(0, CultureInfo.CurrentCulture.TextInfo.ToTitleCase(name.ToLower() + " " + surname.Remove(1)))
                    .SubItems.Add(dict.Key);
                if (agsListView.Items.Count == 9) agsListView.Items.RemoveAt(8);
                
            }

            if (agsListView.Visible)
            {
                agsListView.Visible = false;
                agsListView.Visible = true;
            }
        }

        //Formats the AGS text box contents accordingly
        private void agsLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (e.Button != MouseButtons.Left || ctTextBox.Text == "") return;
            FormatAgs(ctTextBox.Text);
            miscTab.SelectedTab = personDetails;
        }

        /// <summary>
        ///     Formats the AGS from long to short by replacing the first number and trimming the end off
        /// </summary>
        private void FormatAgs(string ags)
        {
            string origAgs = ags;
            if (ags.Length == 8)
            {
                if (ags.StartsWith("2"))
                {
                    ags = ReplaceFirst(ags, "2", "B");
                }
                if (ags.StartsWith("3"))
                {
                    ags = ReplaceFirst(ags, "3", "C");
                }
                if (ags.StartsWith("4"))
                {
                    ags = ReplaceFirst(ags, "4", "D");
                }
                //These might exist one day..
                if (ags.StartsWith("5"))
                {
                    ags = ReplaceFirst(ags, "5", "E");
                }
                if (ags.StartsWith("6"))
                {
                    ags = ReplaceFirst(ags, "6", "F");
                }
                if (ags.StartsWith("7"))
                {
                    ags = ReplaceFirst(ags, "7", "G");
                }
                if (ags.Length > 1)
                {
                    ags = ags.Substring(0, ags.Length - 1);
                }
                else
                {
                    extendedStatusStrip.AddStatus(@"Error while formatting the AGS");
                    return;
                }
                SearchAgs(ags);
                if (ctTextBox.Text == origAgs || ctTextBox.Text == "") ctTextBox.Text = ags;
            }
            else
            {
                SearchAgs(ags);
                if (ctTextBox.Text == origAgs || ctTextBox.Text == "") ctTextBox.Text = ags;
            }
        }

        /// <summary>z Searches the given formatted AGS on people search, parses the values from the table and puts them in a DGV </summary>
        /// <param name="agsText">shortened AGS</param>
        private void SearchAgs(string agsText)
        {
            int errorInt = 1;
            if (!personDetailsDGV.Visible)
            {
                personDetailsDGV.Visible = true;
                personSearchDGV.Visible = false;
                firstNameTextBox.Text = "";
                surNameTextBox.Text = "";
            }
            personDetailsDGV.Rows.Clear();
            if (ctTextBox.Text.StartsWith("N"))
            {
                extendedStatusStrip.AddStatus(@"These AGS don't actually exist, I'm not sure why they appear in some places...");
                return;
            }
            string webUrl = "http://peoplesearch.in.telstra.com.au:8094/peoplesearch/userdetail.aspx?BaseDN=CN=" +
                            agsText + ",OU=People,OU=eProfile,DC=PeopleSearch,DC=Telstra,DC=Com";
            var web = new HtmlWeb();
            HtmlDocument doc = null;
            try
            {
                doc = web.Load(webUrl);
            }
            catch (WebException webex)
            {
                ErrorOutput(webex.Message, @"SearchAGS", webex.Source);
            }
            catch (Exception ex)
            {
                ErrorOutput(ex.Message, @"SearchAGS", ex.Source);
            }
            for (int rowCount = 1; rowCount < 14; rowCount++)
            {
                try
                {
                    var enumerate = doc.DocumentNode.SelectNodes("//tbody/tr[@class='lftAlign']");
                    if (enumerate == null) return;
                    try
                    {
                        string contactDetails =
                            doc.DocumentNode.SelectSingleNode("//tbody/tr[@class='lftAlign'][" + rowCount +
                                                              "]/th[@class='hghCell lftalign']").InnerText;
                        string contactValue =
                            doc.DocumentNode.SelectSingleNode("//tbody/tr[@class='lftAlign'][" + rowCount + "]/td")
                                .InnerText;
                        if (contactDetails.Trim() == "Name" && Properties.Settings.Default.layout == 2)
                        {
                            if (userAGSBoxSys.Text == agsText) nameTextBoxSys.Text = contactValue.Trim();
                            if (agsTextBoxSys.Text == agsText) ctNameTextBox.Text = contactValue.Trim();
                        }
                        personDetailsDGV.Rows.Add(contactDetails.Trim(), contactValue.Trim());
                        if (contactDetails.Trim() == "Name")
                        {
                            var newKvp = new KeyValuePair<string, string>(agsText, contactValue.Trim());
                            if (_agsList.Contains(newKvp))
                            {
                                _agsList.Remove(newKvp);
                                _agsList.Add(newKvp);
                                //_agsDictionary.Add(agsText, contactValue.Trim());
                                
                            }
                            else
                            {
                                _agsList.Add(newKvp);
                            }
                            PopulateAgsHistory();
                        }
                    }
                    catch (HtmlWebException ex)
                    {
                        extendedStatusStrip.AddStatus(@"Failed to collect the details for this AGS");
                        ErrorOutput(ex.Message, @"SearchAGS", ex.Source);
                    }
                    catch (TimeoutException)
                    {
                        extendedStatusStrip.AddStatus(@"Timed out while attempting to pull data");
                    }
                    catch (Exception ex)
                    {
                        ErrorOutput(ex.Message, @"SearchAGS", ex.Source);
                    }
                }
                catch (HtmlWebException ex)
                {
                    extendedStatusStrip.AddStatus(@"A web exception has occured, refer to error log.");
                    ErrorOutput(ex.Message, @"SearchAGS", ex.Source);
                }
                catch (Exception ex)
                {
                    if (errorInt == 1) extendedStatusStrip.AddStatus(@"Failed to load website, is Network Connect running?");
                    ErrorOutput(ex.Message, @"SearchAGS", ex.Source);
                    errorInt = 2;
                }
            }
        }

        //Search person button on the person tab
        private void searchPeopleButton_Click(object sender, EventArgs e)
        {
            personSearchDGV.Visible = true;
            personDetailsDGV.Visible = false;
            SearchPerson(firstNameTextBox.Text, surNameTextBox.Text, locationDropDown.Text, postCodeTextBox.Text);
        }

        //toggles the visible DGV.
        private void backButton_Click(object sender, EventArgs e)
        {
            personSearchDGV.Visible = !personSearchDGV.Visible;
            personDetailsDGV.Visible = !personDetailsDGV.Visible;
        }

        //Clicking a cell after a search will show the detailed view of that result
        private void personSearchDGV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;
            FormatAgs(personSearchDGV.Rows[e.RowIndex].Cells[3].Value.ToString());
        }


        private void postCodeTextBox_Enter(object sender, EventArgs e)
        {
            if (!postCodeTextBox.Text.Equals("Postcode")) return;
            postCodeTextBox.Text = "";
            postCodeTextBox.ForeColor = Color.Black;
        }

        private void postCodeTextBox_Leave(object sender, EventArgs e)
        {
            if (!postCodeTextBox.Text.Equals("")) return;
            postCodeTextBox.Text = @"Postcode";
            postCodeTextBox.ForeColor = Color.Gray;
        }

        /// <summary>
        ///     Queries the Telstra people search website with a first/last name search and puts results in a DGV
        /// </summary>
        /// <param name="firstname">first name search</param>
        /// <param name="surname">surname search</param>
        /// <param name="location">State code</param>
        /// <param name="postcode">Post code</param>
        private void SearchPerson(string firstname, string surname, string location, string postcode)
        {
            personSearchDGV.Rows.Clear();
            linkShowResults.Visible = false;
            personSearchDGV.Height = 352;
            if (postcode == "Postcode") postcode = "";
            string webUrl =
                String.Format(
                    "http://peoplesearch.in.telstra.com.au:8094/peoplesearch/index.aspx?givenname={0}&surname={1}&location={2} {3}",
                    firstname, surname, location, postcode);
            var web = new HtmlWeb();
            HtmlDocument doc = web.Load(webUrl);
            try
            {
                HtmlNodeCollection tables = doc.DocumentNode.SelectNodes("//table[@class='dTbl']//tr");
                HtmlNode tableCaption =
                    doc.DocumentNode.SelectSingleNode("//table[@class='dTbl'][1]/caption[@class='dTblCaption']");
                if (tableCaption == null)
                {
                    personSearchDGV.Rows.Add("No results found.", "", "");
                    return;
                }
                if (tableCaption.InnerText.Trim() != "Search Results")
                {
                    personSearchDGV.Visible = false;
                    personDetailsDGV.Visible = true;
                    FormatAgs(
                        doc.DocumentNode.SelectSingleNode("//table[@class='dTbl'][1]/tbody/tr[@class='lftAlign'][2]/td")
                            .InnerText);
                    return;
                }
                if (tables == null)
                {
                    personSearchDGV.Rows.Add(@"No results found.", "");
                    return;
                }
                for (int i = 1; i < tables.Count; i++)
                {
                    string nameSearchVal =
                        doc.DocumentNode.SelectSingleNode("//table[@class='dTbl']//tr[" + i + "]/td[1]").InnerText;
                    string locationSearchVal =
                        doc.DocumentNode.SelectSingleNode("//table[@class='dTbl']//tr[" + i + "]/td[5]").InnerText;
                    string phoneSearchVal =
                        doc.DocumentNode.SelectSingleNode("//table[@class='dTbl']//tr[" + i + "]/td[6]").InnerText;
                    string employeeSearchVal =
                        doc.DocumentNode.SelectSingleNode("//table[@class='dTbl']//tr[" + i + "]/td[7]").InnerText;
                    personSearchDGV.Rows.Add(
                        string.Join(", ", nameSearchVal.Split(',').Select(s => s.Trim()).Reverse()).Replace(",", ""),
                        locationSearchVal, phoneSearchVal, employeeSearchVal);
                }
            }
            catch (HtmlWebException ex)
            {
                extendedStatusStrip.AddStatus(String.Format("Failed to search for {0} {1}", firstname, surname));
                ErrorOutput(ex.Message, @"SearchPerson", ex.Source);
            }
            if (personSearchDGV.RowCount == 25)
            {
                linkShowResults.Visible = true;
                personSearchDGV.Height = 334;
                linkShowResults.Click += delegate
                {
                    Process.Start(webUrl);
                };
            }
        }

        /// <summary>
        ///     Accepts enter to search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ctTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (!(ctTextBox.Text.Length > 0 & e.KeyCode == Keys.Enter)) return;
            FormatAgs(ctTextBox.Text);
            miscTab.SelectedTab = personDetails;
        }

        // Moves values of mobile/name to your notes 
        private void personDetailsDGV_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left || ModifierKeys != Keys.Control || e.RowIndex < 0 || e.ColumnIndex == 0) return;

            string cellValue = personDetailsDGV.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            if (e.RowIndex == 0)
            {
                int nameLocation = rtbExtended.RichTextBox.Find("Name: ");
                rtbExtended.RichTextBox.Text = nameLocation > -1
                    ? rtbExtended.RichTextBox.Text.Insert(nameLocation + 6, cellValue)
                    : rtbExtended.RichTextBox.Text.Insert(rtbExtended.RichTextBox.TextLength,
                        Environment.NewLine + cellValue);
            }
            if (e.RowIndex != 9) return;
            int mobileLocation = rtbExtended.RichTextBox.Find("Mobile: ");
            rtbExtended.RichTextBox.Text = mobileLocation > -1
                ? rtbExtended.RichTextBox.Text.Insert(mobileLocation + 8, cellValue)
                : rtbExtended.RichTextBox.Text.Insert(rtbExtended.RichTextBox.TextLength,
                    Environment.NewLine + cellValue);
        }


        private void firstNameTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (char) Keys.Enter) return;
            personSearchDGV.Visible = true;
            personDetailsDGV.Visible = false;
            SearchPerson(firstNameTextBox.Text, surNameTextBox.Text, locationDropDown.Text, postCodeTextBox.Text);
        }

        private void surNameTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (char) Keys.Enter) return;
            personSearchDGV.Visible = true;
            personDetailsDGV.Visible = false;
            SearchPerson(firstNameTextBox.Text, surNameTextBox.Text, locationDropDown.Text, postCodeTextBox.Text);
        }

        #region AGS Text Box/Area

        private void ctTextBox_Enter(object sender, EventArgs e)
        {
            if (_agsList.Count > 0) agsListView.Visible = true;
        }

        private void ctTextBox_Leave(object sender, EventArgs e)
        {
            if (agsListView.Focused) return;
            if (agsListView.Visible) agsListView.Visible = false;
            
        }

        private void agsListView_VisibleChanged(object sender, EventArgs e)
        {
            if (agsListView.Items.Count > 8) return;
            int height = _agsList.Count*17;
            if (agsListView.Visible) agsListView.Size = new Size(117, height);
        }

        private void agsLink_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                agsLookup.Show(MousePosition.X, MousePosition.Y);
                agsContextTextBox.Text = "";
                agsContextTextBox.Focus();
            }
            else if (ctTextBox.Text.Length > 0)
            {
                FormatAgs(ctTextBox.Text);
            }
        }

        private void agsContextTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != (char) Keys.Enter || agsContextTextBox.Text.Length <= 0) return;
            FormatAgs(agsContextTextBox.Text);
            miscTab.SelectedTab = personDetails;
            agsLookup.Hide();
        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (agsContextTextBox.Text.Length <= 0) return;
            FormatAgs(agsContextTextBox.Text);
            miscTab.SelectedTab = personDetails;
            agsLookup.Hide();
        }

        private void agsListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (agsListView.SelectedItems.Count != 1) return;
            ctTextBox.Text = agsListView.SelectedItems[0].SubItems[1].Text;
            SearchAgs(agsListView.SelectedItems[0].SubItems[1].Text);
            agsListView.Visible = false;
            miscTab.SelectTab(personDetails);
        }

        #endregion

        #region AGS Context Menu

        private void agsBoxContextMenu_Opening(object sender, CancelEventArgs e)
        {
            agsBoxContextMenu.Items["Undo"].Enabled = ctTextBox.CanUndo;

            if (ctTextBox.SelectedText.Length == 0)
            {
                agsBoxContextMenu.Items["Cut"].Enabled = false;
                agsBoxContextMenu.Items["Copy"].Enabled = false;
                agsBoxContextMenu.Items["Delete"].Enabled = false;
            }
            else
            {
                agsBoxContextMenu.Items["Cut"].Enabled = true;
                agsBoxContextMenu.Items["Copy"].Enabled = true;
                agsBoxContextMenu.Items["Delete"].Enabled = true;
            }

            agsBoxContextMenu.Items["Paste"].Enabled = Clipboard.ContainsText();

            agsBoxContextMenu.Items["SelectAll"].Enabled = ctTextBox.Text.Length != 0;

            agsBoxContextMenu.Items["NewTicket"].Enabled = ctTextBox.Text.Length != 0;
        }

        private void Undo_Click(object sender, EventArgs e)
        {
            ctTextBox.Undo();
        }

        private void Cut_Click(object sender, EventArgs e)
        {
            ctTextBox.Cut();
        }

        private void Copy_Click(object sender, EventArgs e)
        {
            ctTextBox.Copy();
        }

        private void Paste_Click(object sender, EventArgs e)
        {
            ctTextBox.Paste();
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            int selectionIndex = ctTextBox.SelectionStart;
            int selectionCount = ctTextBox.SelectionLength;
            ctTextBox.Text = ctTextBox.Text.Remove(selectionIndex, selectionCount);
            ctTextBox.SelectionStart = selectionIndex;
        }

        private void SelectAll_Click(object sender, EventArgs e)
        {
            ctTextBox.SelectAll();
        }

        private void NewTicket_Click(object sender, EventArgs e)
        {
            string ags = ctTextBox.Text;
            if (!UnsavedData()) SaveTicket();
            toolTip.SetToolTip(menuStrip1, @"Saved today: " + Sql.SavedToday());
            SetDefaultValues();
            _extraSpare = 2;
            _extraFaulty = 2;
            ctTextBox.Text = ags;
        }

        #endregion

        #endregion

        #region Additional Parts

        //Copies the additional part details to the clipboard if you're holding CTRL + clicking on it.
        private void additionalMPN_Click(object sender, EventArgs e)
        {
            if (ModifierKeys != Keys.Control || additionalMPN.Text == "") return;
            Clipboard.SetText(additionalMPN.Text);
            Highlight_Text(additionalMPN);
        }

        //Copies the additional part details to the clipboard if you're holding CTRL + clicking on it.
        private void additionalSerial_Click(object sender, EventArgs e)
        {
            if (ModifierKeys != Keys.Control || additionalSerial.Text == "") return;
            Clipboard.SetText(additionalSerial.Text);
            Highlight_Text(additionalSerial);
        }

        /// <summary> Adds the part to the rich text box and increments the number of spares </summary>
        private void additionalAddbutton_Click(object sender, EventArgs e)
        {
            string partType = "";
            if (additionalFaultradio.Checked)
            {
                partType = "Faulty " + _extraFaulty;
                _extraFaulty++;
            }
            if (additionalSpareradio.Checked)
            {
                partType = "Spare " + _extraSpare;
                _extraSpare++;
            }
            switch (Properties.Settings.Default.additionalLines)
            {
                case (1):
                    rtbExtended.RichTextBox.AppendText(Environment.NewLine + partType + " - " + "Serial: " +
                                                       additionalSerial.Text + " MPN: " + additionalMPN.Text);
                    break;
                case (3):
                    rtbExtended.RichTextBox.AppendText(Environment.NewLine + Environment.NewLine + partType +
                                                       Environment.NewLine + "Serial: " + additionalSerial.Text +
                                                       Environment.NewLine + "MPN: " + additionalMPN.Text);
                    break;
            }

            additionalSerial.Text = "";
            additionalMPN.Text = "";
        }

        #endregion

        /// <summary> Checks if there is any unsaved data before quiting </summary>
        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!UnsavedData() && saveLabel.Text == @"Saved at:")
            {
                DialogResult result = MessageBox.Show(@"You have unsaved changes, would you like to save?", @"Save?",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button3);
                if (result == DialogResult.Yes)
                {
                    if (SaveTicket())
                    {
                        toolTip.SetToolTip(menuStrip1, @"Saved today: " + Sql.SavedToday());
                        Dispose();
                    }
                }
                if (result == DialogResult.No) Dispose();
                if (result == DialogResult.Cancel) e.Cancel = true;
            }
        }

        /// <summary> Toggles the courier if there's text in the connote box or not </summary>
        private void connoteTextBox_TextChanged(object sender, EventArgs e)
        {
            if (connoteTextBox.Text != "") courierCombo.Text = @"AAE";
            if (connoteTextBox.Text == "") courierCombo.Text = @"Toll";
        }

        /// <summary> Adds tooltips to the items in the statusListBox so if they're too long you can read it on the tooltip </summary>
        private void statusListBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (!(sender is ListBox)) return;
            var listBox = (ListBox) sender;
            var point = new Point(e.X, e.Y);
            int hoverIndex = listBox.IndexFromPoint(point);
            if (hoverIndex >= 0 && hoverIndex < listBox.Items.Count &&
                toolTip.GetToolTip(listBox) != listBox.Items[hoverIndex].ToString())
            {
                toolTip.SetToolTip(listBox, listBox.Items[hoverIndex].ToString());
            }
        }

        private void hotkeyLabel_MouseHover(object sender, EventArgs e)
        {
            hotkeyLabel.Visible = false;
        }

        private void hotkeyLabel_MouseLeave(object sender, EventArgs e)
        {
            hotkeyLabel.Visible = true;
        }

        private void hotkeyCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            hotkeysToolStripMenuItem.Checked = hotkeyCheckBox.Checked;
            Properties.Settings.Default.Hotkeys = hotkeyCheckBox.Checked;
            hotkeyLabel.Visible = hotkeyCheckBox.Checked;
        }

        // Populates the Misc text box from the on-going misc.txt file
        private void PopulateMisc()
        {
            var miscPath = Properties.Settings.Default.SavePath + "\\GumshoeSPLG\\Misc.txt";
            try
            {
                if (File.Exists(miscPath))
                {
                    Sql.CreateTab("Text", File.ReadAllText(miscPath));
                    File.Delete(miscPath);
                }
            }
            catch (Exception ex)
            {
                extendedStatusStrip.AddStatus(@"An error occured, refer to error log.");
                ErrorOutput(ex.Message, @"LoadMisc", ex.Source);
            }
        }

        private void RandomJudgement()
        {
            var faceArray = new[]
            {
                "ლ(ಠ益ಠლ)", "( ͡° ͜ʖ ͡°)", "̿̿ ̿̿ ̿'̿'̵͇̿̿з=༼ ▀̿̿Ĺ̯̿̿▀̿ ̿ ༽", "(づ｡◕‿‿◕｡)づ",
                "ᕦ(ò_óˇ)ᕤ", "ヽ༼ຈل͜ຈ༽ﾉ", "【＝◈︿◈＝】", "ᄽὁȍ ̪ őὀᄿ",
                "Ƹ̵̡Ӝ̵̨̄Ʒ", "┬─┬﻿ ︵ /(.□. ）", "(¬‿¬)", "╚(ಠ_ಠ)=┐",
                "┐(‘～` )┌  ", "ヾ(⌐■_■)ノ♪", "Beau for president"
            };
            judgementLabel.Text = faceArray[(new Random().Next(1, 100) - 1)/7];
            judgementLabel.Location = new Point((Width - judgementLabel.Width) - 20, judgementLabel.Location.Y);
        }

        // These are not the droids you're looking for...
        private void Main_ResizeEnd(object sender, EventArgs e)
        {
            if (Width > 800) judgementLabel.Visible = true;
            if (Width < 500) judgementLabel.Visible = false;

            //Shhh, it's a secret to everyone...
            if (Width >= 1000 && nodeTextBox.Text == @"GAME" && pudoTextBox.Text == @"GAME") SecretStart();
            if (Width < 1000 || nodeTextBox.Text != @"GAME" || pudoTextBox.Text != @"GAME") SecretPause();
        }

        private void newTicketLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (e.Button != MouseButtons.Left) return;
            var ags = ctTextBox.Text;
            if (!UnsavedData()) SaveTicket();
            toolTip.SetToolTip(menuStrip1, @"Saved today: " + Sql.SavedToday());
            SetDefaultValues();
            _extraSpare = 2;
            _extraFaulty = 2;
            ctTextBox.Text = ags;
        }

        #region Shhh, it's a secret to everyone...

        private Point _mouseStartLoc;

        private void SecretStart()
        {
            secretGroupBox.Visible = true;
        }

        private void SecretPause()
        {
            secretGroupBox.Visible = false;
        }

        private void happyLabel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                secretGroupBox.Focus();

                _mouseStartLoc = e.Location;
            }
        }

        private void happyLabel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                int tempX = happyLabel.Left + e.X - _mouseStartLoc.X;

                tempX = Math.Max(tempX, 1);
                tempX = Math.Min(tempX, secretGroupBox.Width - happyLabel.Width - 1);

                happyLabel.Left = tempX;
            }
        }

        #endregion

        private void Main_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
            toolStripDragHere.Visible = true;
        }

        private void Main_DragDrop(object sender, DragEventArgs e)
        {
            var data = e.Data.GetData("Text", true);
            if (data.ToString().Length <= 20) return;
            importToolStripTextBox.Text = data.ToString();
            toolStripDragHere.Visible = false;
        }

        private void Main_DragLeave(object sender, EventArgs e)
        {
            toolStripDragHere.Visible = false;
        }

        private void pudoTextBox_TextChanged(object sender, EventArgs e)
        {
            if (pudoTextBox.Text.Length != 4) return;
            for (var i = 0; i < _restrictedSLOC.Rows.Count; i++)
            {
                if (_restrictedSLOC.Rows[i]["Stor. Loc. Descr."].ToString() == "PUDO - " + pudoTextBox.Text)
                {
                    MessageBox.Show(@"This PuDo is listed as restricted, please confirm before continuing",
                                    @"Restricted PuDo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void linkPod_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var tempRTF = rtbExtended.RichTextBox.Rtf;
            tempRTF = tempRTF.Remove(tempRTF.Length - 5);

            rtbExtended.RichTextBox.Rtf = String.Format("{0}{1}{2}", tempRTF, POD_TEXT, @"}");
        }

        #region DataTab

        private void labelClearSearch_MouseEnter(object sender, EventArgs e)
        {
            labelClearSearch.ForeColor = Color.Black;
        }

        private void labelClearSearch_MouseLeave(object sender, EventArgs e)
        {
            labelClearSearch.ForeColor = SystemColors.ControlDark;
        }

        private void labelClearSearch_Click(object sender, EventArgs e)
        {
            textBoxSearch.Text = String.Empty;
        }

        private void comboDataSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBoxSearch.Text = String.Empty;
            _filterBSource.RemoveFilter();
            switch (comboDataSelect.SelectedIndex)
            {
                case (0):
                    tabControlData.SelectTab(tabLocations);
                    break;
                case (1):
                    tabControlData.SelectTab(tabKits);
                    break;
                case (2):
                    tabControlData.SelectTab(tabAddresses);
                    break;
                case (3):
                    tabControlData.SelectTab(tabConsumables);
                    break;
            }
        }

        //Filter the selected data grid by it's relevant search.
        private void searchBox_TextChanged(object sender, EventArgs e)
        {
            textBoxSearch.Text = CleanSearch(textBoxSearch.Text);
            labelClearSearch.Visible = textBoxSearch.Text.Length > 0;
            if (textBoxSearch.Text.Length == 0) _filterBSource.RemoveFilter();
            switch (comboDataSelect.SelectedIndex)
            {
                case (0):
                    _filterBSource = new BindingSource
                    {
                        DataSource = dgvLocations.DataSource,
                        Filter = "`Stor. Loc. Descr.` like '%" + textBoxSearch.Text + "%' OR Plnt like '%" +
                                 textBoxSearch.Text + "%' OR `SLoc` like '%" +
                                 textBoxSearch.Text + "%' OR `Search Term 1` like '%" + textBoxSearch.Text + "%'"
                    };
                    dgvLocations.DataSource = _filterBSource;
                    break;
                case (1):
                    _filterBSource = new BindingSource
                    {
                        DataSource = kitGrid.DataSource,
                        Filter = "SLOC like '%" + textBoxSearch.Text + "%' OR " +
                                 "WHN like '%" + textBoxSearch.Text + "%' OR " +
                                 "KIT like '%" + textBoxSearch.Text + "%' OR " +
                                 "DESC like '%" + textBoxSearch.Text + "%' OR " +
                                 "NODE like '%" + textBoxSearch.Text + "%'"
                    };
                    kitGrid.DataSource = _filterBSource;
                    break;
                case (2):
                    _filterBSource = new BindingSource
                    {
                        DataSource = addressDGV.DataSource,
                        Filter = "Plnt like '%" + textBoxSearch.Text + "%' OR " +
                                 "SLoc like '%" + textBoxSearch.Text + "%' OR " +
                                 "Name like '%" + textBoxSearch.Text + "%' OR " +
                                 "'House No.' like '%" + textBoxSearch.Text + "%' OR " +
                                 "Street like '%" + textBoxSearch.Text + "%' OR " +
                                 "City like '%" + textBoxSearch.Text + "%' OR " +
                                 "`Postl Code` like '%" + textBoxSearch.Text + "%' OR " +
                                 "Rg like '%" + textBoxSearch.Text + "%' OR " +
                                 "`Search Term 1` like '%" + textBoxSearch.Text + "%'"
                    };
                    addressDGV.DataSource = _filterBSource;
                    break;
                case (3):
                    _filterBSource = new BindingSource
                    {
                        DataSource = consumableDGV.DataSource,
                        Filter = "Material like '%" + textBoxSearch.Text + "%' OR " +
                                 "Description like '%" + textBoxSearch.Text + "%'"
                    };
                    consumableDGV.DataSource = _filterBSource;
                    break;
            }
        }

        #endregion

        
    }
}