﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GumshoeSPLG.Disaster_Recovery
{
    public partial class SparesNoFaulties : Form
    {
        private BindingSource bngSource = new BindingSource();
        private DrSqlWrapper _sql = new DrSqlWrapper();

        public SparesNoFaulties()
        {
            InitializeComponent();

            dgvSpareNoFaulty.AutoGenerateColumns = false;
            dgvSpareNoFaulty.DataSource = bngSource;
        }

        public void Build()
        {
            bngSource.DataSource = SparesList;

            foreach (DataGridViewRow row in dgvSpareNoFaulty.Rows)
            {
                var faultyComboCell = ((DataGridViewComboBoxCell)row.Cells["dgvSpareNoFaultyFaulty"]);

                if (FaultiesList.Count <= 0)
                {
                    faultyComboCell.ReadOnly = true;

                    //((DataGridViewComboBoxCell)row.Cells["dgvSpareNoFaultyReturnReason"]).Items.Remove("Swapped");
                    row.Cells["dgvSpareNoFaultyReturnReason"].Value = "Return Unused";
                }
                else
                {
                    faultyComboCell.Items.Add(String.Empty);

                    foreach (var item in FaultiesList)
                    {
                        faultyComboCell.Items.Add(item.ToString());
                    }

                    row.Cells["dgvSpareNoFaultyReturnReason"].Value = "Swapped";
                }
            }
        }

        public List<SparePart> SparesList { get; set; }

        public List<FaultyPart> FaultiesList { get; set; }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Dispose();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            var markedSpares = new List<SparePart>();

            foreach (DataGridViewRow row in dgvSpareNoFaulty.Rows)
            {
                var spare = (SparePart)row.DataBoundItem;

                spare.ReturnReason = row.Cells["dgvSpareNoFaultyReturnReason"].Value.ToString();

                if (spare.ReturnReason == "Swapped")
                {
                    var itemNumber = -1;
                    if (row.Cells["dgvSpareNoFaultyFaulty"].Value != null && Int32.TryParse(row.Cells["dgvSpareNoFaultyFaulty"].Value.ToString().Split(',')[0], out itemNumber))
                    {
                        spare.ItemNumber = itemNumber;
                    }
                    else
                    {
                        MessageBox.Show(String.Format("Spare {0} is marked as Swapped but has no selected faulty.{1}{1}Please select a faulty to replace it or give it a different Return Reason", spare.ToString(), Environment.NewLine), "Swap Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    spare.ReturnToAddress = _sql.GetReturnToAddress(spare.MPN);
                }

                markedSpares.Add(spare);
            }

            DialogResult = DialogResult.OK;
            //No need to throw any event here, as the spares are changed directly
            Dispose();
        }
    }
}