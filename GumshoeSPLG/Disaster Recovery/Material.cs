﻿namespace GumshoeSPLG
{
    public class Material
    {
        private string _source;
        private string _repairable;
        private string _returnto;

        public int Id { get; set; }

        public string MPN { get; set; }

        public string Source
        {
            get
            {
                return _source == "200" ? "Vendor" : "Local";
            }
            set
            {
                _source = value;
            }
        }

        public string Repairable
        {
            get
            {
                return _repairable == "003" ? "false" : "true";
            }
            set { _repairable = value; }
        }

        public string ReturnTo
        {
            get
            {
                switch (_returnto)
                {
                    case ("300"):
                        return "External Vendor";
                    case ("250"):
                        return "IBM SC Sydney";
                    case ("150"):
                        return "IBM SC Melbourne";
                    case ("100"):
                        return "CS Melbourne";
                }
                return string.Empty;
            }
            set
            {
                _returnto = value;
            }
        }

        public string Vendor { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }

    }
}
