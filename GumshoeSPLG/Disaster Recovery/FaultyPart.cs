﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace GumshoeSPLG
{
    public class FaultyPart
    {
        private char[] mpn = new char[0]; //20
        private char[] serial = new char[0]; //18
        private char[] mitsBarcode = new char[0]; //40
        private char[] installNode = new char[0]; //4

        public int ItemNumber { get; set; }
        public string MPN
        {
            get
            {
                if (mpn.Length <= 0) return String.Empty;
                return new string(mpn);
            }
            set
            {
               if (value.Length > 40)
                    MessageBox.Show(String.Format("MPN {0} is too long for SPS, please keep at 40 characters or under", value));
                else
                    mpn = value.ToCharArray();
            }
        }
        public string SerialNumber
        {
            get
            {
                if (serial.Length <= 0) return String.Empty;
                return new string(serial);
            }
            set
            {
                if (value.Contains(','))
                    MessageBox.Show(String.Format("Serial Number {0} contains a comma, which is an invalid character for a serial number", value));
                else if (value.Length > 18)
                    MessageBox.Show(String.Format("Serial Number {0} is too long for SPS, please keep at 18 characters or under", value));
                else
                    serial = value.ToCharArray();
            }
        }
        public string MITSBarcode
        {
            get
            {
                if (mitsBarcode.Length <= 0) return String.Empty;
                return new string(mitsBarcode);
            }
            set
            {
                if (value.Contains(','))
                    MessageBox.Show(String.Format("MITS Barcode {0} contains a comma, which is an invalid character for a MITS Barcode", value));
                else if (value.Length > 40)
                    MessageBox.Show(String.Format("MITS Barcode {0} is too long for MITS, please keep at 40 characters or under", value));
                else
                    mitsBarcode = value.ToCharArray();
            }
        }
        public string InstallNode
        {
            get
            {
                if (installNode.Length <= 0) return String.Empty;
                return new string(installNode);
            }
            set
            {
                if (value.Length != 4)
                    MessageBox.Show(String.Format("Install Node codes must be 4 characters long, received {0}", value));
                else
                    installNode = value.ToCharArray();
            }
        }
        public string InstallLocation { get; set; }
        public string Connote { get; set; }
        public string ReturnToAddress { get; set; }

        public string RTCLogText
        {
            get
            {
                return String.Format("Item Number: {1}{0}" +
                                     "MPN: {2}{0}" +
                                     "Serial: {3}{0}" +
                                     "Barcode: {4}{0}" +
                                     "Install Location: {5} {6}{0}" +
                                     "Connote: {7}{0}" + 
                                     "Return To: {8}{0}",
                                     Environment.NewLine,
                                     ItemNumber,
                                     MPN,
                                     SerialNumber,
                                     MITSBarcode,
                                     InstallNode,
                                     InstallLocation,
                                     Connote,
                                     ReturnToAddress);
            }
        }

        public FaultyPart()
        {

        }

        public override string ToString()
        {
            return String.Format("{0}, MPN: {1}, Serial: {2}", ItemNumber, MPN, SerialNumber);
        }
    }
}
