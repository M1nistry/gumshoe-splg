﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms.VisualStyles;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System;

namespace GumshoeSPLG
{
    public class DrSqlWrapper
    {
        private MySqlConnectionStringBuilder _connection;

        DataTable dtParts = new DataTable("partsTable");

        public DrSqlWrapper()
        {
            _connection = new MySqlConnectionStringBuilder();
            _connection.Server = "10.72.0.126";
            _connection.UserID = "IBM";
            _connection.Password = "h79ZAWMERENyfwpP";
            _connection.Database = "splgdisasterrecovery";
            _constring = _connection.ToString();

            DataTableSetup();
        }

        public void DataTableSetup()
        {
            //Gets all required columns for the part/location datatable
            using (var connection = new MySqlConnection(_constring))
            {
                //We use idPart=1 to grab just one item from the db, as that's all we need
                const string queryParts =
                    @"SELECT p1.*, l1.* FROM `parts` p1 " +
                    @"JOIN `locations` l1 ON p1.idLocation = l1.idLocation WHERE idPart=1;";

                connection.Open();
                using (var cmd = new MySqlCommand(queryParts, connection))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        for (var i = 0; i < reader.FieldCount; i++)
                        {
                            if (dtParts.Columns.Contains(reader.GetName(i))) continue;
                            dtParts.Columns.Add(reader.GetName(i));
                        }
                    }
                }
            }
        }

        public string _constring { get; set; }

        //public List<SparePart> PartsDataTable()
        //{
        //    var listParts = new List<SparePart>();
        //    using (var connection = new MySqlConnection(_constring))
        //    {
        //        const string queryParts =
        //            @"SELECT parts.*, locations.* FROM `parts` JOIN `locations` ON parts.idLocation = locations.idLocation;";
        //        connection.Open();
        //        using (var cmd = new MySqlCommand(queryParts, connection))
        //        {
        //            using (var reader = cmd.ExecuteReader())
        //            {
        //                for (var i = 0; i < reader.FieldCount; i++)
        //                {
        //                    if (dtParts.Columns.Contains(reader.GetName(i))) continue;
        //                    dtParts.Columns.Add(reader.GetName(i));
        //                }

        //                while (reader.Read())
        //                {
        //                    var newPart = new SparePart();
        //                    newPart.ID = reader.GetInt32("idPart");
        //                    newPart.SerialNumber = reader.GetString("serial_number");
        //                    newPart.MPN = reader.GetString("mpn");
        //                    newPart.Barcode = reader.GetString("mits_barcode");
        //                    newPart.MPNDescription = reader.GetString("mpn_description");
        //                    newPart.BinDescription = reader.GetString("bin_description");
        //                    newPart.Manufacturer = reader.GetString("manufacturer");
        //                    newPart.ReservingDocument = reader.GetString("reserving_document");
        //                    newPart.ReservingItem = reader.GetString("reserving_item");
        //                    newPart.ConfigParameters = reader.GetString("config_parameters");
        //                    newPart.Plant = reader.GetString("plant");
        //                    newPart.PlantName = reader.GetString("plant_name");
        //                    newPart.Sloc = reader.GetString("sloc");
        //                    newPart.SlocDescription = reader.GetString("sloc_description");
        //                    newPart.Node = reader.GetString("node");
        //                    newPart.IDAddress = reader.GetInt32("idAddress");
        //                    newPart.IDLocation = reader.GetInt32("idLocation");
        //                    listParts.Add(newPart);
        //                }
        //            }
        //        }
        //        return listParts;
        //    }
        //}

        public Dictionary<StoragePart, string> StorageLocations(string node)
        {
            var dictStorage = new Dictionary<StoragePart, string>();
            using (var connection = new MySqlConnection(_constring))
            {
                const string queryStorage = @"SELECT p1.idPart, p1.serial_number, p1.mpn, p1.bin_description, p1.mits_barcode, p1.mpn_description FROM `parts` p1 JOIN `locations` l1 ON l1.idLocation = p1.idLocation WHERE l1.node=@node;";
                connection.Open();
                using (var cmd = new MySqlCommand(queryStorage, connection))
                {
                    cmd.Parameters.AddWithValue("node", node);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            dictStorage.Add(new StoragePart(int.Parse(reader["idPart"].ToString()), reader["serial_number"].ToString(), reader["mpn"].ToString(), 
                                    reader["bin_description"].ToString(), reader["mits_barcode"].ToString(), 
                                    reader["mpn_description"].ToString()), reader["bin_description"].ToString());
                        }
                    }
                }
                return dictStorage;
            }
        }

        internal List<SparePart> GetPartData(string serial, string mpn, string node, string other)
        {
            var dtParts = this.dtParts.Clone();
            var listParts = new List<SparePart>();
            using (var connection = new MySqlConnection(_constring))
            {
                string queryParts =
                    @"SELECT p1.*, l1.* FROM `parts` p1 " + 
                    @"JOIN `locations` l1 ON p1.idLocation = l1.idLocation " +
                    @"WHERE ";

                var bAND = false;

                if (serial.Length > 0)
                {
                    queryParts += "(p1.serial_number=@serial)";
                    bAND = true;
                }

                if (mpn.Length > 0)
                {
                    queryParts += (bAND ? " AND " : "") + "(p1.mpn LIKE @mpn OR p1.mpn=@mpnFull)";
                    bAND = true;
                }

                if (node.Length > 0)
                {
                    queryParts += (bAND ? " AND " : "") + "(l1.node=@node)";
                    bAND = true;
                }

                if (other.Length > 0)
                {
                    queryParts += (bAND ? " AND " : "") + 
                        //Text is contained in the middle
                        "(`mpn_description` LIKE @otherMid OR " +
                        "`manufacturer` LIKE @otherMid OR " +
                        "`mits_barcode` LIKE @otherMid OR " +
                        "`bin_description` LIKE @otherMid OR " +
                        "`reserving_document` LIKE @otherMid OR " +
                        "`reserving_item` LIKE @otherMid OR " +
                        "`config_parameters` LIKE @otherMid OR " + 
                        //Text is at the start
                        "`mpn_description` LIKE @otherStart OR " +
                        "`manufacturer` LIKE @otherStart OR " +
                        "`mits_barcode` LIKE @otherStart OR " +
                        "`bin_description` LIKE @otherStart OR " +
                        "`reserving_document` LIKE @otherStart OR " +
                        "`reserving_item` LIKE @otherStart OR " +
                        "`config_parameters` LIKE @otherStart OR " +
                        //Text is at the end
                        "`mpn_description` LIKE @otherEnd OR " +
                        "`manufacturer` LIKE @otherEnd OR " +
                        "`mits_barcode` LIKE @otherEnd OR " +
                        "`bin_description` LIKE @otherEnd OR " +
                        "`reserving_document` LIKE @otherEnd OR " +
                        "`reserving_item` LIKE @otherEnd OR " +
                        "`config_parameters` LIKE @otherEnd);";
                }

                connection.Open();
                using (var cmd = new MySqlCommand(queryParts, connection))
                {
                    cmd.Parameters.AddWithValue("@serial", serial);
                    cmd.Parameters.AddWithValue("@mpn", mpn + ":%");
                    cmd.Parameters.AddWithValue("@mpnFull", mpn);
                    cmd.Parameters.AddWithValue("@node", node);
                    cmd.Parameters.AddWithValue("@otherMid", "%" + other + "%");
                    cmd.Parameters.AddWithValue("@otherStart", other + "%");
                    cmd.Parameters.AddWithValue("@otherEnd", "%" + other);

                    using (var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            var newPart = new SparePart
                            {
                                ID = reader.GetInt32("idPart"),
                                SerialNumber = reader.GetString("serial_number"),
                                MPN = reader.GetString("mpn"),
                                Barcode = reader.GetString("mits_barcode"),
                                MPNDescription = reader.GetString("mpn_description"),
                                BinDescription = reader.GetString("bin_description"),
                                Manufacturer = reader.GetString("manufacturer"),
                                ReservingDocument = reader.GetString("reserving_document"),
                                ReservingItem = reader.GetString("reserving_item"),
                                ConfigParameters = reader.GetString("config_parameters"),
                                Plant = reader.GetString("plant"),
                                PlantName = reader.GetString("plant_name"),
                                Sloc = reader.GetString("sloc"),
                                SlocDescription = reader.GetString("sloc_description"),
                                Node = reader.GetString("node"),
                                IDAddress = reader.GetInt32("idAddress"),
                                IDLocation = reader.GetInt32("idLocation")
                            };
                            listParts.Add(newPart);
                        }
                    }
                }
                return listParts;
            }
        }

        public string GetBin(string serial, string mpn)
        {
            using (var connection = new MySqlConnection(_constring))
            {
                const string selectBin =
                    @"SELECT bin_description FROM `parts` WHERE serial_number=@serial AND mpn=@mpn;";
                connection.Open();
                using (var cmd = new MySqlCommand(selectBin, connection))
                {
                    cmd.Parameters.AddWithValue("serial", serial);
                    cmd.Parameters.AddWithValue("mpn", mpn);
                    return cmd.ExecuteScalar().ToString();
                }
            }
        }

        public DataTable VendorTable()
        {
            var dtVendors = new DataTable("vendors");
            var cols = new []
            {
                new DataColumn("Id"), new DataColumn("MPN"),
                new DataColumn("Source"), new DataColumn("Repairable"),
                new DataColumn("ReturnTo"), new DataColumn("Vendor"), new DataColumn("Address"),
                new DataColumn("Telephone") 
            };
            dtVendors.Columns.AddRange(cols);
            using (var connection = new MySqlConnection(_constring))
            {
                const string queryVendors = @"SELECT s1.*, v1.* FROM `sourcefromdata` s1 JOIN `vendors` v1" +
                                            @" ON s1.idVendor = v1.idVendor";
                connection.Open();
                using (var cmd = new MySqlCommand(queryVendors, connection))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var newMaterial = new Material
                            {
                                Id = reader.GetInt32("idSourceData"),
                                MPN = reader.GetString("material"),
                                Source = reader.GetString("source_from"),
                                Repairable = reader.GetString("repairable_or_consumable"),
                                ReturnTo = reader.GetString("return_to"),
                                Vendor = reader.GetString("name"),
                                Address = reader.GetString("address"),
                                Telephone = reader.GetString("telephone")
                            };
                            dtVendors.Rows.Add(newMaterial.Id, newMaterial.MPN, newMaterial.Source,
                                newMaterial.Repairable, newMaterial.ReturnTo, newMaterial.Vendor,
                                newMaterial.Address, newMaterial.Telephone);
                        }
                    }
                }
            }
            return dtVendors;
        }

        public string FaultPuDo(string faultNode)
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    const string queryPuDo = @"SELECT a1.pudo_node_code FROM `node_pudo` n1 JOIN `addresses` a1 ON n1.pudo_sloc = a1.sloc WHERE n1.node=@faultnode";
                    connection.Open();
                    using (var cmd = new MySqlCommand(queryPuDo, connection))
                    {
                        cmd.Parameters.AddWithValue("@faultnode", faultNode);
                        var val = cmd.ExecuteScalar();
                        if (val != null) return val.ToString();
                    }
                }
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
                return "";
            }
            return "";
        }

        public string PudoDesc(string node)
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    const string queryDesc = @"SELECT name FROM `addresses` WHERE pudo_node_code=@node AND name like '%exchange%'";
                    connection.Open();
                    using (var cmd = new MySqlCommand(queryDesc, connection))
                    {
                        cmd.Parameters.AddWithValue("node", node);
                        var val = cmd.ExecuteScalar();
                        if (val != null) return val.ToString();
                    }
                }
            }
            catch (MySqlException)
            {
                return "";
            }
            return "";
        }

        internal int AddChange(string changeType, string ticketNumber, string taskID, string systemID, string workOrder, string comments)
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    const string addQuery = "INSERT INTO changes (change_type, ticket_number, task_id, system_id, work_order, comments) VALUES (@changeType, @ticketNumber, @taskID, @systemID, @workOrder, @comments);";
                    const string getID = "SELECT LAST_INSERT_ID() FROM changes;";

                    connection.Open();

                    using (var cmd = new MySqlCommand(addQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@changeType", changeType);
                        cmd.Parameters.AddWithValue("@ticketNumber", ticketNumber);
                        cmd.Parameters.AddWithValue("@taskID", taskID);
                        cmd.Parameters.AddWithValue("@systemID", systemID);
                        cmd.Parameters.AddWithValue("@workOrder", workOrder);
                        cmd.Parameters.AddWithValue("@comments", comments);

                        cmd.ExecuteNonQuery();

                        cmd.CommandText = getID;

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                                return reader.GetInt32(0);
                            else
                            {
                                MessageBox.Show("Unable to locate change after inserting it!  You should never see this error, so if you do, contact Gumshoe!");
                                return -1;
                            }
                        }
                    }
                }
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("Error in [AddChange]{0}Message: {1}{0}InnerException: {2}{0}, StackTrace: {3}{0}", Environment.NewLine, sqlEx.Message, sqlEx.InnerException, sqlEx.Message);
                return -1;
            }
        }

        internal void AddFaultyChange(object idChange, FaultyPart item)
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    const string addQuery = "INSERT INTO faulty_changes (idChange, item_number, mpn, serial_number, mits_barcode, install_node, install_location, connote) VALUES (@idChange, @itemNumber, @mpn, @serialNumber, @mitsBarcode, @installNode, @installLocation, @connote);";

                    connection.Open();

                    using (var cmd = new MySqlCommand(addQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@idChange", idChange);
                        cmd.Parameters.AddWithValue("@itemNumber", item.ItemNumber);
                        cmd.Parameters.AddWithValue("@mpn", item.MPN);
                        cmd.Parameters.AddWithValue("@serialNumber", item.SerialNumber);
                        cmd.Parameters.AddWithValue("@mitsBarcode", item.MITSBarcode);
                        cmd.Parameters.AddWithValue("@installNode", item.InstallNode);
                        cmd.Parameters.AddWithValue("@installLocation", item.InstallLocation);
                        cmd.Parameters.AddWithValue("@connote", item.Connote);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("Error in [AddFaultyChange]{0}Message: {1}{0}InnerException: {2}{0}, StackTrace: {3}{0}", Environment.NewLine, sqlEx.Message, sqlEx.InnerException, sqlEx.Message);
            }
        }

        internal void AddSpareChange(object idChange, SparePart item)
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    const string addQuery = "INSERT INTO spare_changes (idPart, idChange, item_number, new_bin_description, new_node, connote, foob, return_unused) VALUES (@idPart, @idChange, @itemNumber, @newBinDesc, @newnode, @connote, @foob, @returnUnused);";

                    connection.Open();

                    using (var cmd = new MySqlCommand(addQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@idPart", item.ID);
                        cmd.Parameters.AddWithValue("@idChange", idChange);
                        cmd.Parameters.AddWithValue("@itemNumber", item.ItemNumber);
                        cmd.Parameters.AddWithValue("@newBinDesc", item.NewBinLocation);
                        cmd.Parameters.AddWithValue("@newnode", item.NewNodeLocation);
                        cmd.Parameters.AddWithValue("@connote", item.Connote);
                        cmd.Parameters.AddWithValue("@foob", item.ReturnReason == "Faulty out of Box");
                        cmd.Parameters.AddWithValue("@returnUnused", item.ReturnReason == "Return Unused");

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("Error in [AddSpareChange]{0}Message: {1}{0}InnerException: {2}{0}, StackTrace: {3}{0}", Environment.NewLine, sqlEx.Message, sqlEx.InnerException, sqlEx.Message);
            }
        }

        internal void UpdateRTCLogNumber(int idChange, string rtcLogNum)
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    const string updateQuery = "UPDATE changes SET rtc_log=@rtcLogNum WHERE idChange=@idChange;";

                    connection.Open();

                    using (var cmd = new MySqlCommand(updateQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@idChange", idChange);
                        cmd.Parameters.AddWithValue("@rtcLogNum", rtcLogNum);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("Error in [UpdateRTCLogNumber]{0}Message: {1}{0}InnerException: {2}{0}, StackTrace: {3}{0}", Environment.NewLine, sqlEx.Message, sqlEx.InnerException, sqlEx.Message);
            }
        }

        internal string GetReturnToAddress(string mpn)
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    const string selectQuery = "SELECT v1.name, v1.address, s1.repairable_or_consumable FROM vendors v1 " + 
                                               "JOIN sourcefromdata s1 ON v1.idVendor=s1.idVendor " + 
                                               "WHERE s1.material=@mpn;";

                    connection.Open();

                    using (var cmd = new MySqlCommand(selectQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@mpn", mpn);

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                if (reader.GetString("repairable_or_consumable") != "003")
                                    return String.Format("{0} - {1}", reader.GetString("name"), reader.GetString("address"));
                                else
                                    return String.Format("Scrap at Site  (if Returned Unused: {0} - {1})", reader.GetString("name"), reader.GetString("address"));
                            }
                            else
                            {
                                MessageBox.Show(String.Format("Could not find return to address for MPN: {0}", mpn), "Return To Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return String.Empty;
                            }
                        }
                    }
                }
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("Error in [GetReturnToAddress]{0}Message: {1}{0}InnerException: {2}{0}, StackTrace: {3}{0}", Environment.NewLine, sqlEx.Message, sqlEx.InnerException, sqlEx.Message);
                return String.Empty;
            }
        }

        internal bool IsValidMPN(string givenMPN, out string fullMPN)
        {
            fullMPN = String.Empty;

            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    const string selectQuery = "SELECT material FROM sourcefromdata WHERE material=@mpn OR material LIKE @mpnPart;";

                    connection.Open();

                    using (var cmd = new MySqlCommand(selectQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@mpn", givenMPN);
                        cmd.Parameters.AddWithValue("@mpnPart", String.Format("{0}:%", givenMPN));

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                fullMPN = reader.GetString(0);
                                return true;
                            }
                            else
                                return false;
                        }
                    }
                }
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("Error in [IsValidMPN]{0}Message: {1}{0}InnerException: {2}{0}, StackTrace: {3}{0}", Environment.NewLine, sqlEx.Message, sqlEx.InnerException, sqlEx.Message);
                return false;
            }
        }

        internal bool CheckScrappable(string mpn)
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    const string selectQuery = "SELECT repairable_or_consumable FROM sourcefromdata WHERE material=@mpn OR material LIKE @mpnPart;";

                    connection.Open();

                    using (var cmd = new MySqlCommand(selectQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@mpn", mpn);
                        cmd.Parameters.AddWithValue("@mpnPart", String.Format("{0}:%", mpn));

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                if (reader.GetString(0) == "003")
                                    return true;
                            }

                            return false;
                        }
                    }
                }
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("Error in [CheckScrappable]{0}Message: {1}{0}InnerException: {2}{0}, StackTrace: {3}{0}", Environment.NewLine, sqlEx.Message, sqlEx.InnerException, sqlEx.Message);
                return false;
            }
        }

        internal int CheckSpareChanges(int idPart)
        {
            using (var connection = new MySqlConnection(_constring))
            {
                const string queryChange = @"SELECT idChange FROM `spare_changes` WHERE idPart=@id";
                connection.Open();
                using (var cmd = new MySqlCommand(queryChange, connection))
                {
                    cmd.Parameters.AddWithValue("id", idPart);
                    var val = cmd.ExecuteScalar();
                    if (val != null) return int.Parse(val.ToString());
                }
            }
            return -1;
        }

        internal string GetChangeDetails(int changeId)
        {
            using (var connection = new MySqlConnection(_constring))
            {
                const string queryChange = @"SELECT ticket_number, task_id, work_order, rtc_log FROM `changes` WHERE idChange=@id";
                connection.Open();
                using (var cmd = new MySqlCommand(queryChange, connection))
                {
                    cmd.Parameters.AddWithValue("id", changeId);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var retStr = String.Format("Ticket: {0}, Task: {1}, Reference: {2}, RTC: {3}",
                                reader.GetString("ticket_number"), reader.GetString("task_id"),
                                reader.GetString("work_order"), reader.GetString("rtc_log"));
                            return retStr;
                        }
                    }
                }
            }
            return "Error Occured";
        }

        internal bool GetSourceFromVendor(string mpn)
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    const string selectQuery = "SELECT source_from FROM sourcefromdata WHERE material=@mpn OR material LIKE @mpnPart;";

                    connection.Open();

                    using (var cmd = new MySqlCommand(selectQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@mpn", mpn);
                        cmd.Parameters.AddWithValue("@mpnPart", String.Format("{0}:%", mpn));

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                if (reader.GetString(0) == "200")
                                    return true;
                            }

                            return false;
                        }
                    }
                }
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("Error in [CheckScrappable]{0}Message: {1}{0}InnerException: {2}{0}, StackTrace: {3}{0}", Environment.NewLine, sqlEx.Message, sqlEx.InnerException, sqlEx.Message);
                return false;
            }
        }

        internal bool CheckCSAndIBMSC(string mpn, out int idPart, out string store)
        {
            idPart = -1;
            store = String.Empty;

            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    const string selectQuery = "SELECT p1.idPart, l1.node FROM `parts` p1 JOIN `locations` l1 ON p1.idLocation=l1.idLocation  WHERE (l1.node='IBMSCSYD' OR l1.node='IBMSCMEL' OR l1.node='MU2L') AND (p1.mpn=@mpn OR p1.mpn LIKE @mpnPart);";

                    connection.Open();

                    using (var cmd = new MySqlCommand(selectQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@mpn", mpn);
                        cmd.Parameters.AddWithValue("@mpnPart", String.Format("{0}:%", mpn));

                        using (var reader = cmd.ExecuteReader())
                        {
                            while(reader.Read())
                            {
                                if (CheckAvailable(reader.GetInt32("idPart")))
                                {
                                    idPart = reader.GetInt32("idPart");

                                    switch (reader.GetString("node"))
                                    {
                                        case "MU2L":
                                            store = "Mulgrave";
                                            break;
                                        case "IBMSCSYD":
                                            store = "IBMSC Sydney";
                                            break;
                                        case "IBMSCMEL":
                                            store = "IBMSC Melbourne";
                                            break;
                                        default:
                                            store = "ERROR";
                                            break;
                                    }

                                    if (store == "ERROR") continue;

                                    return true;
                                }
                            }

                            return false;
                        }
                    }
                }
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("Error in [CheckCSAndIBMSC]{0}Message: {1}{0}InnerException: {2}{0}, StackTrace: {3}{0}", Environment.NewLine, sqlEx.Message, sqlEx.InnerException, sqlEx.Message);
                return false;
            }
        }

        private bool CheckAvailable(int idPart)
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    const string selectQuery = "SELECT s1.idChange, o1.idChange FROM spare_changes s1, ordered_spare o1 WHERE s1.idPart=@idPart OR o1.idPart=@idPart;";

                    connection.Open();

                    using (var cmd = new MySqlCommand(selectQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@idPart", idPart);

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return false;
                            }

                            return true;
                        }
                    }
                }
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("Error in [CheckAvailable]{0}Message: {1}{0}InnerException: {2}{0}, StackTrace: {3}{0}", Environment.NewLine, sqlEx.Message, sqlEx.InnerException, sqlEx.Message);
                return false;
            }
        }

        internal SparePart GetPart(int idPart)
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    var sparePart = new SparePart();

                    const string selectQuery = "SELECT * FROM parts WHERE idPart=@idPart;";

                    connection.Open();

                    using (var cmd = new MySqlCommand(selectQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@idPart", idPart);

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                sparePart.ID = idPart;
                                sparePart.IDLocation = reader.GetInt32("idLocation");
                                sparePart.MPN = reader.GetString("mpn");
                                sparePart.MPNDescription = reader.GetString("mpn_description");
                                sparePart.SerialNumber = reader.GetString("serial_number");
                                sparePart.Barcode = reader.GetString("mits_barcode");
                                sparePart.BinDescription = reader.GetString("bin_description");
                                sparePart.ConfigParameters = reader.GetString("config_parameters");
                                sparePart.Manufacturer = reader.GetString("manufacturer");
                            }

                            return sparePart;
                        }
                    }
                }
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("Error in [CheckAvailable]{0}Message: {1}{0}InnerException: {2}{0}, StackTrace: {3}{0}", Environment.NewLine, sqlEx.Message, sqlEx.InnerException, sqlEx.Message);
                return new SparePart();
            }
        }

        internal string GetPUDOAddress(string pudoCode)
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    var sparePart = new SparePart();

                    const string selectQuery = "SELECT name, co_name, house_num, street, city, post_code FROM addresses WHERE pudo_node_code=@pudoCode;";

                    connection.Open();

                    using (var cmd = new MySqlCommand(selectQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@pudoCode", pudoCode);

                        using (var reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                if (reader.GetString("name").Contains("EXCHANGE"))
                                    return String.Format("{0} - {1} - {2} / {3} / {4} / {5}", reader.GetString("name"), reader.GetString("co_name"), reader.GetString("house_num"), reader.GetString("street"), reader.GetString("city"), reader.GetString("post_code"));
                            }

                            return String.Empty;
                        }
                    }
                }
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("Error in [GetPUDOAddress]{0}Message: {1}{0}InnerException: {2}{0}, StackTrace: {3}{0}", Environment.NewLine, sqlEx.Message, sqlEx.InnerException, sqlEx.Message);
                return String.Empty;
            }
        }

        internal string GetVendorName(string mpn)
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    var sparePart = new SparePart();

                    const string selectQuery = "SELECT v1.name FROM vendors v1 JOIN sourcefromdata s1 ON v1.idVendor=s1.idVendor WHERE s1.material=@mpn;";

                    connection.Open();

                    using (var cmd = new MySqlCommand(selectQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@mpn", mpn);

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return reader.GetString(0);
                            }

                            return String.Empty;
                        }
                    }
                }
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("Error in [GetVendorName]{0}Message: {1}{0}InnerException: {2}{0}, StackTrace: {3}{0}", Environment.NewLine, sqlEx.Message, sqlEx.InnerException, sqlEx.Message);
                return String.Empty;
            }
        }

        internal void AddOrderPart(int idChange, SparePart orderedPart, string orderStore, string pudoCode, string pudoAddress)
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    var sparePart = new SparePart();

                    const string selectQuery = "INSERT INTO ordered_spare (idChange, idPart, item_number, mpn, config_parameters, order_store, pudo_code, pudo_address) VALUES (@idChange, @idPart, @itemNumber, @mpn, @configParameters, @orderStore, @pudoCode, @pudoAddress);";

                    connection.Open();

                    using (var cmd = new MySqlCommand(selectQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@idChange", idChange);
                        cmd.Parameters.AddWithValue("@idPart", orderedPart.ID);
                        cmd.Parameters.AddWithValue("@itemNumber", orderedPart.ItemNumber);
                        cmd.Parameters.AddWithValue("@mpn", orderedPart.MPN);
                        cmd.Parameters.AddWithValue("@configParameters", orderedPart.ConfigParameters);
                        cmd.Parameters.AddWithValue("@orderStore", orderStore);
                        cmd.Parameters.AddWithValue("@pudoCode", pudoCode);
                        cmd.Parameters.AddWithValue("@pudoAddress", pudoAddress);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("Error in [AddOrderPart]{0}Message: {1}{0}InnerException: {2}{0}, StackTrace: {3}{0}", Environment.NewLine, sqlEx.Message, sqlEx.InnerException, sqlEx.Message);
            }
        }

        internal void AddOrderPart(int idChange, int itemNumber, string mpn, string configParameters, string vendorName, string pudoCode, string pudoAddress)
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    var sparePart = new SparePart();

                    const string selectQuery = "INSERT INTO ordered_spare (idChange, item_number, mpn, config_parameters, order_store, pudo_code, pudo_address) VALUES (@idChange, @itemNumber, @mpn, @configParameters, @orderStore, @pudoCode, @pudoAddress);";

                    connection.Open();

                    using (var cmd = new MySqlCommand(selectQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@idChange", idChange);
                        cmd.Parameters.AddWithValue("@itemNumber", itemNumber);
                        cmd.Parameters.AddWithValue("@mpn", mpn);
                        cmd.Parameters.AddWithValue("@configParameters", configParameters);
                        cmd.Parameters.AddWithValue("@orderStore", vendorName);
                        cmd.Parameters.AddWithValue("@pudoCode", pudoCode);
                        cmd.Parameters.AddWithValue("@pudoAddress", pudoAddress);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("Error in [AddOrderPart]{0}Message: {1}{0}InnerException: {2}{0}, StackTrace: {3}{0}", Environment.NewLine, sqlEx.Message, sqlEx.InnerException, sqlEx.Message);
            }
        }

        internal string GetMPNDescription(string mpn)
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    var sparePart = new SparePart();

                    const string selectQuery = "SELECT mpn_description FROM parts WHERE mpn=@mpn OR mpn LIKE @mpnPart;";

                    connection.Open();

                    using (var cmd = new MySqlCommand(selectQuery, connection))
                    {
                        cmd.Parameters.AddWithValue("@mpn", mpn);
                        cmd.Parameters.AddWithValue("@mpnPart", mpn + ":%");

                        using (var reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return reader.GetString(0);
                            }

                            return String.Empty;
                        }
                    }
                }
            }
            catch (MySqlException sqlEx)
            {
                Console.WriteLine("Error in [GetMPNDescription]{0}Message: {1}{0}InnerException: {2}{0}, StackTrace: {3}{0}", Environment.NewLine, sqlEx.Message, sqlEx.InnerException, sqlEx.Message);
                return String.Empty;
            }
        }

        internal int GetPartId(string serial, string binDesc, string node = "")
        {
            try
            {
                using (var connection = new MySqlConnection(_constring))
                {
                    var queryPart = "";
                    if (serial != "") queryPart = @"SELECT idPart FROM `parts` WHERE bin_description=@bindesc AND serial_number=@serial";
                    else if (node != "") queryPart = @"SELECT p1.idPart FROM `parts` p1 JOIN `locations` l1 ON p1.idLocation=l1.idLocation WHERE p1.bin_description=@bindesc AND l1.node=@node";
                    connection.Open();
                    using (var cmd = new MySqlCommand(queryPart, connection))
                    {
                        cmd.Parameters.AddWithValue("bindesc", binDesc);
                        cmd.Parameters.AddWithValue("serial", serial);
                        if (serial == "") cmd.Parameters.AddWithValue("node", node);
                        var val = cmd.ExecuteScalar();
                        if (val != null) return int.Parse(val.ToString());
                    }
                }
            }
            catch (Exception)
            {
                return -1;
            }
            return -1;
        }

        internal bool PositionEmpty(string bin, string node)
        {
            using (var connection = new MySqlConnection(_constring))
            {
                const string queryBin = @"SELECT * FROM `parts` p1 JOIN `locations` l1 ON p1.idLocation=l1.idLocation WHERE p1.bin_description=@bin AND l1.node=@node";
                connection.Open();
                using (var cmd = new MySqlCommand(queryBin, connection))
                {
                    cmd.Parameters.AddWithValue("bin", bin);
                    cmd.Parameters.AddWithValue("node", node);
                    var val = cmd.ExecuteScalar();
                    return val != null;
                }
            }
        }
    }

}
