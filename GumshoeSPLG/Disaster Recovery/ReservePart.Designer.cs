﻿namespace GumshoeSPLG
{
    partial class ReservePart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTaskNumber = new System.Windows.Forms.Label();
            this.labelReference = new System.Windows.Forms.Label();
            this.labelRtcLog = new System.Windows.Forms.Label();
            this.labelComments = new System.Windows.Forms.Label();
            this.labelTaskId = new System.Windows.Forms.Label();
            this.textBoxTicketNumber = new System.Windows.Forms.TextBox();
            this.textBoxTaskId = new System.Windows.Forms.TextBox();
            this.textBoxReference = new System.Windows.Forms.TextBox();
            this.textBoxRtcLog = new System.Windows.Forms.TextBox();
            this.textBoxComments = new System.Windows.Forms.TextBox();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelTaskNumber
            // 
            this.labelTaskNumber.AutoSize = true;
            this.labelTaskNumber.Location = new System.Drawing.Point(12, 22);
            this.labelTaskNumber.Name = "labelTaskNumber";
            this.labelTaskNumber.Size = new System.Drawing.Size(80, 13);
            this.labelTaskNumber.TabIndex = 0;
            this.labelTaskNumber.Text = "Ticket Number:";
            // 
            // labelReference
            // 
            this.labelReference.AutoSize = true;
            this.labelReference.Location = new System.Drawing.Point(12, 74);
            this.labelReference.Name = "labelReference";
            this.labelReference.Size = new System.Drawing.Size(100, 13);
            this.labelReference.TabIndex = 1;
            this.labelReference.Text = "Reference Number:";
            // 
            // labelRtcLog
            // 
            this.labelRtcLog.AutoSize = true;
            this.labelRtcLog.Location = new System.Drawing.Point(12, 100);
            this.labelRtcLog.Name = "labelRtcLog";
            this.labelRtcLog.Size = new System.Drawing.Size(53, 13);
            this.labelRtcLog.TabIndex = 2;
            this.labelRtcLog.Text = "RTC Log:";
            // 
            // labelComments
            // 
            this.labelComments.AutoSize = true;
            this.labelComments.Location = new System.Drawing.Point(12, 126);
            this.labelComments.Name = "labelComments";
            this.labelComments.Size = new System.Drawing.Size(59, 13);
            this.labelComments.TabIndex = 3;
            this.labelComments.Text = "Comments:";
            // 
            // labelTaskId
            // 
            this.labelTaskId.AutoSize = true;
            this.labelTaskId.Location = new System.Drawing.Point(12, 48);
            this.labelTaskId.Name = "labelTaskId";
            this.labelTaskId.Size = new System.Drawing.Size(48, 13);
            this.labelTaskId.TabIndex = 4;
            this.labelTaskId.Text = "Task ID:";
            // 
            // textBoxTicketNumber
            // 
            this.textBoxTicketNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxTicketNumber.Location = new System.Drawing.Point(118, 19);
            this.textBoxTicketNumber.Name = "textBoxTicketNumber";
            this.textBoxTicketNumber.Size = new System.Drawing.Size(127, 20);
            this.textBoxTicketNumber.TabIndex = 5;
            // 
            // textBoxTaskId
            // 
            this.textBoxTaskId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxTaskId.Location = new System.Drawing.Point(118, 45);
            this.textBoxTaskId.Name = "textBoxTaskId";
            this.textBoxTaskId.Size = new System.Drawing.Size(127, 20);
            this.textBoxTaskId.TabIndex = 6;
            // 
            // textBoxReference
            // 
            this.textBoxReference.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxReference.Location = new System.Drawing.Point(118, 71);
            this.textBoxReference.Name = "textBoxReference";
            this.textBoxReference.Size = new System.Drawing.Size(127, 20);
            this.textBoxReference.TabIndex = 7;
            // 
            // textBoxRtcLog
            // 
            this.textBoxRtcLog.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBoxRtcLog.Location = new System.Drawing.Point(118, 97);
            this.textBoxRtcLog.Name = "textBoxRtcLog";
            this.textBoxRtcLog.Size = new System.Drawing.Size(127, 20);
            this.textBoxRtcLog.TabIndex = 8;
            // 
            // textBoxComments
            // 
            this.textBoxComments.Location = new System.Drawing.Point(118, 123);
            this.textBoxComments.Multiline = true;
            this.textBoxComments.Name = "textBoxComments";
            this.textBoxComments.Size = new System.Drawing.Size(169, 73);
            this.textBoxComments.TabIndex = 9;
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(57, 212);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 10;
            this.buttonOk.Text = "Reserve";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(151, 212);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 11;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // ReservePart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 247);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.textBoxComments);
            this.Controls.Add(this.textBoxRtcLog);
            this.Controls.Add(this.textBoxReference);
            this.Controls.Add(this.textBoxTaskId);
            this.Controls.Add(this.textBoxTicketNumber);
            this.Controls.Add(this.labelTaskId);
            this.Controls.Add(this.labelComments);
            this.Controls.Add(this.labelRtcLog);
            this.Controls.Add(this.labelReference);
            this.Controls.Add(this.labelTaskNumber);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ReservePart";
            this.Text = "Reserve Part";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTaskNumber;
        private System.Windows.Forms.Label labelReference;
        private System.Windows.Forms.Label labelRtcLog;
        private System.Windows.Forms.Label labelComments;
        private System.Windows.Forms.Label labelTaskId;
        private System.Windows.Forms.TextBox textBoxTicketNumber;
        private System.Windows.Forms.TextBox textBoxTaskId;
        private System.Windows.Forms.TextBox textBoxReference;
        private System.Windows.Forms.TextBox textBoxRtcLog;
        private System.Windows.Forms.TextBox textBoxComments;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
    }
}