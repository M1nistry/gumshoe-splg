﻿using GumshoeSPLG.Disaster_Recovery;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace GumshoeSPLG
{
    public partial class Disaster : Form
    {
        private Main _main;
        private readonly BindingSource _bSource;
        private readonly BindingSource _spareBSource;
        private readonly BindingSource _faultBSource;
        private readonly BindingSource _vendorBSource;
        private readonly BindingSource _orderBSource;
        private readonly DrSqlWrapper _sql;
        private RTCPrompt _rtcPrompt;

        private Point mLastPos = new Point(-1, -1);

        public Disaster(Main main)
        {
            InitializeComponent();
            _main = main;
            Icon = _main.Icon;
            StartupAppearance();
            _sql = new DrSqlWrapper();
            _bSource = new BindingSource();
            _spareBSource = new BindingSource
            {
                DataSource = new List<SparePart>()
            };
            _faultBSource = new BindingSource
            {
                DataSource = new List<FaultyPart>()
            };
            _vendorBSource = new BindingSource
            {
                DataSource = _sql.VendorTable()
            };
            _orderBSource = new BindingSource
            {
                DataSource = new List<SparePart>()
            };
            dgvFaultyParts.AutoGenerateColumns = false;
            dgvSpares.AutoGenerateColumns = false;
            dgvParts.AutoGenerateColumns = false;
            dgvVendors.AutoGenerateColumns = false;
            dgvOrderedSpares.AutoGenerateColumns = false;
            dgvParts.DataSource = _bSource;
            dgvSpares.DataSource = _spareBSource;
            dgvFaultyParts.DataSource = _faultBSource;
            dgvVendors.DataSource = _vendorBSource;
            dgvOrderedSpares.DataSource = _orderBSource;
            tbcChanges.SendToBack();
        }

        private void StartupAppearance()
        {
            Size = new Size(380, 380);
            panelTitle.Size = new Size(554, 290);
            titleImageBox.Visible = true;
            cmbChangeType.Location = new Point(168, 247);
            labelChangePerformed.Location = new Point(60, 249);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            panelTitle.BorderStyle = BorderStyle.None;
            tabControl.Visible = false;
            tbcChanges.Visible = false;
            btnSubmitChange.Visible = false;
            buttonNew.Visible = false;
        }

        #region Parts Tab
        private void buttonAddFaulty_Click(object sender, EventArgs e)
        {
            if (textFaultySerial.Text.Length <= 0)
            {
                MessageBox.Show("A Faulty requires a Serial Number", "Add Faulty", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (textFaultyMpn.Text.Length <= 0)
            {
                MessageBox.Show("A Faulty requires an MPN", "Add Faulty", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (textFaultyNode.Text.Length != 4)
            {
                MessageBox.Show("A Faulty requires an Install Node", "Add Faulty", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (textFaultyInstallLocation.Text.Length <= 0)
            {
                MessageBox.Show("A Faulty requires an Install Location", "Add Faulty", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (textFaultyConnote.Text.Length <= 0)
            {
                MessageBox.Show("A Faulty requires a Connote", "Add Faulty", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            var faultyPart = new FaultyPart
            {
                ItemNumber = dgvFaultyParts.Rows.Count + 1,
                MPN = textFaultyMpn.Text,
                SerialNumber = textFaultySerial.Text,
                MITSBarcode = textFaultyBarcode.Text,
                InstallNode = textFaultyNode.Text,
                InstallLocation = textFaultyInstallLocation.Text,
                Connote = textFaultyConnote.Text
            };

            faultyPart.ReturnToAddress = _sql.GetReturnToAddress(faultyPart.MPN);

            var newList = new List<FaultyPart>((List<FaultyPart>)_faultBSource.DataSource);
            newList.Add(faultyPart);
            _faultBSource.DataSource = newList;

            textFaultySerial.Text = String.Empty;
            textFaultyMpn.Text = String.Empty;
            textFaultyNode.Text = String.Empty;
            textFaultyInstallLocation.Text = String.Empty;
            textFaultyBarcode.Text = String.Empty;
            textFaultyConnote.Text = String.Empty;
            //dgvFaultyParts.DataSource = null;
            //dgvFaultyParts.DataSource = _faultBSource;
        }

        private void textFaultyInstallLocation_Enter(object sender, EventArgs e)
        {
            if (textFaultyInstallLocation.ForeColor == Color.Gray)
            {
                textFaultyInstallLocation.ForeColor = Color.Black;
                textFaultyInstallLocation.Text = String.Empty;
            }
        }


        private void textFaultyInstallLocation_Leave(object sender, EventArgs e)
        {
            if (textFaultyInstallLocation.Text != String.Empty) return;
            textFaultyInstallLocation.ForeColor = Color.Gray;
            textFaultyInstallLocation.Text = @"Floor 1, Suite 13B, Rack C, Shelf 5, Board 13";
        }

        #endregion

        #region Search Spares Tab

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (textSerialSearch.Text.Length <= 0 && textMPNSearch.Text.Length <= 0 && textNodeSearch.Text.Length <= 0 && textSearch.Text.Length <= 0)
                return;

            var startTime = DateTime.Now;
            var partsList = _sql.GetPartData(textSerialSearch.Text, textMPNSearch.Text, textNodeSearch.Text, textSearch.Text);

            _bSource.DataSource = partsList;

            var elapsedTime = DateTime.Now - startTime;

            lblQueryTime.Text = String.Format("Search returned {0} results in {1} seconds", partsList.Count, elapsedTime.TotalSeconds);
        }

        private void AddSparePart()
        {
            var selectedRow = dgvParts.SelectedRows[0];
            if (selectedRow == null) return;
            var changeId = _sql.CheckSpareChanges(int.Parse(selectedRow.Cells["columnId"].Value.ToString()));
            if ( changeId > -1)
            {
                var changeDetails = _sql.GetChangeDetails(changeId);
                MessageBox.Show(String.Format("This part is already reserved for job:\n {0}", changeDetails));
                return;
            }
            var newSpare = ((SparePart)selectedRow.DataBoundItem).Clone() as SparePart;
            if (!((List<SparePart>)_spareBSource.DataSource).Contains(newSpare))
            {
                newSpare.ItemNumber = dgvSpares.Rows.Count + 1;
                var newList = new List<SparePart>((List<SparePart>)_spareBSource.DataSource) {newSpare};
                _spareBSource.DataSource = newList;
                MessageBox.Show(@"Successfully added!", @"Part added", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            //dgvSpares.DataSource = null;
            //dgvSpares.DataSource = _spareBSource;
        }

        private void dgvParts_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex <= -1) return;
            dgvParts.ClearSelection();
            dgvParts.Rows[e.RowIndex].Selected = true;
            AddSparePart();
        }

        private void dgvParts_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && e.RowIndex != -1)
            {
                dgvParts.ClearSelection();
                dgvParts.Rows[e.RowIndex].Selected = true;
                contextMenuSpares.Show(MousePosition.X, MousePosition.Y);
            }
        }

        private void viewLocationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var rowIndex = dgvParts.SelectedRows[0].Index;
            textNode.Text = dgvParts["columnNode", rowIndex].Value.ToString();
            tabControl.SelectTab(tabPageLocation);
            var binDesc = _sql.GetBin(dgvParts["columnSerialNumber", rowIndex].Value.ToString(),
                dgvParts["columnMPN", rowIndex].Value.ToString()).Replace("//", "").Split('/');
            treeView.CollapseAll();
            if (binDesc.Length == 4)
            {
                treeView.Nodes[binDesc[0]].Expand();
                treeView.Nodes[binDesc[0]].Nodes[binDesc[1]].Expand();
                treeView.Nodes[binDesc[0]].Nodes[binDesc[1]].Nodes[binDesc[2]].Expand();
                treeView.Nodes[binDesc[0]].Nodes[binDesc[1]].Nodes[binDesc[2]].Nodes[binDesc[3]].Expand();
                treeView.Nodes[binDesc[0]].Nodes[binDesc[1]].Nodes[binDesc[2]].Nodes[binDesc[3]].Nodes[dgvParts["columnSerialNumber", rowIndex].Value.ToString()].Expand();
                treeView.SelectedNode = treeView.Nodes[binDesc[0]].Nodes[binDesc[1]].Nodes[binDesc[2]].Nodes[binDesc[3]].Nodes[
                    dgvParts["columnSerialNumber", rowIndex].Value.ToString()];

            }
            if (binDesc.Length == 6)
            {
                treeView.Nodes[binDesc[0]].Expand();
                treeView.Nodes[binDesc[0]].Nodes[binDesc[1]].Expand();
                treeView.Nodes[binDesc[0]].Nodes[binDesc[1]].Nodes[binDesc[2]].Expand();
                treeView.Nodes[binDesc[0]].Nodes[binDesc[1]].Nodes[binDesc[2]].Nodes[binDesc[3]].Expand();
                treeView.Nodes[binDesc[0]].Nodes[binDesc[1]].Nodes[binDesc[2]].Nodes[binDesc[3]].Nodes[binDesc[4]].Expand();
                treeView.Nodes[binDesc[0]].Nodes[binDesc[1]].Nodes[binDesc[2]].Nodes[binDesc[3]].Nodes[binDesc[4]].Nodes[binDesc[5]].Expand();
                treeView.SelectedNode =
                    treeView.Nodes[binDesc[0]].Nodes[binDesc[1]].Nodes[binDesc[2]].Nodes[binDesc[3]].Nodes[binDesc[4]]
                        .Nodes[binDesc[5]].Nodes[dgvParts["columnSerialNumber", rowIndex].Value.ToString()];
            } 
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddSparePart();
        }

        private void findVendorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl.SelectTab(tabVendors);
            textSearchVendor.Text = dgvParts.SelectedRows[0].Cells["columnMPN"].Value.ToString();
        }

        #endregion

        #region Spares Location Tab

        private void TreeNodes(IList<string> splitLocation, StoragePart part)
        {
            if (treeView.Nodes[splitLocation[0]] == null && splitLocation[0] != "") treeView.Nodes.Add(splitLocation[0], splitLocation[0]);
            if (treeView.Nodes[splitLocation[0]] == null) return;
            switch (splitLocation.Count)
            {
                case (4):
                    //Field Store
                    if (!treeView.Nodes[splitLocation[0]].Nodes.ContainsKey(splitLocation[1]))
                    {
                        treeView.Nodes[splitLocation[0]].Nodes.Add(splitLocation[1], splitLocation[1]);
                    }
                    if (!treeView.Nodes[splitLocation[0]].Nodes[splitLocation[1]].Nodes.ContainsKey(splitLocation[2]))
                    {
                        treeView.Nodes[splitLocation[0]].Nodes[splitLocation[1]].Nodes.Add(splitLocation[2], splitLocation[2]);
                    }
                    if (!treeView.Nodes[splitLocation[0]].Nodes[splitLocation[1]].Nodes[splitLocation[2]].Nodes.ContainsKey(splitLocation[3]))
                    {
                        treeView.Nodes[splitLocation[0]].Nodes[splitLocation[1]].Nodes[splitLocation[2]].Nodes.Add(splitLocation[3], splitLocation[3]);
                    }
                    //Add the part at the location.
                    if (!treeView.Nodes[splitLocation[0]].Nodes[splitLocation[1]].Nodes[splitLocation[2]].Nodes[splitLocation[3]].Nodes.ContainsKey(part.SerialNumber))
                    {
                        treeView.Nodes[splitLocation[0]].Nodes[splitLocation[1]].Nodes[splitLocation[2]].Nodes[splitLocation[3]].Nodes.Add(part.SerialNumber, part.SerialNumber);
                    }
                    break;
                case (6):
                    if (splitLocation[5] == "") return;
                    //Kit store
                    if (!treeView.Nodes[splitLocation[0]].Nodes.ContainsKey(splitLocation[1]))
                    {
                        treeView.Nodes[splitLocation[0]].Nodes.Add(splitLocation[1], splitLocation[1]);
                    }
                    if (!treeView.Nodes[splitLocation[0]].Nodes[splitLocation[1]].Nodes.ContainsKey(splitLocation[2]))
                    {
                        treeView.Nodes[splitLocation[0]].Nodes[splitLocation[1]].Nodes.Add(splitLocation[2], splitLocation[2]);
                    }
                    if (!treeView.Nodes[splitLocation[0]].Nodes[splitLocation[1]].Nodes[splitLocation[2]].Nodes.ContainsKey(splitLocation[3]))
                    {
                        treeView.Nodes[splitLocation[0]].Nodes[splitLocation[1]].Nodes[splitLocation[2]].Nodes.Add(splitLocation[3], splitLocation[3]);
                    }
                    if (!treeView.Nodes[splitLocation[0]].Nodes[splitLocation[1]].Nodes[splitLocation[2]].Nodes[splitLocation[3]].Nodes.ContainsKey(splitLocation[4]))
                    {
                        treeView.Nodes[splitLocation[0]].Nodes[splitLocation[1]].Nodes[splitLocation[2]].Nodes[splitLocation[3]].Nodes.Add(splitLocation[4], splitLocation[4]);
                    }
                    if (!treeView.Nodes[splitLocation[0]].Nodes[splitLocation[1]].Nodes[splitLocation[2]].Nodes[splitLocation[3]].Nodes[splitLocation[4]].Nodes.ContainsKey(splitLocation[5]))
                    {
                        treeView.Nodes[splitLocation[0]].Nodes[splitLocation[1]].Nodes[splitLocation[2]].Nodes[splitLocation[3]].Nodes[splitLocation[4]].Nodes.Add(splitLocation[5], splitLocation[5]);
                    }
                    //Add part at full path
                    if (!treeView.Nodes[splitLocation[0]].Nodes[splitLocation[1]].Nodes[splitLocation[2]].Nodes[splitLocation[3]].Nodes[splitLocation[4]].Nodes[splitLocation[5]].Nodes.ContainsKey(part.SerialNumber))
                    {
                        treeView.Nodes[splitLocation[0]].Nodes[splitLocation[1]].Nodes[splitLocation[2]].Nodes[splitLocation[3]].Nodes[splitLocation[4]].Nodes[splitLocation[5]].Nodes.Add(part.SerialNumber, part.SerialNumber);
                    }
                    break;
            }

        }

        private void textNode_TextChanged(object sender, EventArgs e)
        {
            if (textNode.Text.Length < 4 && treeView.Nodes.Count > 0) treeView.Nodes.Clear();
            if (textNode.Text.Length != 4) return;
            var dictStorage = _sql.StorageLocations(textNode.Text);
            var sortDict = from entry in dictStorage orderby entry.Value ascending select entry;
            foreach (var entry in sortDict)
            {
                var locSplit = entry.Value.Replace("//", "").Split('/');
                TreeNodes(locSplit, entry.Key);
            }
            if (treeView.Nodes.Count <= 0) return;
            treeView.Nodes[0].Expand();
        }

        #endregion

        #region VendorTab
        private void textSearchVendor_TextChanged(object sender, EventArgs e)
            {
            if (textSearchVendor.Text.Length == 0)
                {
                _vendorBSource.RemoveFilter();
                return;
            }
            labelClearSearch.Visible = true;
            _vendorBSource.Filter = "MPN like '%" + textSearchVendor.Text + "%' OR " +
                                    "Source like '%" + textSearchVendor.Text + "%' OR " +
                                    "ReturnTo like '%" + textSearchVendor.Text + "%' OR " +
                                    "Vendor like '%" + textSearchVendor.Text + "%' OR " +
                                    "Address like '%" + textSearchVendor.Text + "%' OR " +
                                    "Telephone like '%" + textSearchVendor.Text + "%'";


        }

        private void labelClearSearch_Click(object sender, EventArgs e)
        {
            textSearchVendor.Text = String.Empty;
        }

        private void labelClearSearch_MouseEnter(object sender, EventArgs e)
        {
            labelClearSearch.ForeColor = Color.Black;
            
        }

        private void labelClearSearch_MouseLeave(object sender, EventArgs e)
        {
            labelClearSearch.ForeColor = SystemColors.ControlDark;
        }

        #endregion

        private void cmbChangeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (titleImageBox.Visible)
            {
                Size = new Size(596, 649);
                panelTitle.Size = new Size(554, 31);
                cmbChangeType.Location = new Point(106, 5);
                labelChangePerformed.Location = new Point(5, 8);
                FormBorderStyle = FormBorderStyle.Sizable;
                panelTitle.BorderStyle = BorderStyle.FixedSingle;
                tabControl.Visible = true;
                titleImageBox.Visible = false;
                tbcChanges.Visible = true;
                btnSubmitChange.Visible = true;
                buttonNew.Visible = true;
            }
            switch (cmbChangeType.Text)
            {
                case "SWAP":
                    tbcChanges.SelectTab("tabSwap");
                    //Main Grid re-sizing
                    tlpChangeGrid.RowStyles[0].SizeType = SizeType.Absolute;
                    tlpChangeGrid.RowStyles[0].Height = 72;
                    tlpChangeGrid.RowStyles[1].SizeType = SizeType.Percent;
                    tlpChangeGrid.RowStyles[1].Height = 50;
                    tlpChangeGrid.RowStyles[2].SizeType = SizeType.Absolute;
                    tlpChangeGrid.RowStyles[2].Height = 0;

                    //Order Grid re-sizing
                    tlpSpareParts.RowStyles[0].SizeType = SizeType.Percent; //Ordered Spares
                    tlpSpareParts.RowStyles[0].Height = 0;
                    tlpSpareParts.RowStyles[1].SizeType = SizeType.Percent; //Selected Spares
                    tlpSpareParts.RowStyles[1].Height = 100;

                    dgvOrderedSpares.Visible = false;
                    break;
                case "MOVED":
                    tbcChanges.SelectTab("tabMoved");
                    dgvSpares.Visible = true;
                    dgvOrderedSpares.Visible = false;
                    break;
                case "PUTAWAY":
                    tbcChanges.SelectTab("tabPutaway");
                    dgvSpares.Visible = true;
                    dgvOrderedSpares.Visible = false;
                    break;
                case "POD":
                    tbcChanges.SelectTab("tabPOD");
                    dgvSpares.Visible = true;
                    dgvOrderedSpares.Visible = false;
                    break;
                case "ORDER":
                    tbcChanges.SelectTab("tabORDER");
                    //Main Grid re-sizing
                    tlpChangeGrid.RowStyles[0].SizeType = SizeType.Absolute;
                    tlpChangeGrid.RowStyles[0].Height = 0;
                    tlpChangeGrid.RowStyles[1].SizeType = SizeType.Percent;
                    tlpChangeGrid.RowStyles[1].Height = 0;
                    tlpChangeGrid.RowStyles[2].SizeType = SizeType.Absolute;
                    tlpChangeGrid.RowStyles[2].Height = 36;

                    //Order Grid re-sizing
                    tlpSpareParts.RowStyles[0].SizeType = SizeType.Percent; //Ordered Spares
                    tlpSpareParts.RowStyles[0].Height = 50;
                    tlpSpareParts.RowStyles[1].SizeType = SizeType.Percent; //Selected Spares
                    tlpSpareParts.RowStyles[1].Height = 50;

                    dgvOrderedSpares.Visible = true;
                    break;
            }
        }

        private void textFaultNode_TextChanged(object sender, EventArgs e)
        {
            if (textFaultNode.Text.Length != 4) return;
            textPudoOrder.Text = _sql.FaultPuDo(textFaultNode.Text);
        }

        private void textPudoOrder_TextChanged(object sender, EventArgs e)
        {
            if (textPudoOrder.Text.Length == 0) textPudoDesc.Text = String.Empty;
            if (textPudoOrder.Text.Length != 4) return;
            textPudoDesc.Text = _sql.PudoDesc(textPudoOrder.Text);
        }

        private void btnSubmitChange_Click(object sender, EventArgs e)
        {
            if (cmbChangeType.Text == "SWAP")
            {
                var faultyList = new List<FaultyPart>();
                var spareList = new List<SparePart>();
                var disconnectedSpares = new List<SparePart>();
                var itemNumberList = new List<int>();

                foreach (DataGridViewRow item in dgvFaultyParts.Rows)
                {
                    var part = (FaultyPart)item.DataBoundItem;

                    itemNumberList.Add(part.ItemNumber);
                    faultyList.Add(part);
                }

                foreach (DataGridViewRow item in dgvSpares.Rows)
                {
                    var part = (SparePart)item.DataBoundItem;

                    part.ReturnReason = "Swapped";

                    spareList.Add(part);

                    if (itemNumberList.Contains(part.ItemNumber) == false)
                        disconnectedSpares.Add(part);
                }

                if (disconnectedSpares.Count > 0)
                {
                    var sparesNoFaulties = new SparesNoFaulties();
                    sparesNoFaulties.SparesList = disconnectedSpares;
                    sparesNoFaulties.FaultiesList = faultyList;

                    sparesNoFaulties.Show();
                    sparesNoFaulties.Build();
                    sparesNoFaulties.Visible = false;

                    if (sparesNoFaulties.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                        return;
                }

                var idChange = _sql.AddChange(cmbChangeType.Text, txtSwapTOWTicketNum.Text, txtSwapTaskID.Text, cmbSwapSystemID.Text, txtSwapWorkOrder.Text, txtSwapComments.Text);

                foreach (var item in faultyList)
                {
                    _sql.AddFaultyChange(idChange, item);
                }

                foreach (var item in spareList)
                {
                    _sql.AddSpareChange(idChange, item);
                }

                var rtcLogText = String.Format("SWAP performed{0}" +
                                               "TOW Ticket Number: {1}{0}" +
                                               "Task ID: {2}{0}" +
                                               "System ID: {3}{0}" +
                                               "Work Order: {4}{0}" +
                                               "{0}" +
                                               "Comments: {5}{0}" +
                                               "{0}",
                                               Environment.NewLine,
                                               txtSwapTOWTicketNum.Text,
                                               txtSwapTaskID.Text,
                                               cmbSwapSystemID.Text,
                                               txtSwapWorkOrder.Text,
                                               txtSwapComments.Text);

                if (faultyList.Count > 0)
                {
                    rtcLogText += String.Format("{0}Faulty Parts:{0}", Environment.NewLine);

                    foreach (var faulty in faultyList)
                    {
                        rtcLogText += String.Format("{0}{1}", Environment.NewLine, faulty.RTCLogText);
                    }
                }
                if (spareList.Count > 0)
                {
                    rtcLogText += String.Format("{0}Spare Parts:{0}", Environment.NewLine);

                    foreach (var spare in spareList)
                    {
                        rtcLogText += String.Format("{0}{1}", Environment.NewLine, spare.RTCLogText);
                    }
                }
                _faultBSource.DataSource = new List<FaultyPart>();
                _spareBSource.DataSource = new List<SparePart>();
                if (_rtcPrompt != null && _rtcPrompt.Visible)
                {
                    _rtcPrompt.BringToFront();
                }
                else
                {
                    _rtcPrompt = new RTCPrompt
                    {
                        RTCText = rtcLogText,
                        IdChange = idChange,
                        Icon = Icon,
                        Text = @"RTC details: " + txtOrderTOWTicketNum.Text,
                        Owner = this
                    };
                    _rtcPrompt.FormClosed += (o, ea) => _rtcPrompt = null;
                    _rtcPrompt.Show();
                }
            }
            else if (cmbChangeType.Text == "ORDER")
            {
                if (textPudoOrder.Text.Length != 4 && textPudoDesc.Text.Length <= 0)
                {
                    MessageBox.Show("Please enter a valid delivery PUDO for the order", "Order PUDO Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (((List<SparePart>)_orderBSource.DataSource).Count <= 0 && ((List<SparePart>)_spareBSource.DataSource).Count <= 0)
                {
                    MessageBox.Show("No items selected to be ordered", "No Order Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                var idChange = -1;
                var orderDetails = new List<string>();
                var pudoAddress = _sql.GetPUDOAddress(textPudoOrder.Text);

                idChange = _sql.AddChange(cmbChangeType.Text, txtOrderTOWTicketNum.Text, txtOrderTaskID.Text, cmbOrderSystemID.Text, "", txtOrderComments.Text);

                foreach (DataGridViewRow row in dgvOrderedSpares.Rows)
                {
                    var part = (SparePart)row.DataBoundItem;

                    if (_sql.GetSourceFromVendor(part.MPN) == false)
                    {
                        var idPart = -1;
                        var store = String.Empty;

                        if (_sql.CheckCSAndIBMSC(part.MPN, out idPart, out store))
                        {
                            var orderedPart = _sql.GetPart(idPart);
                            orderedPart.ItemNumber = part.ItemNumber;
                            orderedPart.ConfigParameters = part.ConfigParameters;

                            _sql.AddOrderPart(idChange, orderedPart, store, textPudoOrder.Text, pudoAddress);

                            orderDetails.Add(String.Format("{1}" +
                                                           "Ordered From: {2}{0}" +
                                                           "Order for: PUDO - {3} {4}{0}",
                                                           Environment.NewLine,
                                                           orderedPart.RTCLogText,
                                                           store,
                                                           textPudoOrder.Text,
                                                           pudoAddress));
                        }
                        else
                        {
                            store = _sql.GetVendorName(part.MPN);

                            _sql.AddOrderPart(idChange, part.ItemNumber, part.MPN, part.ConfigParameters, store, textPudoOrder.Text, pudoAddress);

                            orderDetails.Add(String.Format("Item Number: {1}{0}" +
                                                           "MPN: {2}{0}" +
                                                           "Config Parameters: {3}{0}" +
                                                           "Ordered From: {4}{0}" +
                                                           "Order for: PUDO - {5} {6}{0}",
                                                           Environment.NewLine,
                                                           part.ItemNumber,
                                                           part.MPN,
                                                           part.ConfigParameters,
                                                           store,
                                                           textPudoOrder.Text,
                                                           pudoAddress));
                        }
                    }
                    else
                    {
                        var store = _sql.GetVendorName(part.MPN);

                        _sql.AddOrderPart(idChange, part.ItemNumber, part.MPN, part.ConfigParameters, store, textPudoOrder.Text, pudoAddress);

                        orderDetails.Add(String.Format("Item Number: {1}{0}" +
                                                       "MPN: {2}{0}" +
                                                       "Config Parameters: {3}{0}" +
                                                       "Ordered From: {4}{0}" +
                                                       "Order for: PUDO - {5} {6}{0}",
                                                       Environment.NewLine,
                                                       part.ItemNumber,
                                                       part.MPN,
                                                       part.ConfigParameters,
                                                       store,
                                                       textPudoOrder.Text,
                                                       pudoAddress));
                    }
                }

                var rtcLogText = String.Format("ORDER performed{0}" +
                                               "TOW Ticket Number: {1}{0}" +
                                               "Task ID: {2}{0}" +
                                               "System ID: {3}{0}" +
                                               "{0}" +
                                               "Comments: {4}{0}" +
                                               "{0}" + 
                                               "{0}" + 
                                               "Ordered Parts:{0}",
                                               Environment.NewLine,
                                               txtOrderTOWTicketNum.Text,
                                               txtOrderTaskID.Text,
                                               cmbOrderSystemID.Text,
                                               txtOrderComments.Text);

                foreach (var item in orderDetails)
                {
                    rtcLogText += String.Format("{0}{1}", Environment.NewLine, item);
                }

                _orderBSource.DataSource = new List<SparePart>();
                _spareBSource.DataSource = new List<SparePart>();
                if (_rtcPrompt != null && _rtcPrompt.Visible)
                {
                    _rtcPrompt.BringToFront();
                }
                else
                {
                    _rtcPrompt = new RTCPrompt
                    {
                        RTCText = rtcLogText,
                        Icon = Icon,
                        Text = @"RTC details: " + txtOrderTOWTicketNum.Text,
                        IdChange = idChange,
                        Owner = this
                    };
                    _rtcPrompt.FormClosed += (o, ea) => _rtcPrompt = null;
                    _rtcPrompt.Show();
                }
            }
            else if (cmbChangeType.Text == @"MOVED")
            {
                if (!txtMovedNewBin.Text.Contains("Position") && !txtMovedNewBin.Text.Contains("position"))
                {
                    MessageBox.Show(@"Please specify a position for this part to be moved into", @"Position Missing!",
                        MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                if (txtMovedOldNode.Text == "")
                {
                    MessageBox.Show(@"No original bin is entered, please select a part to be moved.",
                        @"WHAT ARE YOU PLAYING AT!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                txtMovedNewBin.Text = txtMovedNewBin.Text.Replace(@"\", @"/");
                if (txtMovedNewBin.Text.EndsWith(@"/") && !txtMovedNewBin.Text.EndsWith(@"//"))
                    txtMovedNewBin.Text = txtMovedNewBin.Text + @"/";
                if (!txtMovedNewBin.Text.EndsWith(@"/")) txtMovedNewBin.Text = txtMovedNewBin.Text + @"//";
                if (txtMovedNewBin.Text.EndsWith(@"//"))
                {
                    if (_sql.PositionEmpty(txtMovedNewBin.Text, txtMovedNewNode.Text))
                    {
                        MessageBox.Show(
                            @"This position is already occupied, please choose a different position.",
                            @"Position Taken!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        var idChange = -1;
                        var newSpare = _sql.GetPart(_sql.GetPartId("", txtMovedOldBin.Text, txtMovedOldNode.Text));
                        newSpare.NewBinLocation = txtMovedNewBin.Text;
                        newSpare.NewNodeLocation = txtMovedNewNode.Text;
                        var rtcLogText = String.Format("Serial: {1}{0}" +
                                                       "MPN: {2}{0}" +
                                                       "{0}" +
                                                       "Original Node: {3}{0}" +
                                                       "Original Bin: {4}{0}" +
                                                       "{0}" +
                                                       "Destination Node: {5}{0}" + 
                                                       "Destination Bin: {6}{0}" +
                                                       "{0}" +
                                                       "Comments: {7}",
                                                       Environment.NewLine,
                                                       newSpare.SerialNumber, newSpare.MPN,
                                                       txtMovedOldNode.Text, newSpare.BinDescription,
                                                       txtMovedNewNode.Text, txtMovedNewBin.Text,
                                                       txtMoveComments.Text);
                        idChange = _sql.AddChange("MOVE", "", "", "", "", rtcLogText);
                        _sql.AddSpareChange(idChange, newSpare);
                        _rtcPrompt = new RTCPrompt
                        {
                            RTCText = rtcLogText,
                            IdChange = idChange,
                            Icon = Icon,
                            Text = @"RTC details: " + txtOrderTOWTicketNum.Text,
                            Owner = this
                        };
                        _rtcPrompt.FormClosed += (o, ea) => _rtcPrompt = null;
                        _rtcPrompt.Show();
                    }
                }

                
            }
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.FirstNode == null)
            {
                panelItemDetails.Visible = true;
                var binDesc = e.Node.FullPath.Replace(@"\", @"/");
                binDesc = binDesc.Replace("/" + e.Node.Text, !binDesc.Contains("Sub") ? "//" : "");
                var sparePart = _sql.GetPart(_sql.GetPartId(e.Node.Text, binDesc));
                lvSpareDetails.Items.Clear();
                lvSpareDetails.Items.Add("Serial").SubItems.Add(sparePart.SerialNumber);
                lvSpareDetails.Items.Add("MPN").SubItems.Add(sparePart.MPN);
                lvSpareDetails.Items.Add("Description").SubItems.Add(sparePart.MPNDescription);
                lvSpareDetails.Items.Add("Barcode").SubItems.Add(sparePart.Barcode);
                lvSpareDetails.Items.Add("Location").SubItems.Add(sparePart.BinDescription);
                lvSpareDetails.Items.Add("Config").SubItems.Add(sparePart.ConfigParameters);
                lvSpareDetails.Items.Add("Manufacturer").SubItems.Add(sparePart.Manufacturer);
            }
            else
            {
                panelItemDetails.Visible = false;
                if (cmbChangeType.Text != @"MOVED") return;
                if (!e.Node.FullPath.Contains("Position")) txtMovedNewBin.Text = e.Node.FullPath + @"\";
            }
        }

        private void lvSpareDetails_MouseMove(object sender, MouseEventArgs e)
        {
            var info = lvSpareDetails.HitTest(e.X, e.Y);
            if (mLastPos != e.Location)
            {
                if (info.Item != null && info.SubItem != null)
                {
                    toolTip.Show(info.SubItem.Text, info.Item.ListView, e.X, e.Y, 20000);
                }
                else
                {
                    toolTip.SetToolTip(lvSpareDetails, string.Empty);
                }
            }
            mLastPos = e.Location;
        }

        private void lvSpareDetails_MouseLeave(object sender, EventArgs e)
        {
            toolTip.Hide(lvSpareDetails);
        }

                
        private void dgvSpares_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && e.RowIndex != -1)
            {
                dgvSpares.ClearSelection();
                dgvSpares.Rows[e.RowIndex].Selected = true;
                ToolStripMenuItemFaultSpare.Text = @"Spare";
                contextMenuParts.Show(MousePosition.X, MousePosition.Y);
            }
        }

        private void dgvFaultyParts_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && e.RowIndex != -1)
            {
                dgvFaultyParts.ClearSelection();
                dgvFaultyParts.Rows[e.RowIndex].Selected = true;
                ToolStripMenuItemFaultSpare.Text = @"Fault";
                contextMenuParts.Show(MousePosition.X, MousePosition.Y);
            }
        }

        private void dgvOrderedSpares_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && e.RowIndex != -1)
            {
                dgvOrderedSpares.ClearSelection();
                dgvOrderedSpares.Rows[e.RowIndex].Selected = true;
                ToolStripMenuItemFaultSpare.Text = @"Ordered Spare";
                contextMenuParts.Show(MousePosition.X, MousePosition.Y);
            }
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ToolStripMenuItemFaultSpare.Text == @"Spare")
            {
                if (MessageBox.Show(@"Are you sure?", @"Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) ==
                    DialogResult.No) return;
                dgvSpares.Rows.Remove(dgvSpares.SelectedRows[0]);
            }
            else if (ToolStripMenuItemFaultSpare.Text == @"Ordered Spare")
            {
                if (MessageBox.Show(@"Are you sure?", @"Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) ==
                    DialogResult.No) return;
                dgvOrderedSpares.Rows.Remove(dgvOrderedSpares.SelectedRows[0]);
            }
            else
            {
                if (MessageBox.Show(@"Are you sure?", @"Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) ==
                    DialogResult.No) return;
                dgvFaultyParts.Rows.Remove(dgvFaultyParts.SelectedRows[0]);
            }
        }

        private void treeView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.FirstNode == null)
            {
                textSerialSearch.Text = e.Node.Text;
                textNodeSearch.Text = textNode.Text;
                btnSearch_Click(sender, e);
                tabControl.SelectTab(tabPageSearch);
            }
        }

        private void buttonAddSpareMPN_Click(object sender, EventArgs e)
        {
            if (textOrderMpn.Text.Length <= 0)
            {
                MessageBox.Show("Please enter an MPN before adding the order", "Missing MPN", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (textBoxConfigParamsSpare.Text.Length <= 0)
            {
                MessageBox.Show("Config Parameters are required when ordering spares.  If there are no required configurations, enter NONE", "Missing Configuration", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            var newList = new List<SparePart>((List<SparePart>)_orderBSource.DataSource);
            var itemNumber = ((List<SparePart>) _orderBSource.DataSource).Count + 1;
            var sparePart = new SparePart
            {
                ItemNumber = itemNumber,
                MPN = textOrderMpn.Text,
                MPNDescription = _sql.GetMPNDescription(textOrderMpn.Text),
                ConfigParameters = textBoxConfigParamsSpare.Text
            };
            newList.Add(sparePart);
            _orderBSource.DataSource = newList;

            textOrderMpn.Text = String.Empty;
            textBoxConfigParamsSpare.Text = String.Empty;
        }

        private void textFaultyMpn_Leave(object sender, EventArgs e)
        {
            if (textFaultyMpn.Text.Length <= 0)
                return;

            var mpn = String.Empty;

            if (_sql.IsValidMPN(textFaultyMpn.Text, out mpn) == false)
            {
                MessageBox.Show(String.Format("MPN given (MPN: {0}) is not a valid MPN. Please enter a valid mpn.", textFaultyMpn.Text), "Invalid MPN", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textFaultyMpn.Focus();
                textFaultyMpn.SelectAll();
                return;
            }
            else
            {
                textFaultyMpn.Text = mpn;

                if (_sql.CheckScrappable(mpn))
                    MessageBox.Show(String.Format("NOTE: MPN: {0} is a Scrappable part.", mpn), "Scrappable Part", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void textOrderMpn_Leave(object sender, EventArgs e)
        {
            if (textOrderMpn.Text.Length <= 0)
                return;

            var mpn = String.Empty;

            if (_sql.IsValidMPN(textOrderMpn.Text, out mpn) == false)
            {
                MessageBox.Show(String.Format("MPN given (MPN: {0}) is not a valid MPN. Please enter a valid mpn.", textOrderMpn.Text), "Invalid MPN", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textOrderMpn.Focus();
                textOrderMpn.SelectAll();
                return;
            }
            else
            {
                textOrderMpn.Text = mpn;

                if (_sql.CheckScrappable(mpn))
                    MessageBox.Show(String.Format("NOTE: MPN: {0} is a Scrappable part.", mpn), "Scrappable Part", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void reserveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var selectedRow = dgvParts.SelectedRows[0];
            var changeId = _sql.CheckSpareChanges(int.Parse(selectedRow.Cells["columnId"].Value.ToString()));
            if (changeId > -1)
            {
                var changeDetails = _sql.GetChangeDetails(changeId);
                MessageBox.Show(String.Format("This part is already reserved for job:\n {0}", changeDetails));
                return;
            }
            var newSpare = ((SparePart)selectedRow.DataBoundItem).Clone() as SparePart;
            
            var reservePart = new ReservePart(newSpare) {Icon = Icon};
            reservePart.ShowDialog();
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            cmbChangeType.SelectedIndex = -1;
            StartupAppearance();
            txtMovedNewBin.Text = String.Empty;
            txtMovedNewNode.Text = String.Empty;
            txtMovedOldBin.Text = String.Empty;
            txtMovedNewNode.Text = String.Empty;
            txtOrderComments.Text = String.Empty;
            txtOrderTOWTicketNum.Text = String.Empty; 
            txtOrderTaskID.Text = String.Empty;
            txtPutawayBin.Text = String.Empty;
            txtPutawayNode.Text = String.Empty;
            txtPutawayOutbound.Text = String.Empty;
            txtPutawayWorkOrder.Text = String.Empty;
            txtSwapComments.Text = String.Empty;
            txtSwapRLPUDO.Text = String.Empty;
            txtSwapTOWTicketNum.Text = String.Empty;
            txtSwapTaskID.Text = String.Empty;
            txtSwapWorkOrder.Text = String.Empty;
            textNode.Text = String.Empty;
            textSearch.Text = String.Empty;
            textSearchVendor.Text = String.Empty;
            textFaultNode.Text = String.Empty;
            textFaultyBarcode.Text = String.Empty;
            textFaultyConnote.Text = String.Empty;
            textFaultyInstallLocation.Text = String.Empty;
            textFaultyMpn.Text = String.Empty;
            textFaultyNode.Text = String.Empty;
            textFaultySerial.Text = String.Empty;
        }

        private void moveLocationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var selectedRow = dgvParts.SelectedRows[0];
            tbcChanges.SelectTab("tabMOVED");
            txtMovedOldNode.Text = selectedRow.Cells["columnNode"].Value.ToString();
            txtMovedOldBin.Text = selectedRow.Cells["columnBin"].Value.ToString();
            tabControl.SelectTab("tabPageLocation");
            textNode.Text = selectedRow.Cells["columnNode"].Value.ToString();
            txtMovedNewNode.Focus();
        }

        private void txtSwapRLPUDO_TextChanged(object sender, EventArgs e)
        {
            if (txtSwapRLPUDO.Text.Length != 4)
            {
                txtSwapRLPUDOAddress.Text = String.Empty;
                return;
            }

            txtSwapRLPUDOAddress.Text = _sql.GetPUDOAddress(txtSwapRLPUDO.Text);
        }
    }
}
