﻿namespace GumshoeSPLG.Disaster_Recovery
{
    partial class SparesNoFaulties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHeader = new System.Windows.Forms.Label();
            this.dgvSpareNoFaulty = new System.Windows.Forms.DataGridView();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.dgvSpareNoFaultyItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSpareNoFaultyMPN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSpareNoFaultySerial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvSpareNoFaultyReturnReason = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvSpareNoFaultyFaulty = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSpareNoFaulty)).BeginInit();
            this.SuspendLayout();
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.Location = new System.Drawing.Point(131, 9);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(267, 26);
            this.lblHeader.TabIndex = 0;
            this.lblHeader.Text = "The following spares aren\'t attached to any faulty parts.\r\nPlease connect them or" +
    " mark their return reason.";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dgvSpareNoFaulty
            // 
            this.dgvSpareNoFaulty.AllowUserToAddRows = false;
            this.dgvSpareNoFaulty.AllowUserToDeleteRows = false;
            this.dgvSpareNoFaulty.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSpareNoFaulty.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSpareNoFaulty.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvSpareNoFaultyItem,
            this.dgvSpareNoFaultyMPN,
            this.dgvSpareNoFaultySerial,
            this.dgvSpareNoFaultyReturnReason,
            this.dgvSpareNoFaultyFaulty});
            this.dgvSpareNoFaulty.Location = new System.Drawing.Point(12, 51);
            this.dgvSpareNoFaulty.Name = "dgvSpareNoFaulty";
            this.dgvSpareNoFaulty.Size = new System.Drawing.Size(480, 150);
            this.dgvSpareNoFaulty.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(417, 209);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(336, 209);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // dgvSpareNoFaultyItem
            // 
            this.dgvSpareNoFaultyItem.DataPropertyName = "ItemNumber";
            this.dgvSpareNoFaultyItem.HeaderText = "Item";
            this.dgvSpareNoFaultyItem.Name = "dgvSpareNoFaultyItem";
            this.dgvSpareNoFaultyItem.ReadOnly = true;
            this.dgvSpareNoFaultyItem.Width = 35;
            // 
            // dgvSpareNoFaultyMPN
            // 
            this.dgvSpareNoFaultyMPN.DataPropertyName = "MPN";
            this.dgvSpareNoFaultyMPN.HeaderText = "MPN";
            this.dgvSpareNoFaultyMPN.Name = "dgvSpareNoFaultyMPN";
            this.dgvSpareNoFaultyMPN.ReadOnly = true;
            // 
            // dgvSpareNoFaultySerial
            // 
            this.dgvSpareNoFaultySerial.DataPropertyName = "SerialNumber";
            this.dgvSpareNoFaultySerial.HeaderText = "Serial Number";
            this.dgvSpareNoFaultySerial.Name = "dgvSpareNoFaultySerial";
            this.dgvSpareNoFaultySerial.ReadOnly = true;
            // 
            // dgvSpareNoFaultyReturnReason
            // 
            this.dgvSpareNoFaultyReturnReason.HeaderText = "Return Reason";
            this.dgvSpareNoFaultyReturnReason.Items.AddRange(new object[] {
            "Swapped",
            "Return Unused",
            "Faulty out of Box"});
            this.dgvSpareNoFaultyReturnReason.Name = "dgvSpareNoFaultyReturnReason";
            // 
            // dgvSpareNoFaultyFaulty
            // 
            this.dgvSpareNoFaultyFaulty.HeaderText = "Faulty";
            this.dgvSpareNoFaultyFaulty.Name = "dgvSpareNoFaultyFaulty";
            // 
            // SparesNoFaulties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 244);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.dgvSpareNoFaulty);
            this.Controls.Add(this.lblHeader);
            this.Name = "SparesNoFaulties";
            this.Text = "SparesNoFaulties";
            ((System.ComponentModel.ISupportInitialize)(this.dgvSpareNoFaulty)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.DataGridView dgvSpareNoFaulty;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvSpareNoFaultyItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvSpareNoFaultyMPN;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvSpareNoFaultySerial;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvSpareNoFaultyReturnReason;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvSpareNoFaultyFaulty;
    }
}