﻿namespace GumshoeSPLG.Disaster_Recovery
{
    partial class RTCPrompt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtRTCLog = new System.Windows.Forms.TextBox();
            this.txtRTCLogNumber = new System.Windows.Forms.TextBox();
            this.lblRTCLogNumber = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtRTCLog
            // 
            this.txtRTCLog.Location = new System.Drawing.Point(12, 12);
            this.txtRTCLog.Multiline = true;
            this.txtRTCLog.Name = "txtRTCLog";
            this.txtRTCLog.ReadOnly = true;
            this.txtRTCLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtRTCLog.Size = new System.Drawing.Size(486, 355);
            this.txtRTCLog.TabIndex = 1;
            // 
            // txtRTCLogNumber
            // 
            this.txtRTCLogNumber.Location = new System.Drawing.Point(317, 375);
            this.txtRTCLogNumber.Name = "txtRTCLogNumber";
            this.txtRTCLogNumber.Size = new System.Drawing.Size(100, 20);
            this.txtRTCLogNumber.TabIndex = 0;
            // 
            // lblRTCLogNumber
            // 
            this.lblRTCLogNumber.AutoSize = true;
            this.lblRTCLogNumber.Location = new System.Drawing.Point(218, 378);
            this.lblRTCLogNumber.Name = "lblRTCLogNumber";
            this.lblRTCLogNumber.Size = new System.Drawing.Size(93, 13);
            this.lblRTCLogNumber.TabIndex = 2;
            this.lblRTCLogNumber.Text = "RTC Log Number:";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(423, 373);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // RTCPrompt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 408);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblRTCLogNumber);
            this.Controls.Add(this.txtRTCLogNumber);
            this.Controls.Add(this.txtRTCLog);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "RTCPrompt";
            this.Text = "RTCPrompt";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RTCPrompt_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtRTCLog;
        private System.Windows.Forms.TextBox txtRTCLogNumber;
        private System.Windows.Forms.Label lblRTCLogNumber;
        private System.Windows.Forms.Button btnOK;
    }
}