﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GumshoeSPLG.Disaster_Recovery
{
    public partial class RTCPrompt : Form
    {
        private DrSqlWrapper _sql = new DrSqlWrapper();

        public string RTCText
        {
            get { return txtRTCLog.Text; }
            set { txtRTCLog.Text = value; }
        }

        public int IdChange { get; set; }

        public RTCPrompt()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (txtRTCLogNumber.Text.Length >= 5 && txtRTCLogNumber.Text.Length <= 6)
            {
                _sql.UpdateRTCLogNumber(IdChange, txtRTCLogNumber.Text);
                MessageBox.Show(@"Change added to the database successfully!", @"Change Successful",
                    MessageBoxButtons.OK, MessageBoxIcon.None);
                Dispose();
            }
            else
            {
                MessageBox.Show("Please enter an RTC Log number for this change", "No RTC Log Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void RTCPrompt_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (txtRTCLogNumber.Text.Length != 5 && txtRTCLogNumber.Text.Length != 6)
            {
                MessageBox.Show(@"Please enter an RTC Log number for this change", @"No RTC Log Number",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
            else
            {
                _sql.UpdateRTCLogNumber(IdChange, txtRTCLogNumber.Text);
                MessageBox.Show(@"Change added to the database successfully!", @"Change Successful",
                    MessageBoxButtons.OK, MessageBoxIcon.None);
                Dispose();
            }
        }
    }
}
