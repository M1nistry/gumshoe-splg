﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace GumshoeSPLG
{
    public class SparePart : ICloneable, IEquatable<SparePart>
    {
        private string returnReason;
        private List<string> validReturnReasons = new List<string>() { "Swapped", "Return Unused", "Faulty out of Box" };

        public int ID { get; set; }
        public string SerialNumber { get; set; }
        public string MPN { get; set; }
        public string Barcode { get; set; }
        public string MPNDescription { get; set; }
        public string Manufacturer { get; set; }
        public string BinDescription { get; set; }
        public string ReservingDocument { get; set; }
        public string ReservingItem { get; set; }
        public string ConfigParameters { get; set; }
        public string Plant { get; set; }
        public string PlantName { get; set; }
        public string Sloc { get; set; }
        public string SlocDescription { get; set; }
        public string Node { get; set; }
        public int IDAddress { get; set; }
        public int IDLocation { get; set; }
        public int ItemNumber { get; set; }

        public string Connote { get; set; }
        public string NewNodeLocation { get; set; }
        public string NewBinLocation { get; set; }
        public string ReturnToAddress { get; set; }

        public string ReturnReason
        {
            get
            {
                return returnReason;
            }
            
            set
            {
                if (validReturnReasons.Contains(value))
                {
                    returnReason = value;
                }
                else
                    MessageBox.Show("Invalid Return Reason selected", "Invalid Value Given", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public string RTCLogText
        {
            get
            {
                return String.Format("Item Number: {1}{11}{0}" +
                                     "MPN: {2}{0}" +
                                     "Serial: {3}{0}" +
                                     "Barcode: {4}{0}" +
                                     "Config Parameters: {5}{0}" + 
                                     "Location: {6} {7}{0}" +
                                     "{8}{9}{10}", //Connote, Return To Address and New Bin Location if needed
                                     Environment.NewLine,
                                     ItemNumber,
                                     MPN,
                                     SerialNumber,
                                     Barcode,
                                     ConfigParameters, 
                                     Node,
                                     BinDescription,
                                     Connote != null ? String.Format("Connote: {1}{0}", Environment.NewLine, Connote) : String.Empty,
                                     (ReturnReason != null && ReturnReason != "Swapped") ? String.Format("Return To: {1}{0}", Environment.NewLine, ReturnToAddress) : String.Empty,
                                     NewBinLocation != null ? String.Format("New Location: {1}{0}", Environment.NewLine, NewBinLocation) : String.Empty,
                                     (ReturnReason != null && ReturnReason != "Swapped") ? String.Format(" - {0}", ReturnReason) : String.Empty);
            }
        }

        public SparePart()
        {
            
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        public override int GetHashCode()
        {
            return ID;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var item = (SparePart) obj;
            return item != null && Equals(item);
        }

        public bool Equals(SparePart other)
        {
            return (ID == other.ID);
        }

        public override string ToString()
        {
            return String.Format("{0}, MPN: {1}, Serial: {2}", ItemNumber, MPN, SerialNumber);
        }
    }
}
