﻿using System;

namespace GumshoeSPLG
{
    public class StoragePart : IEquatable<StoragePart>
    {
        public int IdPart { get; set; }
        public string SerialNumber { get; set; }
        public string Mpn { get; set; }
        public string BinLocation { get; set; }
        public string Barcode { get; set; }
        public string MpnDescription { get; set; }

        public StoragePart(int id, string serial, string mpn, string binlocation, string barcode, string mpndescription)
        {
            IdPart = id;
            SerialNumber = serial;
            Mpn = mpn;
            BinLocation = binlocation;
            Barcode = barcode;
            MpnDescription = mpndescription;
        }

        public override int GetHashCode()
        {
            return IdPart;
        }

        public bool Equals(StoragePart other)
        {
            return (SerialNumber == other.SerialNumber && Mpn == other.Mpn && BinLocation == other.BinLocation &&
                    Barcode == other.Barcode && MpnDescription == other.MpnDescription);
        }

        public override bool Equals(Object obj)
        {
            if (obj == null) return false;
            var part = (StoragePart) obj;
            return Equals(part);
        }
    }
}
