﻿using System.Windows.Forms;

namespace GumshoeSPLG
{
    public partial class ReservePart : Form
    {
        private readonly SparePart _spare;
        private readonly DrSqlWrapper _sql = new DrSqlWrapper();

        public ReservePart(SparePart sparePart)
        {
            InitializeComponent();
            _spare = sparePart;
        }

        private void buttonOk_Click(object sender, System.EventArgs e)
        {
            var change = _sql.AddChange("Reserve", textBoxTicketNumber.Text, textBoxTaskId.Text, "RESV",
                textBoxReference.Text, textBoxComments.Text);
            _sql.AddSpareChange(change, _spare);
            MessageBox.Show(@"Reserved Successfully");
            Dispose();
        }

        private void buttonCancel_Click(object sender, System.EventArgs e)
        {
            Dispose();
        }
    }
}
