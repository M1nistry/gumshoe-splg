﻿namespace GumshoeSPLG
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.aboutImageBox = new System.Windows.Forms.PictureBox();
            this.titleLabel = new System.Windows.Forms.Label();
            this.creditLabel = new System.Windows.Forms.Label();
            this.versionLabel = new System.Windows.Forms.Label();
            this.directoryLabel = new System.Windows.Forms.Label();
            this.hiddenPannel = new System.Windows.Forms.Panel();
            this.contactLabel = new System.Windows.Forms.Label();
            this.emailLinkLabel = new System.Windows.Forms.LinkLabel();
            this.faceLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.aboutImageBox)).BeginInit();
            this.hiddenPannel.SuspendLayout();
            this.SuspendLayout();
            // 
            // aboutImageBox
            // 
            this.aboutImageBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.aboutImageBox.Image = global::GumshoeSPLG.Properties.Resources.aboutImage;
            this.aboutImageBox.Location = new System.Drawing.Point(12, 13);
            this.aboutImageBox.Name = "aboutImageBox";
            this.aboutImageBox.Size = new System.Drawing.Size(232, 156);
            this.aboutImageBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.aboutImageBox.TabIndex = 0;
            this.aboutImageBox.TabStop = false;
            // 
            // titleLabel
            // 
            this.titleLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(67, 184);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(116, 16);
            this.titleLabel.TabIndex = 1;
            this.titleLabel.Text = "Gumshoe SPLG";
            // 
            // creditLabel
            // 
            this.creditLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.creditLabel.AutoSize = true;
            this.creditLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creditLabel.Location = new System.Drawing.Point(19, 207);
            this.creditLabel.Name = "creditLabel";
            this.creditLabel.Size = new System.Drawing.Size(214, 12);
            this.creditLabel.TabIndex = 2;
            this.creditLabel.Text = "Developed by Beau Palmer-Brame and Brian Parish";
            // 
            // versionLabel
            // 
            this.versionLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.versionLabel.AutoSize = true;
            this.versionLabel.Enabled = false;
            this.versionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.versionLabel.Location = new System.Drawing.Point(169, 324);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(37, 9);
            this.versionLabel.TabIndex = 3;
            this.versionLabel.Text = "Version | ";
            // 
            // directoryLabel
            // 
            this.directoryLabel.AutoSize = true;
            this.directoryLabel.Enabled = false;
            this.directoryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.directoryLabel.Location = new System.Drawing.Point(1, 11);
            this.directoryLabel.Name = "directoryLabel";
            this.directoryLabel.Size = new System.Drawing.Size(0, 9);
            this.directoryLabel.TabIndex = 4;
            this.directoryLabel.Visible = false;
            // 
            // hiddenPannel
            // 
            this.hiddenPannel.Controls.Add(this.directoryLabel);
            this.hiddenPannel.Location = new System.Drawing.Point(-1, 272);
            this.hiddenPannel.Name = "hiddenPannel";
            this.hiddenPannel.Size = new System.Drawing.Size(257, 44);
            this.hiddenPannel.TabIndex = 5;
            this.hiddenPannel.MouseLeave += new System.EventHandler(this.hiddenPannel_MouseLeave);
            this.hiddenPannel.MouseHover += new System.EventHandler(this.hiddenPannel_MouseHover);
            // 
            // contactLabel
            // 
            this.contactLabel.AutoSize = true;
            this.contactLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contactLabel.Location = new System.Drawing.Point(87, 228);
            this.contactLabel.Name = "contactLabel";
            this.contactLabel.Size = new System.Drawing.Size(41, 12);
            this.contactLabel.TabIndex = 6;
            this.contactLabel.Text = "Contact:";
            // 
            // emailLinkLabel
            // 
            this.emailLinkLabel.AutoSize = true;
            this.emailLinkLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailLinkLabel.Location = new System.Drawing.Point(134, 228);
            this.emailLinkLabel.Name = "emailLinkLabel";
            this.emailLinkLabel.Size = new System.Drawing.Size(31, 12);
            this.emailLinkLabel.TabIndex = 7;
            this.emailLinkLabel.TabStop = true;
            this.emailLinkLabel.Text = "E-mail";
            this.emailLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.emailLinkLabel_LinkClicked);
            // 
            // faceLabel
            // 
            this.faceLabel.AutoSize = true;
            this.faceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.faceLabel.Location = new System.Drawing.Point(103, 269);
            this.faceLabel.Name = "faceLabel";
            this.faceLabel.Size = new System.Drawing.Size(49, 20);
            this.faceLabel.TabIndex = 5;
            this.faceLabel.Text = "( ͡° ͜ʖ ͡°)";
            this.faceLabel.Visible = false;
            // 
            // About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(256, 337);
            this.Controls.Add(this.faceLabel);
            this.Controls.Add(this.emailLinkLabel);
            this.Controls.Add(this.contactLabel);
            this.Controls.Add(this.hiddenPannel);
            this.Controls.Add(this.versionLabel);
            this.Controls.Add(this.creditLabel);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.aboutImageBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "About";
            this.Text = "Gumshoe SPLG";
            ((System.ComponentModel.ISupportInitialize)(this.aboutImageBox)).EndInit();
            this.hiddenPannel.ResumeLayout(false);
            this.hiddenPannel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox aboutImageBox;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label creditLabel;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.Label directoryLabel;
        private System.Windows.Forms.Panel hiddenPannel;
        private System.Windows.Forms.Label contactLabel;
        private System.Windows.Forms.LinkLabel emailLinkLabel;
        private System.Windows.Forms.Label faceLabel;
    }
}