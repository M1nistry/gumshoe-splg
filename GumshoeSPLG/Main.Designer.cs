﻿namespace GumshoeSPLG
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.sendToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addonsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.complexConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rFSTrackerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.issueTrackerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.puDoAuditsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disasterRecoveryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.alwaysOnTopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hotkeysToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.templateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editTemplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wikiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.errorLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkForUpdatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDragHere = new System.Windows.Forms.ToolStripMenuItem();
            this.tasLabel = new System.Windows.Forms.Label();
            this.tasTextBox = new System.Windows.Forms.TextBox();
            this.workOrderLabel = new System.Windows.Forms.Label();
            this.workOrderTextBox = new System.Windows.Forms.TextBox();
            this.faultySerialLabel = new System.Windows.Forms.Label();
            this.faultySerialBox = new System.Windows.Forms.TextBox();
            this.faultyMPNLabel = new System.Windows.Forms.Label();
            this.faultyMPNBox = new System.Windows.Forms.TextBox();
            this.spareSerialLabel = new System.Windows.Forms.Label();
            this.spareSerialBox = new System.Windows.Forms.TextBox();
            this.spareMPNLabel = new System.Windows.Forms.Label();
            this.spareMPNBox = new System.Windows.Forms.TextBox();
            this.spareBox = new System.Windows.Forms.GroupBox();
            this.faultyBox = new System.Windows.Forms.GroupBox();
            this.ctTextBox = new System.Windows.Forms.TextBox();
            this.agsBoxContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Undo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.Cut = new System.Windows.Forms.ToolStripMenuItem();
            this.Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.Paste = new System.Windows.Forms.ToolStripMenuItem();
            this.Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.SelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.NewTicket = new System.Windows.Forms.ToolStripMenuItem();
            this.nodeLabel = new System.Windows.Forms.Label();
            this.nodeTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pudoTextBox = new System.Windows.Forms.TextBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.connoteTextBox = new System.Windows.Forms.TextBox();
            this.ticketTypeCombo = new System.Windows.Forms.ComboBox();
            this.additionalSerial = new System.Windows.Forms.TextBox();
            this.additionalMPN = new System.Windows.Forms.TextBox();
            this.hotkeyLabel = new System.Windows.Forms.Label();
            this.hotkeyCheckBox = new System.Windows.Forms.CheckBox();
            this.personDetailsDGV = new System.Windows.Forms.DataGridView();
            this.typeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.level1GroupBox = new System.Windows.Forms.GroupBox();
            this.agsListView = new System.Windows.Forms.ListView();
            this.nameColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.agsColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBoxMMGGeneral = new System.Windows.Forms.GroupBox();
            this.textMMGReservation = new System.Windows.Forms.TextBox();
            this.labelReservationNumber = new System.Windows.Forms.Label();
            this.rtcLinkLabel = new System.Windows.Forms.LinkLabel();
            this.newTicketLinkLabel = new System.Windows.Forms.LinkLabel();
            this.rtcTextBox = new System.Windows.Forms.TextBox();
            this.agsLink = new System.Windows.Forms.LinkLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePickerSys = new System.Windows.Forms.DateTimePicker();
            this.courierCombo = new System.Windows.Forms.ComboBox();
            this.courierLabel = new System.Windows.Forms.Label();
            this.miscTab = new System.Windows.Forms.TabControl();
            this.tabNotes = new System.Windows.Forms.TabPage();
            this.linkPod = new System.Windows.Forms.LinkLabel();
            this.hotkeysSysLog = new System.Windows.Forms.Label();
            this.judgementLabel = new System.Windows.Forms.Label();
            this.rtbExtended = new RichTextBoxExtended.RichTextBoxExtended();
            this.additionalAddbutton = new System.Windows.Forms.Button();
            this.additionalSpareradio = new System.Windows.Forms.RadioButton();
            this.additionalFaultradio = new System.Windows.Forms.RadioButton();
            this.additionalSeriallabel = new System.Windows.Forms.Label();
            this.additionalMPNlabel = new System.Windows.Forms.Label();
            this.tabDataSearch = new System.Windows.Forms.TabPage();
            this.tabControlData = new System.Windows.Forms.TabControl();
            this.tabLocations = new System.Windows.Forms.TabPage();
            this.dgvLocations = new System.Windows.Forms.DataGridView();
            this.plntColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.slocColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.storageLocationColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nodeSearchColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.E = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.latitudeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.longitudeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.locationTypeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.searchTerm2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabKits = new System.Windows.Forms.TabPage();
            this.kitGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kitColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nodeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabAddresses = new System.Windows.Forms.TabPage();
            this.addressDGV = new System.Windows.Forms.DataGridView();
            this.PlntCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SLoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addrNoHidden = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HouseNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StreetCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CityCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PostlCodeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TranspZoneHidden = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PuDoCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name4col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cOName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabConsumables = new System.Windows.Forms.TabPage();
            this.consumableDGV = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelClearSearch = new System.Windows.Forms.Label();
            this.comboDataSelect = new System.Windows.Forms.ComboBox();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.personDetails = new System.Windows.Forms.TabPage();
            this.linkShowResults = new System.Windows.Forms.LinkLabel();
            this.postCodeTextBox = new System.Windows.Forms.TextBox();
            this.locationDropDown = new System.Windows.Forms.ComboBox();
            this.backButton = new System.Windows.Forms.Button();
            this.searchPeopleButton = new System.Windows.Forms.Button();
            this.surNameTextBox = new System.Windows.Forms.TextBox();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.surNameLabel = new System.Windows.Forms.Label();
            this.firstNameLabel = new System.Windows.Forms.Label();
            this.personSearchDGV = new System.Windows.Forms.DataGridView();
            this.nameSearch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.locationSearch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phoneSearch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.employeeSearch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabMisc = new System.Windows.Forms.TabPage();
            this.tabControlMisc = new System.Windows.Forms.TabControl();
            this.tabAdder = new System.Windows.Forms.TabPage();
            this.statusListBox = new System.Windows.Forms.ListBox();
            this.sysLogsGroupBox = new System.Windows.Forms.GroupBox();
            this.peopleGroupBox = new System.Windows.Forms.GroupBox();
            this.ctNameTextBox = new System.Windows.Forms.TextBox();
            this.agsLinkSys = new System.Windows.Forms.LinkLabel();
            this.agsTextBoxSys = new System.Windows.Forms.TextBox();
            this.userLinkLabelSys = new System.Windows.Forms.LinkLabel();
            this.ctNameLabel = new System.Windows.Forms.Label();
            this.userAGSBoxSys = new System.Windows.Forms.TextBox();
            this.nameTextBoxSys = new System.Windows.Forms.TextBox();
            this.nameLabelSys = new System.Windows.Forms.Label();
            this.ticketTextBoxSys = new System.Windows.Forms.TextBox();
            this.ticketLabelSys = new System.Windows.Forms.Label();
            this.rtcTextBoxSys = new System.Windows.Forms.TextBox();
            this.rtcLabelSys = new System.Windows.Forms.Label();
            this.taskComboBoxSys = new System.Windows.Forms.ComboBox();
            this.taskCatLabelSys = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.workOrderTextBoxSys = new System.Windows.Forms.TextBox();
            this.workOrderLabelSys = new System.Windows.Forms.Label();
            this.errorMessageTextBox = new System.Windows.Forms.TextBox();
            this.errorLabel = new System.Windows.Forms.Label();
            this.saveLabel = new System.Windows.Forms.Label();
            this.rtbRightClick = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agsLookup = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.agsContextTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addressesContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyFullAddressToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyFullAddressFormattedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.happyLabel = new System.Windows.Forms.Label();
            this.secretGroupBox = new System.Windows.Forms.GroupBox();
            this.hiddenMarioBox = new System.Windows.Forms.RichTextBox();
            this.extendedStatusStrip = new GumshoePuDo.ExtendedStatusStrip();
            this.menuStrip1.SuspendLayout();
            this.spareBox.SuspendLayout();
            this.faultyBox.SuspendLayout();
            this.agsBoxContextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.personDetailsDGV)).BeginInit();
            this.level1GroupBox.SuspendLayout();
            this.groupBoxMMGGeneral.SuspendLayout();
            this.miscTab.SuspendLayout();
            this.tabNotes.SuspendLayout();
            this.tabDataSearch.SuspendLayout();
            this.tabControlData.SuspendLayout();
            this.tabLocations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocations)).BeginInit();
            this.tabKits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kitGrid)).BeginInit();
            this.tabAddresses.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.addressDGV)).BeginInit();
            this.tabConsumables.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.consumableDGV)).BeginInit();
            this.personDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.personSearchDGV)).BeginInit();
            this.tabMisc.SuspendLayout();
            this.tabControlMisc.SuspendLayout();
            this.sysLogsGroupBox.SuspendLayout();
            this.peopleGroupBox.SuspendLayout();
            this.rtbRightClick.SuspendLayout();
            this.agsLookup.SuspendLayout();
            this.addressesContextMenu.SuspendLayout();
            this.secretGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.toolStripDragHere});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(392, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.sendToolStripMenuItem1,
            this.toolStripSeparator1,
            this.sendToolStripMenuItem,
            this.importToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.ToolTipText = "Save the current field entries to the cache";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // sendToolStripMenuItem1
            // 
            this.sendToolStripMenuItem1.Name = "sendToolStripMenuItem1";
            this.sendToolStripMenuItem1.Size = new System.Drawing.Size(144, 22);
            this.sendToolStripMenuItem1.Text = "Send to SPLG";
            this.sendToolStripMenuItem1.ToolTipText = "Sends an e-mail copy of this ticket to the SPLG mail";
            this.sendToolStripMenuItem1.Click += new System.EventHandler(this.sendToolStripMenuItem1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(141, 6);
            // 
            // sendToolStripMenuItem
            // 
            this.sendToolStripMenuItem.Name = "sendToolStripMenuItem";
            this.sendToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.sendToolStripMenuItem.Text = "Export";
            this.sendToolStripMenuItem.ToolTipText = "Exports the current job to string which can be quickly sent to someone to import";
            this.sendToolStripMenuItem.Click += new System.EventHandler(this.sendToolStripMenuItem_Click);
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStripTextBox});
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.importToolStripMenuItem.Text = "Import";
            // 
            // importToolStripTextBox
            // 
            this.importToolStripTextBox.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.importToolStripTextBox.Name = "importToolStripTextBox";
            this.importToolStripTextBox.Size = new System.Drawing.Size(120, 23);
            this.importToolStripTextBox.Text = "Paste here...";
            this.importToolStripTextBox.ToolTipText = "Paste the compressed ticket here";
            this.importToolStripTextBox.Click += new System.EventHandler(this.importToolStripTextBox_Click);
            this.importToolStripTextBox.MouseEnter += new System.EventHandler(this.importToolStripTextBox_MouseEnter);
            this.importToolStripTextBox.TextChanged += new System.EventHandler(this.importToolStripTextBox_TextChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(141, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.historyToolStripMenuItem,
            this.addonsToolStripMenuItem,
            this.disasterRecoveryToolStripMenuItem,
            this.toolStripSeparator3,
            this.alwaysOnTopToolStripMenuItem,
            this.hotkeysToolStripMenuItem,
            this.templateToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // historyToolStripMenuItem
            // 
            this.historyToolStripMenuItem.Name = "historyToolStripMenuItem";
            this.historyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.historyToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.historyToolStripMenuItem.Text = "History";
            this.historyToolStripMenuItem.Click += new System.EventHandler(this.historyToolStripMenuItem_Click);
            // 
            // addonsToolStripMenuItem
            // 
            this.addonsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.complexConfigToolStripMenuItem,
            this.rFSTrackerToolStripMenuItem,
            this.issueTrackerToolStripMenuItem,
            this.puDoAuditsToolStripMenuItem});
            this.addonsToolStripMenuItem.Name = "addonsToolStripMenuItem";
            this.addonsToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.addonsToolStripMenuItem.Text = "Add-ons";
            // 
            // complexConfigToolStripMenuItem
            // 
            this.complexConfigToolStripMenuItem.Name = "complexConfigToolStripMenuItem";
            this.complexConfigToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.complexConfigToolStripMenuItem.Text = "Complex Config";
            this.complexConfigToolStripMenuItem.Click += new System.EventHandler(this.complexConfigToolStripMenuItem_Click);
            // 
            // rFSTrackerToolStripMenuItem
            // 
            this.rFSTrackerToolStripMenuItem.Name = "rFSTrackerToolStripMenuItem";
            this.rFSTrackerToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.rFSTrackerToolStripMenuItem.Text = "RFS Tracker";
            this.rFSTrackerToolStripMenuItem.Click += new System.EventHandler(this.rFSTrackerToolStripMenuItem_Click);
            // 
            // issueTrackerToolStripMenuItem
            // 
            this.issueTrackerToolStripMenuItem.Name = "issueTrackerToolStripMenuItem";
            this.issueTrackerToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.issueTrackerToolStripMenuItem.Text = "Issue Tracker";
            this.issueTrackerToolStripMenuItem.Click += new System.EventHandler(this.issueTrackerToolStripMenuItem_Click);
            // 
            // puDoAuditsToolStripMenuItem
            // 
            this.puDoAuditsToolStripMenuItem.Name = "puDoAuditsToolStripMenuItem";
            this.puDoAuditsToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.puDoAuditsToolStripMenuItem.Text = "PuDo Audits";
            this.puDoAuditsToolStripMenuItem.Click += new System.EventHandler(this.puDoAuditsToolStripMenuItem_Click);
            // 
            // disasterRecoveryToolStripMenuItem
            // 
            this.disasterRecoveryToolStripMenuItem.Name = "disasterRecoveryToolStripMenuItem";
            this.disasterRecoveryToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.disasterRecoveryToolStripMenuItem.Text = "Disaster Recovery";
            this.disasterRecoveryToolStripMenuItem.ToolTipText = "Interface for DR while SPS is down";
            this.disasterRecoveryToolStripMenuItem.Click += new System.EventHandler(this.disasterRecoveryToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(163, 6);
            // 
            // alwaysOnTopToolStripMenuItem
            // 
            this.alwaysOnTopToolStripMenuItem.CheckOnClick = true;
            this.alwaysOnTopToolStripMenuItem.Name = "alwaysOnTopToolStripMenuItem";
            this.alwaysOnTopToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.alwaysOnTopToolStripMenuItem.Text = "Always On Top";
            this.alwaysOnTopToolStripMenuItem.Click += new System.EventHandler(this.alwaysOnTopToolStripMenuItem_Click);
            // 
            // hotkeysToolStripMenuItem
            // 
            this.hotkeysToolStripMenuItem.Checked = true;
            this.hotkeysToolStripMenuItem.CheckOnClick = true;
            this.hotkeysToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.hotkeysToolStripMenuItem.Name = "hotkeysToolStripMenuItem";
            this.hotkeysToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.hotkeysToolStripMenuItem.Text = "Hotkeys";
            this.hotkeysToolStripMenuItem.Click += new System.EventHandler(this.hotkeysToolStripMenuItem_Click);
            // 
            // templateToolStripMenuItem
            // 
            this.templateToolStripMenuItem.Checked = true;
            this.templateToolStripMenuItem.CheckOnClick = true;
            this.templateToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.templateToolStripMenuItem.Name = "templateToolStripMenuItem";
            this.templateToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.templateToolStripMenuItem.Text = "Template";
            this.templateToolStripMenuItem.CheckedChanged += new System.EventHandler(this.templateToolStripMenuItem_CheckedChanged);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editTemplateToolStripMenuItem,
            this.settingsToolStripMenuItem1});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // editTemplateToolStripMenuItem
            // 
            this.editTemplateToolStripMenuItem.Name = "editTemplateToolStripMenuItem";
            this.editTemplateToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.editTemplateToolStripMenuItem.Text = "Edit Template";
            this.editTemplateToolStripMenuItem.Click += new System.EventHandler(this.editTemplateToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem1
            // 
            this.settingsToolStripMenuItem1.Name = "settingsToolStripMenuItem1";
            this.settingsToolStripMenuItem1.Size = new System.Drawing.Size(147, 22);
            this.settingsToolStripMenuItem1.Text = "Settings";
            this.settingsToolStripMenuItem1.Click += new System.EventHandler(this.settingsToolStripMenuItem1_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wikiToolStripMenuItem,
            this.changeLogToolStripMenuItem,
            this.errorLogToolStripMenuItem,
            this.checkForUpdatesToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // wikiToolStripMenuItem
            // 
            this.wikiToolStripMenuItem.Name = "wikiToolStripMenuItem";
            this.wikiToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.wikiToolStripMenuItem.Text = "Wiki";
            this.wikiToolStripMenuItem.Click += new System.EventHandler(this.wikiToolStripMenuItem_Click);
            // 
            // changeLogToolStripMenuItem
            // 
            this.changeLogToolStripMenuItem.Name = "changeLogToolStripMenuItem";
            this.changeLogToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.changeLogToolStripMenuItem.Text = "Change log";
            this.changeLogToolStripMenuItem.Click += new System.EventHandler(this.changeLogToolStripMenuItem_Click);
            // 
            // errorLogToolStripMenuItem
            // 
            this.errorLogToolStripMenuItem.Name = "errorLogToolStripMenuItem";
            this.errorLogToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.errorLogToolStripMenuItem.Text = "Error log";
            this.errorLogToolStripMenuItem.Click += new System.EventHandler(this.errorLogToolStripMenuItem_Click);
            // 
            // checkForUpdatesToolStripMenuItem
            // 
            this.checkForUpdatesToolStripMenuItem.Name = "checkForUpdatesToolStripMenuItem";
            this.checkForUpdatesToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.checkForUpdatesToolStripMenuItem.Text = "Check for updates";
            this.checkForUpdatesToolStripMenuItem.Click += new System.EventHandler(this.checkForUpdatesToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // toolStripDragHere
            // 
            this.toolStripDragHere.Name = "toolStripDragHere";
            this.toolStripDragHere.Size = new System.Drawing.Size(72, 20);
            this.toolStripDragHere.Text = "Drag Here";
            this.toolStripDragHere.Visible = false;
            // 
            // tasLabel
            // 
            this.tasLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.tasLabel.AutoSize = true;
            this.tasLabel.Location = new System.Drawing.Point(7, 28);
            this.tasLabel.Name = "tasLabel";
            this.tasLabel.Size = new System.Drawing.Size(31, 13);
            this.tasLabel.TabIndex = 1;
            this.tasLabel.Text = "TAS:";
            // 
            // tasTextBox
            // 
            this.tasTextBox.AllowDrop = true;
            this.tasTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tasTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tasTextBox.Location = new System.Drawing.Point(69, 24);
            this.tasTextBox.MaxLength = 17;
            this.tasTextBox.Name = "tasTextBox";
            this.tasTextBox.Size = new System.Drawing.Size(117, 20);
            this.tasTextBox.TabIndex = 2;
            this.tasTextBox.Text = "TAS00000";
            this.toolTip.SetToolTip(this.tasTextBox, "Job TAS number (CTRL + 1)");
            this.tasTextBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tasTextBox_Click);
            // 
            // workOrderLabel
            // 
            this.workOrderLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.workOrderLabel.AutoSize = true;
            this.workOrderLabel.Location = new System.Drawing.Point(194, 27);
            this.workOrderLabel.Name = "workOrderLabel";
            this.workOrderLabel.Size = new System.Drawing.Size(36, 13);
            this.workOrderLabel.TabIndex = 3;
            this.workOrderLabel.Text = "Order:";
            // 
            // workOrderTextBox
            // 
            this.workOrderTextBox.AllowDrop = true;
            this.workOrderTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.workOrderTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.workOrderTextBox.Location = new System.Drawing.Point(238, 25);
            this.workOrderTextBox.MaxLength = 12;
            this.workOrderTextBox.Name = "workOrderTextBox";
            this.workOrderTextBox.Size = new System.Drawing.Size(129, 20);
            this.workOrderTextBox.TabIndex = 3;
            this.toolTip.SetToolTip(this.workOrderTextBox, "Work order (CTRL + 2)");
            this.workOrderTextBox.Click += new System.EventHandler(this.workOrderTextBox_Click);
            // 
            // faultySerialLabel
            // 
            this.faultySerialLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.faultySerialLabel.AutoSize = true;
            this.faultySerialLabel.Location = new System.Drawing.Point(3, 22);
            this.faultySerialLabel.Name = "faultySerialLabel";
            this.faultySerialLabel.Size = new System.Drawing.Size(36, 13);
            this.faultySerialLabel.TabIndex = 6;
            this.faultySerialLabel.Text = "Serial:";
            // 
            // faultySerialBox
            // 
            this.faultySerialBox.AllowDrop = true;
            this.faultySerialBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.faultySerialBox.BackColor = System.Drawing.SystemColors.Window;
            this.faultySerialBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.faultySerialBox.Location = new System.Drawing.Point(39, 19);
            this.faultySerialBox.MaxLength = 25;
            this.faultySerialBox.Name = "faultySerialBox";
            this.faultySerialBox.Size = new System.Drawing.Size(138, 20);
            this.faultySerialBox.TabIndex = 7;
            this.toolTip.SetToolTip(this.faultySerialBox, "Faulty parts serial (CTRL + 4)");
            this.faultySerialBox.Click += new System.EventHandler(this.faultySerialBox_Click);
            // 
            // faultyMPNLabel
            // 
            this.faultyMPNLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.faultyMPNLabel.AutoSize = true;
            this.faultyMPNLabel.Location = new System.Drawing.Point(187, 22);
            this.faultyMPNLabel.Name = "faultyMPNLabel";
            this.faultyMPNLabel.Size = new System.Drawing.Size(34, 13);
            this.faultyMPNLabel.TabIndex = 8;
            this.faultyMPNLabel.Text = "MPN:";
            // 
            // faultyMPNBox
            // 
            this.faultyMPNBox.AllowDrop = true;
            this.faultyMPNBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.faultyMPNBox.BackColor = System.Drawing.SystemColors.Window;
            this.faultyMPNBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.faultyMPNBox.Location = new System.Drawing.Point(229, 19);
            this.faultyMPNBox.MaxLength = 25;
            this.faultyMPNBox.Name = "faultyMPNBox";
            this.faultyMPNBox.Size = new System.Drawing.Size(132, 20);
            this.faultyMPNBox.TabIndex = 8;
            this.toolTip.SetToolTip(this.faultyMPNBox, "Faulty part number (CTRL + 5)");
            this.faultyMPNBox.Click += new System.EventHandler(this.faultyMPNBox_Click);
            // 
            // spareSerialLabel
            // 
            this.spareSerialLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.spareSerialLabel.AutoSize = true;
            this.spareSerialLabel.Location = new System.Drawing.Point(2, 22);
            this.spareSerialLabel.Name = "spareSerialLabel";
            this.spareSerialLabel.Size = new System.Drawing.Size(36, 13);
            this.spareSerialLabel.TabIndex = 11;
            this.spareSerialLabel.Text = "Serial:";
            // 
            // spareSerialBox
            // 
            this.spareSerialBox.AllowDrop = true;
            this.spareSerialBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.spareSerialBox.BackColor = System.Drawing.SystemColors.Window;
            this.spareSerialBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.spareSerialBox.Location = new System.Drawing.Point(39, 19);
            this.spareSerialBox.MaxLength = 25;
            this.spareSerialBox.Name = "spareSerialBox";
            this.spareSerialBox.Size = new System.Drawing.Size(138, 20);
            this.spareSerialBox.TabIndex = 9;
            this.toolTip.SetToolTip(this.spareSerialBox, "Spare serial (CTRL + 6)");
            this.spareSerialBox.Click += new System.EventHandler(this.spareSerialBox_Click);
            // 
            // spareMPNLabel
            // 
            this.spareMPNLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.spareMPNLabel.AutoSize = true;
            this.spareMPNLabel.Location = new System.Drawing.Point(187, 22);
            this.spareMPNLabel.Name = "spareMPNLabel";
            this.spareMPNLabel.Size = new System.Drawing.Size(34, 13);
            this.spareMPNLabel.TabIndex = 13;
            this.spareMPNLabel.Text = "MPN:";
            // 
            // spareMPNBox
            // 
            this.spareMPNBox.AllowDrop = true;
            this.spareMPNBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.spareMPNBox.BackColor = System.Drawing.SystemColors.Window;
            this.spareMPNBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.spareMPNBox.Location = new System.Drawing.Point(229, 19);
            this.spareMPNBox.MaxLength = 25;
            this.spareMPNBox.Name = "spareMPNBox";
            this.spareMPNBox.Size = new System.Drawing.Size(132, 20);
            this.spareMPNBox.TabIndex = 10;
            this.toolTip.SetToolTip(this.spareMPNBox, "Spare part number (CTRL + 7)");
            this.spareMPNBox.Click += new System.EventHandler(this.spareMPNBox_Click);
            // 
            // spareBox
            // 
            this.spareBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.spareBox.Controls.Add(this.spareSerialBox);
            this.spareBox.Controls.Add(this.spareMPNBox);
            this.spareBox.Controls.Add(this.spareSerialLabel);
            this.spareBox.Controls.Add(this.spareMPNLabel);
            this.spareBox.Location = new System.Drawing.Point(6, 133);
            this.spareBox.Name = "spareBox";
            this.spareBox.Size = new System.Drawing.Size(374, 51);
            this.spareBox.TabIndex = 12;
            this.spareBox.TabStop = false;
            this.spareBox.Text = "Spare ";
            // 
            // faultyBox
            // 
            this.faultyBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.faultyBox.Controls.Add(this.faultySerialBox);
            this.faultyBox.Controls.Add(this.faultySerialLabel);
            this.faultyBox.Controls.Add(this.faultyMPNBox);
            this.faultyBox.Controls.Add(this.faultyMPNLabel);
            this.faultyBox.Location = new System.Drawing.Point(6, 79);
            this.faultyBox.Name = "faultyBox";
            this.faultyBox.Size = new System.Drawing.Size(374, 51);
            this.faultyBox.TabIndex = 11;
            this.faultyBox.TabStop = false;
            this.faultyBox.Text = "Faulty";
            // 
            // ctTextBox
            // 
            this.ctTextBox.AllowDrop = true;
            this.ctTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctTextBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ctTextBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.ctTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ctTextBox.ContextMenuStrip = this.agsBoxContextMenu;
            this.ctTextBox.Location = new System.Drawing.Point(67, 56);
            this.ctTextBox.MaxLength = 25;
            this.ctTextBox.Name = "ctTextBox";
            this.ctTextBox.Size = new System.Drawing.Size(117, 20);
            this.ctTextBox.TabIndex = 4;
            this.toolTip.SetToolTip(this.ctTextBox, "CT AGS (CTRL + 8)");
            this.ctTextBox.Click += new System.EventHandler(this.ctTextBox_Click);
            this.ctTextBox.Enter += new System.EventHandler(this.ctTextBox_Enter);
            this.ctTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ctTextBox_KeyDown);
            this.ctTextBox.Leave += new System.EventHandler(this.ctTextBox_Leave);
            // 
            // agsBoxContextMenu
            // 
            this.agsBoxContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Undo,
            this.toolStripSeparator4,
            this.Cut,
            this.Copy,
            this.Paste,
            this.Delete,
            this.toolStripSeparator5,
            this.SelectAll,
            this.toolStripSeparator6,
            this.NewTicket});
            this.agsBoxContextMenu.Name = "agsBoxContextMenu";
            this.agsBoxContextMenu.Size = new System.Drawing.Size(165, 176);
            this.agsBoxContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.agsBoxContextMenu_Opening);
            // 
            // Undo
            // 
            this.Undo.Name = "Undo";
            this.Undo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.Undo.Size = new System.Drawing.Size(164, 22);
            this.Undo.Text = "Undo";
            this.Undo.Click += new System.EventHandler(this.Undo_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(161, 6);
            // 
            // Cut
            // 
            this.Cut.Name = "Cut";
            this.Cut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.Cut.Size = new System.Drawing.Size(164, 22);
            this.Cut.Text = "Cut";
            this.Cut.Click += new System.EventHandler(this.Cut_Click);
            // 
            // Copy
            // 
            this.Copy.Name = "Copy";
            this.Copy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.Copy.Size = new System.Drawing.Size(164, 22);
            this.Copy.Text = "Copy";
            this.Copy.Click += new System.EventHandler(this.Copy_Click);
            // 
            // Paste
            // 
            this.Paste.Name = "Paste";
            this.Paste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.Paste.Size = new System.Drawing.Size(164, 22);
            this.Paste.Text = "Paste";
            this.Paste.Click += new System.EventHandler(this.Paste_Click);
            // 
            // Delete
            // 
            this.Delete.Name = "Delete";
            this.Delete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.Delete.Size = new System.Drawing.Size(164, 22);
            this.Delete.Text = "Delete";
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(161, 6);
            // 
            // SelectAll
            // 
            this.SelectAll.Name = "SelectAll";
            this.SelectAll.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.SelectAll.Size = new System.Drawing.Size(164, 22);
            this.SelectAll.Text = "Select All";
            this.SelectAll.Click += new System.EventHandler(this.SelectAll_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(161, 6);
            // 
            // NewTicket
            // 
            this.NewTicket.Name = "NewTicket";
            this.NewTicket.Size = new System.Drawing.Size(164, 22);
            this.NewTicket.Text = "New Ticket";
            this.NewTicket.Click += new System.EventHandler(this.NewTicket_Click);
            // 
            // nodeLabel
            // 
            this.nodeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nodeLabel.AutoSize = true;
            this.nodeLabel.Location = new System.Drawing.Point(192, 59);
            this.nodeLabel.Name = "nodeLabel";
            this.nodeLabel.Size = new System.Drawing.Size(36, 13);
            this.nodeLabel.TabIndex = 19;
            this.nodeLabel.Text = "Node:";
            // 
            // nodeTextBox
            // 
            this.nodeTextBox.AllowDrop = true;
            this.nodeTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nodeTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.nodeTextBox.Location = new System.Drawing.Point(238, 56);
            this.nodeTextBox.MaxLength = 4;
            this.nodeTextBox.Name = "nodeTextBox";
            this.nodeTextBox.Size = new System.Drawing.Size(40, 20);
            this.nodeTextBox.TabIndex = 5;
            this.toolTip.SetToolTip(this.nodeTextBox, "Fault node code (CTRL + 3)");
            this.nodeTextBox.Click += new System.EventHandler(this.nodeTextBox_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(284, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "PuDo:";
            // 
            // pudoTextBox
            // 
            this.pudoTextBox.AllowDrop = true;
            this.pudoTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pudoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.pudoTextBox.Location = new System.Drawing.Point(327, 56);
            this.pudoTextBox.MaxLength = 4;
            this.pudoTextBox.Name = "pudoTextBox";
            this.pudoTextBox.Size = new System.Drawing.Size(40, 20);
            this.pudoTextBox.TabIndex = 6;
            this.toolTip.SetToolTip(this.pudoTextBox, "PuDo of part being left at (CTRL + 9)");
            this.pudoTextBox.Click += new System.EventHandler(this.pudoTextBox_Click);
            this.pudoTextBox.TextChanged += new System.EventHandler(this.pudoTextBox_TextChanged);
            // 
            // connoteTextBox
            // 
            this.connoteTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.connoteTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.connoteTextBox.Location = new System.Drawing.Point(235, 194);
            this.connoteTextBox.MaxLength = 20;
            this.connoteTextBox.Name = "connoteTextBox";
            this.connoteTextBox.Size = new System.Drawing.Size(132, 20);
            this.connoteTextBox.TabIndex = 14;
            this.toolTip.SetToolTip(this.connoteTextBox, "Connote being sent on (CTRL + 0)");
            this.connoteTextBox.Click += new System.EventHandler(this.connoteTextBox_Click);
            this.connoteTextBox.TextChanged += new System.EventHandler(this.connoteTextBox_TextChanged);
            // 
            // ticketTypeCombo
            // 
            this.ticketTypeCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.ticketTypeCombo.FormattingEnabled = true;
            this.ticketTypeCombo.Items.AddRange(new object[] {
            "TAS",
            "SiiAM",
            "Other"});
            this.ticketTypeCombo.Location = new System.Drawing.Point(6, 23);
            this.ticketTypeCombo.Name = "ticketTypeCombo";
            this.ticketTypeCombo.Size = new System.Drawing.Size(57, 21);
            this.ticketTypeCombo.TabIndex = 26;
            this.ticketTypeCombo.Text = "TAS";
            this.toolTip.SetToolTip(this.ticketTypeCombo, "Ticket type");
            this.ticketTypeCombo.SelectedIndexChanged += new System.EventHandler(this.ticketTypeCombo_SelectedIndexChanged);
            // 
            // additionalSerial
            // 
            this.additionalSerial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.additionalSerial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.additionalSerial.Location = new System.Drawing.Point(47, 369);
            this.additionalSerial.Name = "additionalSerial";
            this.additionalSerial.Size = new System.Drawing.Size(128, 20);
            this.additionalSerial.TabIndex = 24;
            this.toolTip.SetToolTip(this.additionalSerial, "Additional part\'s serial");
            this.additionalSerial.Click += new System.EventHandler(this.additionalSerial_Click);
            // 
            // additionalMPN
            // 
            this.additionalMPN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.additionalMPN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.additionalMPN.Location = new System.Drawing.Point(47, 396);
            this.additionalMPN.Name = "additionalMPN";
            this.additionalMPN.Size = new System.Drawing.Size(128, 20);
            this.additionalMPN.TabIndex = 23;
            this.toolTip.SetToolTip(this.additionalMPN, "Addition part\'s MPN");
            this.additionalMPN.Click += new System.EventHandler(this.additionalMPN_Click);
            // 
            // hotkeyLabel
            // 
            this.hotkeyLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.hotkeyLabel.AutoSize = true;
            this.hotkeyLabel.BackColor = System.Drawing.SystemColors.Window;
            this.hotkeyLabel.Location = new System.Drawing.Point(264, 208);
            this.hotkeyLabel.Name = "hotkeyLabel";
            this.hotkeyLabel.Size = new System.Drawing.Size(102, 143);
            this.hotkeyLabel.TabIndex = 30;
            this.hotkeyLabel.Text = "Hotkeys - CTRL + #\r\n1 - TAS/CASE\r\n2 - Work Order\r\n3 - Fault Node\r\n4 - Fault Seria" +
    "l\r\n5 - Fault MPN\r\n6 - Spare Serial\r\n7 - Spare MPN\r\n8 - AGS\r\n9 - PUDO\r\n0 - Connot" +
    "e";
            this.toolTip.SetToolTip(this.hotkeyLabel, "Hold mouse click to hide this pannel");
            this.hotkeyLabel.MouseLeave += new System.EventHandler(this.hotkeyLabel_MouseLeave);
            this.hotkeyLabel.MouseHover += new System.EventHandler(this.hotkeyLabel_MouseHover);
            // 
            // hotkeyCheckBox
            // 
            this.hotkeyCheckBox.AutoSize = true;
            this.hotkeyCheckBox.Location = new System.Drawing.Point(187, 5);
            this.hotkeyCheckBox.Name = "hotkeyCheckBox";
            this.hotkeyCheckBox.Size = new System.Drawing.Size(65, 17);
            this.hotkeyCheckBox.TabIndex = 33;
            this.hotkeyCheckBox.Text = "Hotkeys";
            this.toolTip.SetToolTip(this.hotkeyCheckBox, "Toggle hotkey pannel visibility");
            this.hotkeyCheckBox.UseVisualStyleBackColor = true;
            this.hotkeyCheckBox.CheckedChanged += new System.EventHandler(this.hotkeyCheckBox_CheckedChanged);
            // 
            // personDetailsDGV
            // 
            this.personDetailsDGV.AllowUserToAddRows = false;
            this.personDetailsDGV.AllowUserToDeleteRows = false;
            this.personDetailsDGV.AllowUserToResizeRows = false;
            this.personDetailsDGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.personDetailsDGV.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.personDetailsDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.personDetailsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.personDetailsDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.typeColumn,
            this.valueColumn});
            this.personDetailsDGV.Location = new System.Drawing.Point(0, 64);
            this.personDetailsDGV.MultiSelect = false;
            this.personDetailsDGV.Name = "personDetailsDGV";
            this.personDetailsDGV.ReadOnly = true;
            this.personDetailsDGV.RowHeadersVisible = false;
            this.personDetailsDGV.Size = new System.Drawing.Size(388, 352);
            this.personDetailsDGV.TabIndex = 9;
            this.toolTip.SetToolTip(this.personDetailsDGV, "CTRL + Left click a cell to copy it\'s value to your notes");
            this.personDetailsDGV.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.personDetailsDGV_CellMouseClick);
            // 
            // typeColumn
            // 
            this.typeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.typeColumn.DataPropertyName = "contactDetails";
            this.typeColumn.HeaderText = "Contact Details";
            this.typeColumn.MaxInputLength = 150;
            this.typeColumn.Name = "typeColumn";
            this.typeColumn.ReadOnly = true;
            this.typeColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.typeColumn.Width = 150;
            // 
            // valueColumn
            // 
            this.valueColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.valueColumn.DataPropertyName = "contactValue";
            this.valueColumn.HeaderText = "";
            this.valueColumn.MaxInputLength = 150;
            this.valueColumn.Name = "valueColumn";
            this.valueColumn.ReadOnly = true;
            this.valueColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // level1GroupBox
            // 
            this.level1GroupBox.Controls.Add(this.agsListView);
            this.level1GroupBox.Controls.Add(this.groupBoxMMGGeneral);
            this.level1GroupBox.Controls.Add(this.rtcLinkLabel);
            this.level1GroupBox.Controls.Add(this.newTicketLinkLabel);
            this.level1GroupBox.Controls.Add(this.rtcTextBox);
            this.level1GroupBox.Controls.Add(this.ticketTypeCombo);
            this.level1GroupBox.Controls.Add(this.tasLabel);
            this.level1GroupBox.Controls.Add(this.agsLink);
            this.level1GroupBox.Controls.Add(this.tasTextBox);
            this.level1GroupBox.Controls.Add(this.workOrderLabel);
            this.level1GroupBox.Controls.Add(this.connoteTextBox);
            this.level1GroupBox.Controls.Add(this.workOrderTextBox);
            this.level1GroupBox.Controls.Add(this.label5);
            this.level1GroupBox.Controls.Add(this.spareBox);
            this.level1GroupBox.Controls.Add(this.faultyBox);
            this.level1GroupBox.Controls.Add(this.ctTextBox);
            this.level1GroupBox.Controls.Add(this.nodeLabel);
            this.level1GroupBox.Controls.Add(this.nodeTextBox);
            this.level1GroupBox.Controls.Add(this.pudoTextBox);
            this.level1GroupBox.Controls.Add(this.label2);
            this.level1GroupBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.level1GroupBox.Location = new System.Drawing.Point(2, 25);
            this.level1GroupBox.Name = "level1GroupBox";
            this.level1GroupBox.Size = new System.Drawing.Size(388, 225);
            this.level1GroupBox.TabIndex = 34;
            this.level1GroupBox.TabStop = false;
            this.level1GroupBox.Text = "Calls";
            this.toolTip.SetToolTip(this.level1GroupBox, "Level 1 call taking layout");
            // 
            // agsListView
            // 
            this.agsListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.agsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameColumn,
            this.agsColumn});
            this.agsListView.FullRowSelect = true;
            this.agsListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.agsListView.Location = new System.Drawing.Point(67, 75);
            this.agsListView.MaximumSize = new System.Drawing.Size(150, 138);
            this.agsListView.MultiSelect = false;
            this.agsListView.Name = "agsListView";
            this.agsListView.Size = new System.Drawing.Size(117, 10);
            this.agsListView.TabIndex = 9;
            this.agsListView.UseCompatibleStateImageBehavior = false;
            this.agsListView.View = System.Windows.Forms.View.Details;
            this.agsListView.Visible = false;
            this.agsListView.VisibleChanged += new System.EventHandler(this.agsListView_VisibleChanged);
            this.agsListView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.agsListView_MouseDoubleClick);
            // 
            // nameColumn
            // 
            this.nameColumn.Text = "Name";
            this.nameColumn.Width = 57;
            // 
            // agsColumn
            // 
            this.agsColumn.Text = "AGS";
            this.agsColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.agsColumn.Width = 57;
            // 
            // groupBoxMMGGeneral
            // 
            this.groupBoxMMGGeneral.Controls.Add(this.textMMGReservation);
            this.groupBoxMMGGeneral.Controls.Add(this.labelReservationNumber);
            this.groupBoxMMGGeneral.Location = new System.Drawing.Point(116, 0);
            this.groupBoxMMGGeneral.Name = "groupBoxMMGGeneral";
            this.groupBoxMMGGeneral.Size = new System.Drawing.Size(272, 18);
            this.groupBoxMMGGeneral.TabIndex = 36;
            this.groupBoxMMGGeneral.TabStop = false;
            this.groupBoxMMGGeneral.Text = "MMG";
            this.groupBoxMMGGeneral.Visible = false;
            // 
            // textMMGReservation
            // 
            this.textMMGReservation.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textMMGReservation.Location = new System.Drawing.Point(80, 25);
            this.textMMGReservation.Name = "textMMGReservation";
            this.textMMGReservation.Size = new System.Drawing.Size(100, 20);
            this.textMMGReservation.TabIndex = 1;
            // 
            // labelReservationNumber
            // 
            this.labelReservationNumber.AutoSize = true;
            this.labelReservationNumber.Location = new System.Drawing.Point(13, 27);
            this.labelReservationNumber.Name = "labelReservationNumber";
            this.labelReservationNumber.Size = new System.Drawing.Size(67, 13);
            this.labelReservationNumber.TabIndex = 0;
            this.labelReservationNumber.Text = "Reservation:";
            // 
            // rtcLinkLabel
            // 
            this.rtcLinkLabel.AutoSize = true;
            this.rtcLinkLabel.Location = new System.Drawing.Point(12, 196);
            this.rtcLinkLabel.Name = "rtcLinkLabel";
            this.rtcLinkLabel.Size = new System.Drawing.Size(32, 13);
            this.rtcLinkLabel.TabIndex = 35;
            this.rtcLinkLabel.TabStop = true;
            this.rtcLinkLabel.Text = "RTC:";
            this.rtcLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.rtcLinkLabel_LinkClicked);
            // 
            // newTicketLinkLabel
            // 
            this.newTicketLinkLabel.AutoSize = true;
            this.newTicketLinkLabel.BackColor = System.Drawing.Color.White;
            this.newTicketLinkLabel.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.newTicketLinkLabel.Location = new System.Drawing.Point(164, 59);
            this.newTicketLinkLabel.Name = "newTicketLinkLabel";
            this.newTicketLinkLabel.Size = new System.Drawing.Size(16, 13);
            this.newTicketLinkLabel.TabIndex = 34;
            this.newTicketLinkLabel.TabStop = true;
            this.newTicketLinkLabel.Text = "->";
            this.toolTip.SetToolTip(this.newTicketLinkLabel, "Creates a new ticket and pertains the AGS");
            this.newTicketLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.newTicketLinkLabel_LinkClicked);
            // 
            // rtcTextBox
            // 
            this.rtcTextBox.Location = new System.Drawing.Point(45, 193);
            this.rtcTextBox.MaxLength = 6;
            this.rtcTextBox.Name = "rtcTextBox";
            this.rtcTextBox.Size = new System.Drawing.Size(98, 20);
            this.rtcTextBox.TabIndex = 15;
            this.toolTip.SetToolTip(this.rtcTextBox, "RTC item number");
            this.rtcTextBox.Click += new System.EventHandler(this.rtcTextBox_Click);
            this.rtcTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rtcTextBox_KeyPress);
            // 
            // agsLink
            // 
            this.agsLink.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.agsLink.AutoSize = true;
            this.agsLink.Location = new System.Drawing.Point(12, 59);
            this.agsLink.Name = "agsLink";
            this.agsLink.Size = new System.Drawing.Size(51, 13);
            this.agsLink.TabIndex = 31;
            this.agsLink.TabStop = true;
            this.agsLink.Text = "CT/AGS:";
            this.agsLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.agsLink_LinkClicked);
            this.agsLink.MouseClick += new System.Windows.Forms.MouseEventHandler(this.agsLink_MouseClick);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(180, 197);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "Connote:";
            // 
            // dateTimePickerSys
            // 
            this.dateTimePickerSys.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerSys.Location = new System.Drawing.Point(62, 185);
            this.dateTimePickerSys.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.dateTimePickerSys.Name = "dateTimePickerSys";
            this.dateTimePickerSys.Size = new System.Drawing.Size(107, 20);
            this.dateTimePickerSys.TabIndex = 55;
            this.toolTip.SetToolTip(this.dateTimePickerSys, "Date of error occurance");
            // 
            // courierCombo
            // 
            this.courierCombo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.courierCombo.Enabled = false;
            this.courierCombo.FormattingEnabled = true;
            this.courierCombo.Items.AddRange(new object[] {
            "Toll",
            "AAE"});
            this.courierCombo.Location = new System.Drawing.Point(28, -23);
            this.courierCombo.Name = "courierCombo";
            this.courierCombo.Size = new System.Drawing.Size(36, 21);
            this.courierCombo.TabIndex = 13;
            this.courierCombo.Text = "Toll";
            this.courierCombo.Visible = false;
            // 
            // courierLabel
            // 
            this.courierLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.courierLabel.AutoSize = true;
            this.courierLabel.Location = new System.Drawing.Point(210, 9);
            this.courierLabel.Name = "courierLabel";
            this.courierLabel.Size = new System.Drawing.Size(43, 13);
            this.courierLabel.TabIndex = 27;
            this.courierLabel.Text = "Courier:";
            this.courierLabel.Visible = false;
            // 
            // miscTab
            // 
            this.miscTab.AllowDrop = true;
            this.miscTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.miscTab.Controls.Add(this.tabNotes);
            this.miscTab.Controls.Add(this.tabDataSearch);
            this.miscTab.Controls.Add(this.personDetails);
            this.miscTab.Controls.Add(this.tabMisc);
            this.miscTab.Location = new System.Drawing.Point(-1, 256);
            this.miscTab.Name = "miscTab";
            this.miscTab.SelectedIndex = 0;
            this.miscTab.Size = new System.Drawing.Size(399, 445);
            this.miscTab.TabIndex = 23;
            // 
            // tabNotes
            // 
            this.tabNotes.Controls.Add(this.linkPod);
            this.tabNotes.Controls.Add(this.hotkeyLabel);
            this.tabNotes.Controls.Add(this.hotkeysSysLog);
            this.tabNotes.Controls.Add(this.judgementLabel);
            this.tabNotes.Controls.Add(this.hotkeyCheckBox);
            this.tabNotes.Controls.Add(this.rtbExtended);
            this.tabNotes.Controls.Add(this.additionalAddbutton);
            this.tabNotes.Controls.Add(this.additionalSpareradio);
            this.tabNotes.Controls.Add(this.additionalFaultradio);
            this.tabNotes.Controls.Add(this.additionalSeriallabel);
            this.tabNotes.Controls.Add(this.additionalMPNlabel);
            this.tabNotes.Controls.Add(this.additionalSerial);
            this.tabNotes.Controls.Add(this.additionalMPN);
            this.tabNotes.Location = new System.Drawing.Point(4, 22);
            this.tabNotes.Name = "tabNotes";
            this.tabNotes.Padding = new System.Windows.Forms.Padding(3);
            this.tabNotes.Size = new System.Drawing.Size(391, 419);
            this.tabNotes.TabIndex = 0;
            this.tabNotes.Text = "Notes";
            this.tabNotes.ToolTipText = "Additional notes here.";
            this.tabNotes.UseVisualStyleBackColor = true;
            // 
            // linkPod
            // 
            this.linkPod.AutoSize = true;
            this.linkPod.Location = new System.Drawing.Point(253, 7);
            this.linkPod.Name = "linkPod";
            this.linkPod.Size = new System.Drawing.Size(30, 13);
            this.linkPod.TabIndex = 36;
            this.linkPod.TabStop = true;
            this.linkPod.Text = "POD";
            this.linkPod.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkPod_LinkClicked);
            // 
            // hotkeysSysLog
            // 
            this.hotkeysSysLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.hotkeysSysLog.AutoSize = true;
            this.hotkeysSysLog.BackColor = System.Drawing.SystemColors.Window;
            this.hotkeysSysLog.Location = new System.Drawing.Point(262, 208);
            this.hotkeysSysLog.Name = "hotkeysSysLog";
            this.hotkeysSysLog.Size = new System.Drawing.Size(102, 143);
            this.hotkeysSysLog.TabIndex = 34;
            this.hotkeysSysLog.Text = "Hotkeys - CTRL + #\r\n1 - Ticket\r\n2 - Work order\r\n3 - Error\r\n4 - User AGS\r\n5 - User" +
    " Name\r\n6 - CT AGS\r\n7 - CT Name\r\n8 - RTC\r\n9 - Date\r\n0 - Task Category";
            this.hotkeysSysLog.Visible = false;
            // 
            // judgementLabel
            // 
            this.judgementLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.judgementLabel.AutoSize = true;
            this.judgementLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.judgementLabel.Location = new System.Drawing.Point(265, 0);
            this.judgementLabel.Name = "judgementLabel";
            this.judgementLabel.Size = new System.Drawing.Size(99, 25);
            this.judgementLabel.TabIndex = 32;
            this.judgementLabel.Text = "ლ(ಠ益ಠლ)";
            this.judgementLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.judgementLabel.Visible = false;
            // 
            // rtbExtended
            // 
            this.rtbExtended.AcceptsTab = true;
            this.rtbExtended.AllowDrop = true;
            this.rtbExtended.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbExtended.AutoWordSelection = true;
            this.rtbExtended.BackColor = System.Drawing.Color.Transparent;
            this.rtbExtended.DetectURLs = true;
            this.rtbExtended.Location = new System.Drawing.Point(0, 0);
            this.rtbExtended.Name = "rtbExtended";
            this.rtbExtended.ReadOnly = false;
            // 
            // 
            // 
            this.rtbExtended.RichTextBox.AcceptsTab = true;
            this.rtbExtended.RichTextBox.AutoWordSelection = true;
            this.rtbExtended.RichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbExtended.RichTextBox.Location = new System.Drawing.Point(0, 26);
            this.rtbExtended.RichTextBox.Name = "rtb1";
            this.rtbExtended.RichTextBox.Size = new System.Drawing.Size(389, 337);
            this.rtbExtended.RichTextBox.TabIndex = 1;
            this.rtbExtended.RichTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.richTextBoxExtended1_RichTextBox_KeyDown);
            this.rtbExtended.RichTextBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rtbExtended_RichTextBox_MouseDown);
            this.rtbExtended.ShowBold = true;
            this.rtbExtended.ShowCenterJustify = false;
            this.rtbExtended.ShowColors = true;
            this.rtbExtended.ShowCopy = false;
            this.rtbExtended.ShowCut = false;
            this.rtbExtended.ShowFont = false;
            this.rtbExtended.ShowFontSize = true;
            this.rtbExtended.ShowItalic = true;
            this.rtbExtended.ShowLeftJustify = false;
            this.rtbExtended.ShowOpen = false;
            this.rtbExtended.ShowPaste = false;
            this.rtbExtended.ShowRedo = false;
            this.rtbExtended.ShowRightJustify = false;
            this.rtbExtended.ShowSave = false;
            this.rtbExtended.ShowStamp = false;
            this.rtbExtended.ShowStrikeout = true;
            this.rtbExtended.ShowToolBarText = false;
            this.rtbExtended.ShowUnderline = true;
            this.rtbExtended.ShowUndo = false;
            this.rtbExtended.Size = new System.Drawing.Size(389, 364);
            this.rtbExtended.StampAction = RichTextBoxExtended.StampActions.EditedBy;
            this.rtbExtended.StampColor = System.Drawing.Color.Blue;
            this.rtbExtended.TabIndex = 32;
            // 
            // 
            // 
            this.rtbExtended.Toolbar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
            this.rtbExtended.Toolbar.ButtonSize = new System.Drawing.Size(16, 16);
            this.rtbExtended.Toolbar.Divider = false;
            this.rtbExtended.Toolbar.DropDownArrows = true;
            this.rtbExtended.Toolbar.Location = new System.Drawing.Point(0, 0);
            this.rtbExtended.Toolbar.Name = "tb1";
            this.rtbExtended.Toolbar.ShowToolTips = true;
            this.rtbExtended.Toolbar.Size = new System.Drawing.Size(389, 26);
            this.rtbExtended.Toolbar.TabIndex = 0;
            // 
            // additionalAddbutton
            // 
            this.additionalAddbutton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.additionalAddbutton.Location = new System.Drawing.Point(245, 381);
            this.additionalAddbutton.Name = "additionalAddbutton";
            this.additionalAddbutton.Size = new System.Drawing.Size(75, 23);
            this.additionalAddbutton.TabIndex = 29;
            this.additionalAddbutton.Text = "Add";
            this.additionalAddbutton.UseVisualStyleBackColor = true;
            this.additionalAddbutton.Click += new System.EventHandler(this.additionalAddbutton_Click);
            // 
            // additionalSpareradio
            // 
            this.additionalSpareradio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.additionalSpareradio.AutoSize = true;
            this.additionalSpareradio.Location = new System.Drawing.Point(188, 397);
            this.additionalSpareradio.Name = "additionalSpareradio";
            this.additionalSpareradio.Size = new System.Drawing.Size(53, 17);
            this.additionalSpareradio.TabIndex = 28;
            this.additionalSpareradio.Text = "Spare";
            this.additionalSpareradio.UseVisualStyleBackColor = true;
            // 
            // additionalFaultradio
            // 
            this.additionalFaultradio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.additionalFaultradio.AutoSize = true;
            this.additionalFaultradio.Checked = true;
            this.additionalFaultradio.Location = new System.Drawing.Point(188, 371);
            this.additionalFaultradio.Name = "additionalFaultradio";
            this.additionalFaultradio.Size = new System.Drawing.Size(53, 17);
            this.additionalFaultradio.TabIndex = 27;
            this.additionalFaultradio.TabStop = true;
            this.additionalFaultradio.Text = "Faulty";
            this.additionalFaultradio.UseVisualStyleBackColor = true;
            // 
            // additionalSeriallabel
            // 
            this.additionalSeriallabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.additionalSeriallabel.AutoSize = true;
            this.additionalSeriallabel.Location = new System.Drawing.Point(5, 372);
            this.additionalSeriallabel.Name = "additionalSeriallabel";
            this.additionalSeriallabel.Size = new System.Drawing.Size(39, 13);
            this.additionalSeriallabel.TabIndex = 26;
            this.additionalSeriallabel.Text = "Serial: ";
            // 
            // additionalMPNlabel
            // 
            this.additionalMPNlabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.additionalMPNlabel.AutoSize = true;
            this.additionalMPNlabel.Location = new System.Drawing.Point(6, 399);
            this.additionalMPNlabel.Name = "additionalMPNlabel";
            this.additionalMPNlabel.Size = new System.Drawing.Size(37, 13);
            this.additionalMPNlabel.TabIndex = 25;
            this.additionalMPNlabel.Text = "MPN: ";
            // 
            // tabDataSearch
            // 
            this.tabDataSearch.Controls.Add(this.tabControlData);
            this.tabDataSearch.Controls.Add(this.labelClearSearch);
            this.tabDataSearch.Controls.Add(this.comboDataSelect);
            this.tabDataSearch.Controls.Add(this.textBoxSearch);
            this.tabDataSearch.Controls.Add(this.label6);
            this.tabDataSearch.Location = new System.Drawing.Point(4, 22);
            this.tabDataSearch.Name = "tabDataSearch";
            this.tabDataSearch.Size = new System.Drawing.Size(391, 416);
            this.tabDataSearch.TabIndex = 2;
            this.tabDataSearch.Text = "Locations & Parts";
            this.tabDataSearch.UseVisualStyleBackColor = true;
            // 
            // tabControlData
            // 
            this.tabControlData.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControlData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlData.Controls.Add(this.tabLocations);
            this.tabControlData.Controls.Add(this.tabKits);
            this.tabControlData.Controls.Add(this.tabAddresses);
            this.tabControlData.Controls.Add(this.tabConsumables);
            this.tabControlData.ItemSize = new System.Drawing.Size(1, 1);
            this.tabControlData.Location = new System.Drawing.Point(-4, 27);
            this.tabControlData.Margin = new System.Windows.Forms.Padding(0);
            this.tabControlData.Name = "tabControlData";
            this.tabControlData.Padding = new System.Drawing.Point(0, 0);
            this.tabControlData.SelectedIndex = 0;
            this.tabControlData.Size = new System.Drawing.Size(396, 396);
            this.tabControlData.TabIndex = 6;
            // 
            // tabLocations
            // 
            this.tabLocations.Controls.Add(this.dgvLocations);
            this.tabLocations.Location = new System.Drawing.Point(4, 4);
            this.tabLocations.Margin = new System.Windows.Forms.Padding(0);
            this.tabLocations.Name = "tabLocations";
            this.tabLocations.Size = new System.Drawing.Size(388, 387);
            this.tabLocations.TabIndex = 0;
            this.tabLocations.UseVisualStyleBackColor = true;
            // 
            // dgvLocations
            // 
            this.dgvLocations.AllowUserToAddRows = false;
            this.dgvLocations.AllowUserToDeleteRows = false;
            this.dgvLocations.AllowUserToOrderColumns = true;
            this.dgvLocations.AllowUserToResizeRows = false;
            this.dgvLocations.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvLocations.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvLocations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocations.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.plntColumn,
            this.slocColumn,
            this.storageLocationColumn,
            this.nodeSearchColumn,
            this.E,
            this.D,
            this.F,
            this.latitudeColumn,
            this.longitudeColumn,
            this.locationTypeColumn,
            this.searchTerm2});
            this.dgvLocations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLocations.EnableHeadersVisualStyles = false;
            this.dgvLocations.Location = new System.Drawing.Point(0, 0);
            this.dgvLocations.Name = "dgvLocations";
            this.dgvLocations.ReadOnly = true;
            this.dgvLocations.RowHeadersVisible = false;
            this.dgvLocations.Size = new System.Drawing.Size(388, 387);
            this.dgvLocations.TabIndex = 2;
            // 
            // plntColumn
            // 
            this.plntColumn.DataPropertyName = "Plnt";
            this.plntColumn.HeaderText = "Plant";
            this.plntColumn.MaxInputLength = 5;
            this.plntColumn.Name = "plntColumn";
            this.plntColumn.ReadOnly = true;
            this.plntColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.plntColumn.Width = 50;
            // 
            // slocColumn
            // 
            this.slocColumn.DataPropertyName = "SLoc";
            this.slocColumn.HeaderText = "SLOC";
            this.slocColumn.MaxInputLength = 5;
            this.slocColumn.Name = "slocColumn";
            this.slocColumn.ReadOnly = true;
            this.slocColumn.Width = 50;
            // 
            // storageLocationColumn
            // 
            this.storageLocationColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.storageLocationColumn.DataPropertyName = "Stor. Loc. Descr.";
            this.storageLocationColumn.HeaderText = "Description";
            this.storageLocationColumn.MaxInputLength = 50;
            this.storageLocationColumn.Name = "storageLocationColumn";
            this.storageLocationColumn.ReadOnly = true;
            // 
            // nodeSearchColumn
            // 
            this.nodeSearchColumn.DataPropertyName = "Search Term 1";
            this.nodeSearchColumn.HeaderText = "Node";
            this.nodeSearchColumn.MaxInputLength = 20;
            this.nodeSearchColumn.Name = "nodeSearchColumn";
            this.nodeSearchColumn.ReadOnly = true;
            this.nodeSearchColumn.Width = 75;
            // 
            // E
            // 
            this.E.DataPropertyName = "Warehouse Accessibility Status";
            this.E.HeaderText = "";
            this.E.MaxInputLength = 10;
            this.E.Name = "E";
            this.E.ReadOnly = true;
            this.E.ToolTipText = "01 - Restricted \\n 02 - Unrestricted";
            this.E.Width = 25;
            // 
            // D
            // 
            this.D.DataPropertyName = "Warehouse Status";
            this.D.HeaderText = "Warehouse Status";
            this.D.Name = "D";
            this.D.ReadOnly = true;
            this.D.Visible = false;
            // 
            // F
            // 
            this.F.DataPropertyName = "Warehouse Type";
            this.F.HeaderText = "Warehouse Type";
            this.F.Name = "F";
            this.F.ReadOnly = true;
            this.F.Visible = false;
            // 
            // latitudeColumn
            // 
            this.latitudeColumn.DataPropertyName = "Latitude";
            this.latitudeColumn.HeaderText = "Latitude";
            this.latitudeColumn.Name = "latitudeColumn";
            this.latitudeColumn.ReadOnly = true;
            this.latitudeColumn.Visible = false;
            // 
            // longitudeColumn
            // 
            this.longitudeColumn.DataPropertyName = "Longitude";
            this.longitudeColumn.HeaderText = "Longitude";
            this.longitudeColumn.Name = "longitudeColumn";
            this.longitudeColumn.ReadOnly = true;
            this.longitudeColumn.Visible = false;
            // 
            // locationTypeColumn
            // 
            this.locationTypeColumn.DataPropertyName = "Location Type";
            this.locationTypeColumn.HeaderText = "locType";
            this.locationTypeColumn.Name = "locationTypeColumn";
            this.locationTypeColumn.ReadOnly = true;
            this.locationTypeColumn.Visible = false;
            // 
            // searchTerm2
            // 
            this.searchTerm2.DataPropertyName = "Search Term 2";
            this.searchTerm2.HeaderText = "searchTerm2";
            this.searchTerm2.Name = "searchTerm2";
            this.searchTerm2.ReadOnly = true;
            this.searchTerm2.Visible = false;
            // 
            // tabKits
            // 
            this.tabKits.Controls.Add(this.kitGrid);
            this.tabKits.Location = new System.Drawing.Point(4, 4);
            this.tabKits.Name = "tabKits";
            this.tabKits.Size = new System.Drawing.Size(388, 387);
            this.tabKits.TabIndex = 1;
            this.tabKits.UseVisualStyleBackColor = true;
            // 
            // kitGrid
            // 
            this.kitGrid.AllowUserToAddRows = false;
            this.kitGrid.AllowUserToDeleteRows = false;
            this.kitGrid.AllowUserToResizeRows = false;
            this.kitGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this.kitGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.kitGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.kitGrid.ColumnHeadersVisible = false;
            this.kitGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.kitColumn,
            this.CC,
            this.nodeColumn});
            this.kitGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kitGrid.EnableHeadersVisualStyles = false;
            this.kitGrid.Location = new System.Drawing.Point(0, 0);
            this.kitGrid.Name = "kitGrid";
            this.kitGrid.ReadOnly = true;
            this.kitGrid.RowHeadersVisible = false;
            this.kitGrid.Size = new System.Drawing.Size(388, 387);
            this.kitGrid.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "SLOC";
            this.dataGridViewTextBoxColumn1.HeaderText = "A";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "WHN";
            this.dataGridViewTextBoxColumn2.HeaderText = "B";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 5;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 50;
            // 
            // kitColumn
            // 
            this.kitColumn.DataPropertyName = "KIT";
            this.kitColumn.HeaderText = "KIT";
            this.kitColumn.MaxInputLength = 80;
            this.kitColumn.Name = "kitColumn";
            this.kitColumn.ReadOnly = true;
            this.kitColumn.Width = 35;
            // 
            // CC
            // 
            this.CC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CC.DataPropertyName = "DESC";
            this.CC.HeaderText = "desc";
            this.CC.MaxInputLength = 40;
            this.CC.MinimumWidth = 178;
            this.CC.Name = "CC";
            this.CC.ReadOnly = true;
            // 
            // nodeColumn
            // 
            this.nodeColumn.DataPropertyName = "NODE";
            this.nodeColumn.HeaderText = "NODE";
            this.nodeColumn.MaxInputLength = 80;
            this.nodeColumn.MinimumWidth = 53;
            this.nodeColumn.Name = "nodeColumn";
            this.nodeColumn.ReadOnly = true;
            this.nodeColumn.Width = 53;
            // 
            // tabAddresses
            // 
            this.tabAddresses.Controls.Add(this.addressDGV);
            this.tabAddresses.Location = new System.Drawing.Point(4, 4);
            this.tabAddresses.Margin = new System.Windows.Forms.Padding(0);
            this.tabAddresses.Name = "tabAddresses";
            this.tabAddresses.Size = new System.Drawing.Size(388, 387);
            this.tabAddresses.TabIndex = 2;
            this.tabAddresses.UseVisualStyleBackColor = true;
            // 
            // addressDGV
            // 
            this.addressDGV.AllowUserToAddRows = false;
            this.addressDGV.AllowUserToDeleteRows = false;
            this.addressDGV.BackgroundColor = System.Drawing.SystemColors.Control;
            this.addressDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.addressDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.addressDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PlntCol,
            this.SLoc,
            this.addrNoHidden,
            this.NameCol,
            this.HouseNo,
            this.StreetCol,
            this.CityCol,
            this.PostlCodeCol,
            this.TranspZoneHidden,
            this.Rg,
            this.PuDoCode,
            this.name4col,
            this.cOName});
            this.addressDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addressDGV.EnableHeadersVisualStyles = false;
            this.addressDGV.Location = new System.Drawing.Point(0, 0);
            this.addressDGV.Name = "addressDGV";
            this.addressDGV.ReadOnly = true;
            this.addressDGV.RowHeadersVisible = false;
            this.addressDGV.Size = new System.Drawing.Size(388, 387);
            this.addressDGV.TabIndex = 8;
            this.addressDGV.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.addressDGV_CellMouseClick);
            // 
            // PlntCol
            // 
            this.PlntCol.DataPropertyName = "Plnt";
            this.PlntCol.HeaderText = "Plnt";
            this.PlntCol.MaxInputLength = 15;
            this.PlntCol.Name = "PlntCol";
            this.PlntCol.ReadOnly = true;
            this.PlntCol.ToolTipText = "Plant";
            this.PlntCol.Width = 40;
            // 
            // SLoc
            // 
            this.SLoc.DataPropertyName = "SLoc";
            this.SLoc.HeaderText = "SLoc";
            this.SLoc.MaxInputLength = 15;
            this.SLoc.Name = "SLoc";
            this.SLoc.ReadOnly = true;
            this.SLoc.ToolTipText = "Storage Location";
            this.SLoc.Width = 40;
            // 
            // addrNoHidden
            // 
            this.addrNoHidden.DataPropertyName = "Addr. no.";
            this.addrNoHidden.HeaderText = "Addr. no.";
            this.addrNoHidden.MaxInputLength = 20;
            this.addrNoHidden.Name = "addrNoHidden";
            this.addrNoHidden.ReadOnly = true;
            this.addrNoHidden.ToolTipText = "Address number";
            this.addrNoHidden.Visible = false;
            this.addrNoHidden.Width = 40;
            // 
            // NameCol
            // 
            this.NameCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NameCol.DataPropertyName = "Name";
            this.NameCol.HeaderText = "Name";
            this.NameCol.MaxInputLength = 50;
            this.NameCol.MinimumWidth = 115;
            this.NameCol.Name = "NameCol";
            this.NameCol.ReadOnly = true;
            this.NameCol.ToolTipText = "Name";
            // 
            // HouseNo
            // 
            this.HouseNo.DataPropertyName = "House No.";
            this.HouseNo.HeaderText = "No.";
            this.HouseNo.MaxInputLength = 70;
            this.HouseNo.Name = "HouseNo";
            this.HouseNo.ReadOnly = true;
            this.HouseNo.ToolTipText = "Street number";
            this.HouseNo.Width = 50;
            // 
            // StreetCol
            // 
            this.StreetCol.DataPropertyName = "Street";
            this.StreetCol.HeaderText = "Street";
            this.StreetCol.MaxInputLength = 50;
            this.StreetCol.Name = "StreetCol";
            this.StreetCol.ReadOnly = true;
            this.StreetCol.ToolTipText = "Street name";
            // 
            // CityCol
            // 
            this.CityCol.DataPropertyName = "City";
            this.CityCol.HeaderText = "City";
            this.CityCol.MaxInputLength = 50;
            this.CityCol.Name = "CityCol";
            this.CityCol.ReadOnly = true;
            this.CityCol.ToolTipText = "City";
            this.CityCol.Width = 80;
            // 
            // PostlCodeCol
            // 
            this.PostlCodeCol.DataPropertyName = "Postl Code";
            this.PostlCodeCol.HeaderText = "Post Code";
            this.PostlCodeCol.MaxInputLength = 50;
            this.PostlCodeCol.Name = "PostlCodeCol";
            this.PostlCodeCol.ReadOnly = true;
            this.PostlCodeCol.ToolTipText = "Postal Code";
            this.PostlCodeCol.Width = 85;
            // 
            // TranspZoneHidden
            // 
            this.TranspZoneHidden.DataPropertyName = "TranspZone";
            this.TranspZoneHidden.HeaderText = "TranspZone";
            this.TranspZoneHidden.MaxInputLength = 80;
            this.TranspZoneHidden.Name = "TranspZoneHidden";
            this.TranspZoneHidden.ReadOnly = true;
            this.TranspZoneHidden.ToolTipText = "Transport Zone";
            this.TranspZoneHidden.Visible = false;
            // 
            // Rg
            // 
            this.Rg.DataPropertyName = "Rg";
            this.Rg.HeaderText = "Rg";
            this.Rg.MaxInputLength = 80;
            this.Rg.Name = "Rg";
            this.Rg.ReadOnly = true;
            this.Rg.ToolTipText = "Region";
            this.Rg.Width = 50;
            // 
            // PuDoCode
            // 
            this.PuDoCode.DataPropertyName = "Search Term 1";
            this.PuDoCode.HeaderText = "PuDo";
            this.PuDoCode.MaxInputLength = 50;
            this.PuDoCode.Name = "PuDoCode";
            this.PuDoCode.ReadOnly = true;
            this.PuDoCode.ToolTipText = "PuDo Code";
            this.PuDoCode.Width = 50;
            // 
            // name4col
            // 
            this.name4col.DataPropertyName = "Name 4";
            this.name4col.HeaderText = "name4";
            this.name4col.Name = "name4col";
            this.name4col.ReadOnly = true;
            this.name4col.Visible = false;
            // 
            // cOName
            // 
            this.cOName.DataPropertyName = "c/o name";
            this.cOName.HeaderText = "coName";
            this.cOName.Name = "cOName";
            this.cOName.ReadOnly = true;
            this.cOName.Visible = false;
            // 
            // tabConsumables
            // 
            this.tabConsumables.Controls.Add(this.consumableDGV);
            this.tabConsumables.Location = new System.Drawing.Point(4, 4);
            this.tabConsumables.Name = "tabConsumables";
            this.tabConsumables.Size = new System.Drawing.Size(388, 387);
            this.tabConsumables.TabIndex = 3;
            this.tabConsumables.UseVisualStyleBackColor = true;
            // 
            // consumableDGV
            // 
            this.consumableDGV.AllowUserToAddRows = false;
            this.consumableDGV.AllowUserToDeleteRows = false;
            this.consumableDGV.AllowUserToResizeRows = false;
            this.consumableDGV.BackgroundColor = System.Drawing.SystemColors.Control;
            this.consumableDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.consumableDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.consumableDGV.ColumnHeadersVisible = false;
            this.consumableDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.consumableDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.consumableDGV.EnableHeadersVisualStyles = false;
            this.consumableDGV.Location = new System.Drawing.Point(0, 0);
            this.consumableDGV.Name = "consumableDGV";
            this.consumableDGV.ReadOnly = true;
            this.consumableDGV.RowHeadersVisible = false;
            this.consumableDGV.Size = new System.Drawing.Size(388, 387);
            this.consumableDGV.TabIndex = 7;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Material";
            this.dataGridViewTextBoxColumn3.HeaderText = "A";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 80;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Description";
            this.dataGridViewTextBoxColumn4.HeaderText = "B";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Serialised";
            this.dataGridViewTextBoxColumn5.HeaderText = "C";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 50;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.ToolTipText = "00, 01 - Serialised, 02 - Not Serialised";
            this.dataGridViewTextBoxColumn5.Width = 40;
            // 
            // labelClearSearch
            // 
            this.labelClearSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelClearSearch.AutoSize = true;
            this.labelClearSearch.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.labelClearSearch.Location = new System.Drawing.Point(371, 8);
            this.labelClearSearch.Name = "labelClearSearch";
            this.labelClearSearch.Size = new System.Drawing.Size(12, 13);
            this.labelClearSearch.TabIndex = 5;
            this.labelClearSearch.Text = "x";
            this.labelClearSearch.Visible = false;
            this.labelClearSearch.Click += new System.EventHandler(this.labelClearSearch_Click);
            this.labelClearSearch.MouseEnter += new System.EventHandler(this.labelClearSearch_MouseEnter);
            this.labelClearSearch.MouseLeave += new System.EventHandler(this.labelClearSearch_MouseLeave);
            // 
            // comboDataSelect
            // 
            this.comboDataSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDataSelect.FormattingEnabled = true;
            this.comboDataSelect.Items.AddRange(new object[] {
            "Locations",
            "Kit Stores",
            "Addresses",
            "Consumables"});
            this.comboDataSelect.Location = new System.Drawing.Point(9, 4);
            this.comboDataSelect.Name = "comboDataSelect";
            this.comboDataSelect.Size = new System.Drawing.Size(90, 21);
            this.comboDataSelect.TabIndex = 4;
            this.comboDataSelect.SelectedIndexChanged += new System.EventHandler(this.comboDataSelect_SelectedIndexChanged);
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSearch.Location = new System.Drawing.Point(234, 5);
            this.textBoxSearch.MaxLength = 50;
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(151, 20);
            this.textBoxSearch.TabIndex = 1;
            this.textBoxSearch.TextChanged += new System.EventHandler(this.searchBox_TextChanged);
            this.textBoxSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchBox_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(184, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Search:";
            // 
            // personDetails
            // 
            this.personDetails.Controls.Add(this.linkShowResults);
            this.personDetails.Controls.Add(this.postCodeTextBox);
            this.personDetails.Controls.Add(this.locationDropDown);
            this.personDetails.Controls.Add(this.backButton);
            this.personDetails.Controls.Add(this.searchPeopleButton);
            this.personDetails.Controls.Add(this.surNameTextBox);
            this.personDetails.Controls.Add(this.firstNameTextBox);
            this.personDetails.Controls.Add(this.surNameLabel);
            this.personDetails.Controls.Add(this.firstNameLabel);
            this.personDetails.Controls.Add(this.personDetailsDGV);
            this.personDetails.Controls.Add(this.personSearchDGV);
            this.personDetails.Location = new System.Drawing.Point(4, 22);
            this.personDetails.Name = "personDetails";
            this.personDetails.Size = new System.Drawing.Size(391, 416);
            this.personDetails.TabIndex = 6;
            this.personDetails.Text = "Person";
            this.personDetails.UseVisualStyleBackColor = true;
            // 
            // linkShowResults
            // 
            this.linkShowResults.AutoSize = true;
            this.linkShowResults.Location = new System.Drawing.Point(139, 401);
            this.linkShowResults.Name = "linkShowResults";
            this.linkShowResults.Size = new System.Drawing.Size(102, 13);
            this.linkShowResults.TabIndex = 10;
            this.linkShowResults.TabStop = true;
            this.linkShowResults.Text = "Show more results...";
            this.linkShowResults.Visible = false;
            // 
            // postCodeTextBox
            // 
            this.postCodeTextBox.ForeColor = System.Drawing.Color.Gray;
            this.postCodeTextBox.Location = new System.Drawing.Point(219, 38);
            this.postCodeTextBox.MaxLength = 5;
            this.postCodeTextBox.Name = "postCodeTextBox";
            this.postCodeTextBox.Size = new System.Drawing.Size(77, 20);
            this.postCodeTextBox.TabIndex = 6;
            this.postCodeTextBox.Text = "Postcode";
            this.postCodeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.postCodeTextBox.Enter += new System.EventHandler(this.postCodeTextBox_Enter);
            this.postCodeTextBox.Leave += new System.EventHandler(this.postCodeTextBox_Leave);
            // 
            // locationDropDown
            // 
            this.locationDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.locationDropDown.FormattingEnabled = true;
            this.locationDropDown.Items.AddRange(new object[] {
            "",
            "ACT",
            "NT",
            "NSW",
            "QLD",
            "SA",
            "TAS",
            "VIC",
            "WA"});
            this.locationDropDown.Location = new System.Drawing.Point(219, 8);
            this.locationDropDown.Name = "locationDropDown";
            this.locationDropDown.Size = new System.Drawing.Size(77, 21);
            this.locationDropDown.TabIndex = 5;
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(302, 35);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 23);
            this.backButton.TabIndex = 8;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // searchPeopleButton
            // 
            this.searchPeopleButton.Location = new System.Drawing.Point(302, 6);
            this.searchPeopleButton.Name = "searchPeopleButton";
            this.searchPeopleButton.Size = new System.Drawing.Size(75, 23);
            this.searchPeopleButton.TabIndex = 7;
            this.searchPeopleButton.Text = "Search";
            this.searchPeopleButton.UseVisualStyleBackColor = true;
            this.searchPeopleButton.Click += new System.EventHandler(this.searchPeopleButton_Click);
            // 
            // surNameTextBox
            // 
            this.surNameTextBox.Location = new System.Drawing.Point(71, 38);
            this.surNameTextBox.Name = "surNameTextBox";
            this.surNameTextBox.Size = new System.Drawing.Size(142, 20);
            this.surNameTextBox.TabIndex = 4;
            this.surNameTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.surNameTextBox_KeyPress);
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Location = new System.Drawing.Point(71, 8);
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(142, 20);
            this.firstNameTextBox.TabIndex = 3;
            this.firstNameTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.firstNameTextBox_KeyPress);
            // 
            // surNameLabel
            // 
            this.surNameLabel.AutoSize = true;
            this.surNameLabel.Location = new System.Drawing.Point(13, 40);
            this.surNameLabel.Name = "surNameLabel";
            this.surNameLabel.Size = new System.Drawing.Size(52, 13);
            this.surNameLabel.TabIndex = 2;
            this.surNameLabel.Text = "Surname:";
            // 
            // firstNameLabel
            // 
            this.firstNameLabel.AutoSize = true;
            this.firstNameLabel.Location = new System.Drawing.Point(5, 11);
            this.firstNameLabel.Name = "firstNameLabel";
            this.firstNameLabel.Size = new System.Drawing.Size(60, 13);
            this.firstNameLabel.TabIndex = 1;
            this.firstNameLabel.Text = "First Name:";
            // 
            // personSearchDGV
            // 
            this.personSearchDGV.AllowUserToAddRows = false;
            this.personSearchDGV.AllowUserToDeleteRows = false;
            this.personSearchDGV.AllowUserToResizeRows = false;
            this.personSearchDGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.personSearchDGV.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.personSearchDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.personSearchDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.personSearchDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameSearch,
            this.locationSearch,
            this.phoneSearch,
            this.employeeSearch});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.personSearchDGV.DefaultCellStyle = dataGridViewCellStyle4;
            this.personSearchDGV.Location = new System.Drawing.Point(-1, 64);
            this.personSearchDGV.Name = "personSearchDGV";
            this.personSearchDGV.ReadOnly = true;
            this.personSearchDGV.RowHeadersVisible = false;
            this.personSearchDGV.Size = new System.Drawing.Size(389, 352);
            this.personSearchDGV.TabIndex = 6;
            this.personSearchDGV.Visible = false;
            this.personSearchDGV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.personSearchDGV_CellClick);
            // 
            // nameSearch
            // 
            this.nameSearch.HeaderText = "Name";
            this.nameSearch.MaxInputLength = 80;
            this.nameSearch.Name = "nameSearch";
            this.nameSearch.ReadOnly = true;
            this.nameSearch.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.nameSearch.Width = 94;
            // 
            // locationSearch
            // 
            this.locationSearch.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.locationSearch.HeaderText = "Location";
            this.locationSearch.MaxInputLength = 150;
            this.locationSearch.MinimumWidth = 135;
            this.locationSearch.Name = "locationSearch";
            this.locationSearch.ReadOnly = true;
            // 
            // phoneSearch
            // 
            this.phoneSearch.HeaderText = "Phone";
            this.phoneSearch.MaxInputLength = 80;
            this.phoneSearch.Name = "phoneSearch";
            this.phoneSearch.ReadOnly = true;
            this.phoneSearch.Width = 70;
            // 
            // employeeSearch
            // 
            this.employeeSearch.HeaderText = "Employee";
            this.employeeSearch.MaxInputLength = 80;
            this.employeeSearch.Name = "employeeSearch";
            this.employeeSearch.ReadOnly = true;
            this.employeeSearch.Width = 70;
            // 
            // tabMisc
            // 
            this.tabMisc.Controls.Add(this.tabControlMisc);
            this.tabMisc.Location = new System.Drawing.Point(4, 22);
            this.tabMisc.Name = "tabMisc";
            this.tabMisc.Padding = new System.Windows.Forms.Padding(3);
            this.tabMisc.Size = new System.Drawing.Size(391, 416);
            this.tabMisc.TabIndex = 9;
            this.tabMisc.Text = "Misc";
            this.tabMisc.UseVisualStyleBackColor = true;
            // 
            // tabControlMisc
            // 
            this.tabControlMisc.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControlMisc.Controls.Add(this.tabAdder);
            this.tabControlMisc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlMisc.ItemSize = new System.Drawing.Size(20, 18);
            this.tabControlMisc.Location = new System.Drawing.Point(3, 3);
            this.tabControlMisc.Multiline = true;
            this.tabControlMisc.Name = "tabControlMisc";
            this.tabControlMisc.SelectedIndex = 0;
            this.tabControlMisc.Size = new System.Drawing.Size(385, 410);
            this.tabControlMisc.TabIndex = 1;
            this.tabControlMisc.SelectedIndexChanged += new System.EventHandler(this.tabControlMisc_SelectedIndexChanged);
            this.tabControlMisc.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tabControlMisc_MouseUp);
            // 
            // tabAdder
            // 
            this.tabAdder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.tabAdder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabAdder.Location = new System.Drawing.Point(4, 4);
            this.tabAdder.Name = "tabAdder";
            this.tabAdder.Padding = new System.Windows.Forms.Padding(3);
            this.tabAdder.Size = new System.Drawing.Size(377, 384);
            this.tabAdder.TabIndex = 1;
            this.tabAdder.Text = "    +";
            this.tabAdder.ToolTipText = "Click here to add a new tab";
            this.tabAdder.UseVisualStyleBackColor = true;
            // 
            // statusListBox
            // 
            this.statusListBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.statusListBox.BackColor = System.Drawing.SystemColors.Control;
            this.statusListBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.statusListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusListBox.FormattingEnabled = true;
            this.statusListBox.Location = new System.Drawing.Point(0, 608);
            this.statusListBox.Name = "statusListBox";
            this.statusListBox.Size = new System.Drawing.Size(392, 0);
            this.statusListBox.TabIndex = 31;
            this.statusListBox.TabStop = false;
            this.statusListBox.Visible = false;
            this.statusListBox.Leave += new System.EventHandler(this.statusListBox_Leave);
            this.statusListBox.MouseLeave += new System.EventHandler(this.statusListBox_MouseLeave);
            this.statusListBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.statusListBox_MouseMove);
            // 
            // sysLogsGroupBox
            // 
            this.sysLogsGroupBox.Controls.Add(this.peopleGroupBox);
            this.sysLogsGroupBox.Controls.Add(this.ticketTextBoxSys);
            this.sysLogsGroupBox.Controls.Add(this.ticketLabelSys);
            this.sysLogsGroupBox.Controls.Add(this.rtcTextBoxSys);
            this.sysLogsGroupBox.Controls.Add(this.rtcLabelSys);
            this.sysLogsGroupBox.Controls.Add(this.taskComboBoxSys);
            this.sysLogsGroupBox.Controls.Add(this.taskCatLabelSys);
            this.sysLogsGroupBox.Controls.Add(this.dateTimePickerSys);
            this.sysLogsGroupBox.Controls.Add(this.label7);
            this.sysLogsGroupBox.Controls.Add(this.workOrderTextBoxSys);
            this.sysLogsGroupBox.Controls.Add(this.workOrderLabelSys);
            this.sysLogsGroupBox.Controls.Add(this.errorMessageTextBox);
            this.sysLogsGroupBox.Controls.Add(this.errorLabel);
            this.sysLogsGroupBox.Location = new System.Drawing.Point(2, 25);
            this.sysLogsGroupBox.Name = "sysLogsGroupBox";
            this.sysLogsGroupBox.Size = new System.Drawing.Size(389, 225);
            this.sysLogsGroupBox.TabIndex = 34;
            this.sysLogsGroupBox.TabStop = false;
            this.sysLogsGroupBox.Text = "System Logs";
            this.sysLogsGroupBox.Visible = false;
            // 
            // peopleGroupBox
            // 
            this.peopleGroupBox.Controls.Add(this.ctNameTextBox);
            this.peopleGroupBox.Controls.Add(this.agsLinkSys);
            this.peopleGroupBox.Controls.Add(this.agsTextBoxSys);
            this.peopleGroupBox.Controls.Add(this.userLinkLabelSys);
            this.peopleGroupBox.Controls.Add(this.ctNameLabel);
            this.peopleGroupBox.Controls.Add(this.userAGSBoxSys);
            this.peopleGroupBox.Controls.Add(this.nameTextBoxSys);
            this.peopleGroupBox.Controls.Add(this.nameLabelSys);
            this.peopleGroupBox.Location = new System.Drawing.Point(10, 81);
            this.peopleGroupBox.Name = "peopleGroupBox";
            this.peopleGroupBox.Size = new System.Drawing.Size(368, 72);
            this.peopleGroupBox.TabIndex = 51;
            this.peopleGroupBox.TabStop = false;
            this.peopleGroupBox.Text = "People";
            // 
            // ctNameTextBox
            // 
            this.ctNameTextBox.Location = new System.Drawing.Point(247, 42);
            this.ctNameTextBox.Name = "ctNameTextBox";
            this.ctNameTextBox.ReadOnly = true;
            this.ctNameTextBox.Size = new System.Drawing.Size(108, 20);
            this.ctNameTextBox.TabIndex = 80;
            // 
            // agsLinkSys
            // 
            this.agsLinkSys.AutoSize = true;
            this.agsLinkSys.Location = new System.Drawing.Point(190, 19);
            this.agsLinkSys.Name = "agsLinkSys";
            this.agsLinkSys.Size = new System.Drawing.Size(51, 13);
            this.agsLinkSys.TabIndex = 81;
            this.agsLinkSys.TabStop = true;
            this.agsLinkSys.Text = "CT/AGS:";
            this.agsLinkSys.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.agsLinkSys_LinkClicked);
            // 
            // agsTextBoxSys
            // 
            this.agsTextBoxSys.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.agsTextBoxSys.Location = new System.Drawing.Point(247, 16);
            this.agsTextBoxSys.Name = "agsTextBoxSys";
            this.agsTextBoxSys.Size = new System.Drawing.Size(108, 20);
            this.agsTextBoxSys.TabIndex = 52;
            this.agsTextBoxSys.Leave += new System.EventHandler(this.agsTextBoxSys_Leave);
            // 
            // userLinkLabelSys
            // 
            this.userLinkLabelSys.AutoSize = true;
            this.userLinkLabelSys.Location = new System.Drawing.Point(12, 19);
            this.userLinkLabelSys.Name = "userLinkLabelSys";
            this.userLinkLabelSys.Size = new System.Drawing.Size(32, 13);
            this.userLinkLabelSys.TabIndex = 80;
            this.userLinkLabelSys.TabStop = true;
            this.userLinkLabelSys.Text = "User:";
            this.userLinkLabelSys.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.userLinkLabelSys_LinkClicked);
            // 
            // ctNameLabel
            // 
            this.ctNameLabel.AutoSize = true;
            this.ctNameLabel.Location = new System.Drawing.Point(186, 45);
            this.ctNameLabel.Name = "ctNameLabel";
            this.ctNameLabel.Size = new System.Drawing.Size(55, 13);
            this.ctNameLabel.TabIndex = 34;
            this.ctNameLabel.Text = "CT Name:";
            // 
            // userAGSBoxSys
            // 
            this.userAGSBoxSys.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.userAGSBoxSys.Location = new System.Drawing.Point(53, 16);
            this.userAGSBoxSys.Name = "userAGSBoxSys";
            this.userAGSBoxSys.Size = new System.Drawing.Size(107, 20);
            this.userAGSBoxSys.TabIndex = 51;
            this.userAGSBoxSys.Leave += new System.EventHandler(this.userAGSBoxSys_Leave);
            // 
            // nameTextBoxSys
            // 
            this.nameTextBoxSys.Location = new System.Drawing.Point(52, 42);
            this.nameTextBoxSys.Name = "nameTextBoxSys";
            this.nameTextBoxSys.ReadOnly = true;
            this.nameTextBoxSys.Size = new System.Drawing.Size(108, 20);
            this.nameTextBoxSys.TabIndex = 80;
            // 
            // nameLabelSys
            // 
            this.nameLabelSys.AutoSize = true;
            this.nameLabelSys.Location = new System.Drawing.Point(6, 45);
            this.nameLabelSys.Name = "nameLabelSys";
            this.nameLabelSys.Size = new System.Drawing.Size(38, 13);
            this.nameLabelSys.TabIndex = 41;
            this.nameLabelSys.Text = "Name:";
            // 
            // ticketTextBoxSys
            // 
            this.ticketTextBoxSys.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ticketTextBoxSys.Location = new System.Drawing.Point(63, 26);
            this.ticketTextBoxSys.Name = "ticketTextBoxSys";
            this.ticketTextBoxSys.Size = new System.Drawing.Size(149, 20);
            this.ticketTextBoxSys.TabIndex = 48;
            // 
            // ticketLabelSys
            // 
            this.ticketLabelSys.AutoSize = true;
            this.ticketLabelSys.Location = new System.Drawing.Point(14, 29);
            this.ticketLabelSys.Name = "ticketLabelSys";
            this.ticketLabelSys.Size = new System.Drawing.Size(40, 13);
            this.ticketLabelSys.TabIndex = 47;
            this.ticketLabelSys.Text = "Ticket:";
            // 
            // rtcTextBoxSys
            // 
            this.rtcTextBoxSys.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.rtcTextBoxSys.Location = new System.Drawing.Point(63, 159);
            this.rtcTextBoxSys.Name = "rtcTextBoxSys";
            this.rtcTextBoxSys.Size = new System.Drawing.Size(107, 20);
            this.rtcTextBoxSys.TabIndex = 53;
            // 
            // rtcLabelSys
            // 
            this.rtcLabelSys.AutoSize = true;
            this.rtcLabelSys.Location = new System.Drawing.Point(22, 162);
            this.rtcLabelSys.Name = "rtcLabelSys";
            this.rtcLabelSys.Size = new System.Drawing.Size(32, 13);
            this.rtcLabelSys.TabIndex = 44;
            this.rtcLabelSys.Text = "RTC:";
            // 
            // taskComboBoxSys
            // 
            this.taskComboBoxSys.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.taskComboBoxSys.FormattingEnabled = true;
            this.taskComboBoxSys.Items.AddRange(new object[] {
            "Parts Actual",
            "Putaway",
            "Unassigned"});
            this.taskComboBoxSys.Location = new System.Drawing.Point(257, 159);
            this.taskComboBoxSys.Name = "taskComboBoxSys";
            this.taskComboBoxSys.Size = new System.Drawing.Size(108, 21);
            this.taskComboBoxSys.TabIndex = 54;
            // 
            // taskCatLabelSys
            // 
            this.taskCatLabelSys.AutoSize = true;
            this.taskCatLabelSys.Location = new System.Drawing.Point(172, 162);
            this.taskCatLabelSys.Name = "taskCatLabelSys";
            this.taskCatLabelSys.Size = new System.Drawing.Size(79, 13);
            this.taskCatLabelSys.TabIndex = 42;
            this.taskCatLabelSys.Text = "Task Category:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 188);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 36;
            this.label7.Text = "Date:";
            // 
            // workOrderTextBoxSys
            // 
            this.workOrderTextBoxSys.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.workOrderTextBoxSys.Location = new System.Drawing.Point(257, 27);
            this.workOrderTextBoxSys.Name = "workOrderTextBoxSys";
            this.workOrderTextBoxSys.Size = new System.Drawing.Size(108, 20);
            this.workOrderTextBoxSys.TabIndex = 49;
            // 
            // workOrderLabelSys
            // 
            this.workOrderLabelSys.AutoSize = true;
            this.workOrderLabelSys.Location = new System.Drawing.Point(218, 30);
            this.workOrderLabelSys.Name = "workOrderLabelSys";
            this.workOrderLabelSys.Size = new System.Drawing.Size(33, 13);
            this.workOrderLabelSys.TabIndex = 2;
            this.workOrderLabelSys.Text = "Order";
            // 
            // errorMessageTextBox
            // 
            this.errorMessageTextBox.Location = new System.Drawing.Point(63, 55);
            this.errorMessageTextBox.Name = "errorMessageTextBox";
            this.errorMessageTextBox.Size = new System.Drawing.Size(302, 20);
            this.errorMessageTextBox.TabIndex = 50;
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.Location = new System.Drawing.Point(22, 58);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(32, 13);
            this.errorLabel.TabIndex = 0;
            this.errorLabel.Text = "Error:";
            // 
            // saveLabel
            // 
            this.saveLabel.AutoSize = true;
            this.saveLabel.Enabled = false;
            this.saveLabel.Location = new System.Drawing.Point(280, 9);
            this.saveLabel.Name = "saveLabel";
            this.saveLabel.Size = new System.Drawing.Size(53, 13);
            this.saveLabel.TabIndex = 30;
            this.saveLabel.Text = "Saved at:";
            // 
            // rtbRightClick
            // 
            this.rtbRightClick.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.selectAllToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.rtbRightClick.Name = "rtbRightClick";
            this.rtbRightClick.Size = new System.Drawing.Size(165, 136);
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.undoToolStripMenuItem.Text = "Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.cutToolStripMenuItem.Text = "Cut";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.selectAllToolStripMenuItem.Text = "Select All";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // agsLookup
            // 
            this.agsLookup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agsContextTextBox,
            this.searchToolStripMenuItem});
            this.agsLookup.Name = "agsLookup";
            this.agsLookup.ShowImageMargin = false;
            this.agsLookup.Size = new System.Drawing.Size(150, 51);
            // 
            // agsContextTextBox
            // 
            this.agsContextTextBox.MaxLength = 9;
            this.agsContextTextBox.Name = "agsContextTextBox";
            this.agsContextTextBox.Size = new System.Drawing.Size(114, 23);
            this.agsContextTextBox.ToolTipText = "Paste AGS here";
            this.agsContextTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.agsContextTextBox_KeyPress);
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.ShortcutKeyDisplayString = "Enter";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.searchToolStripMenuItem.Text = "Search";
            this.searchToolStripMenuItem.Click += new System.EventHandler(this.searchToolStripMenuItem_Click);
            // 
            // addressesContextMenu
            // 
            this.addressesContextMenu.BackColor = System.Drawing.SystemColors.Control;
            this.addressesContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyFullAddressToolStripMenuItem,
            this.copyFullAddressFormattedToolStripMenuItem});
            this.addressesContextMenu.Name = "addressesContextMenu";
            this.addressesContextMenu.Size = new System.Drawing.Size(236, 48);
            // 
            // copyFullAddressToolStripMenuItem
            // 
            this.copyFullAddressToolStripMenuItem.Name = "copyFullAddressToolStripMenuItem";
            this.copyFullAddressToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.copyFullAddressToolStripMenuItem.Text = "Copy Full Address (&Line)";
            this.copyFullAddressToolStripMenuItem.Click += new System.EventHandler(this.copyFullAddressLineToolStripMenuItem_Click);
            // 
            // copyFullAddressFormattedToolStripMenuItem
            // 
            this.copyFullAddressFormattedToolStripMenuItem.Name = "copyFullAddressFormattedToolStripMenuItem";
            this.copyFullAddressFormattedToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.copyFullAddressFormattedToolStripMenuItem.Text = "Copy Full Address (&Formatted)";
            this.copyFullAddressFormattedToolStripMenuItem.Click += new System.EventHandler(this.copyFullAddressFormattedToolStripMenuItem_Click);
            // 
            // happyLabel
            // 
            this.happyLabel.AutoSize = true;
            this.happyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.happyLabel.Location = new System.Drawing.Point(199, 210);
            this.happyLabel.Name = "happyLabel";
            this.happyLabel.Size = new System.Drawing.Size(91, 25);
            this.happyLabel.TabIndex = 35;
            this.happyLabel.Text = "ლ(°ٮ°ლ)";
            this.happyLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.happyLabel_MouseDown);
            this.happyLabel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.happyLabel_MouseMove);
            // 
            // secretGroupBox
            // 
            this.secretGroupBox.Controls.Add(this.happyLabel);
            this.secretGroupBox.Location = new System.Drawing.Point(524, 7);
            this.secretGroupBox.Name = "secretGroupBox";
            this.secretGroupBox.Size = new System.Drawing.Size(449, 243);
            this.secretGroupBox.TabIndex = 36;
            this.secretGroupBox.TabStop = false;
            this.secretGroupBox.Visible = false;
            // 
            // hiddenMarioBox
            // 
            this.hiddenMarioBox.BackColor = System.Drawing.SystemColors.Control;
            this.hiddenMarioBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.hiddenMarioBox.Font = new System.Drawing.Font("Arial", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hiddenMarioBox.Location = new System.Drawing.Point(396, 42);
            this.hiddenMarioBox.Name = "hiddenMarioBox";
            this.hiddenMarioBox.Size = new System.Drawing.Size(208, 190);
            this.hiddenMarioBox.TabIndex = 37;
            this.hiddenMarioBox.Text = resources.GetString("hiddenMarioBox.Text");
            this.hiddenMarioBox.Visible = false;
            // 
            // extendedStatusStrip
            // 
            this.extendedStatusStrip.BackColor = System.Drawing.Color.Transparent;
            this.extendedStatusStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.extendedStatusStrip.Location = new System.Drawing.Point(0, 699);
            this.extendedStatusStrip.Name = "extendedStatusStrip";
            this.extendedStatusStrip.Size = new System.Drawing.Size(392, 22);
            this.extendedStatusStrip.TabIndex = 37;
            this.extendedStatusStrip.Timestamps = true;
            // 
            // Main
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(392, 721);
            this.Controls.Add(this.extendedStatusStrip);
            this.Controls.Add(this.miscTab);
            this.Controls.Add(this.hiddenMarioBox);
            this.Controls.Add(this.secretGroupBox);
            this.Controls.Add(this.statusListBox);
            this.Controls.Add(this.level1GroupBox);
            this.Controls.Add(this.saveLabel);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.sysLogsGroupBox);
            this.Controls.Add(this.courierCombo);
            this.Controls.Add(this.courierLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(408, 600);
            this.Name = "Main";
            this.Text = " Gumshoe SPLG - v2.9.1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.ResizeEnd += new System.EventHandler(this.Main_ResizeEnd);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Main_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.Main_DragEnter);
            this.DragLeave += new System.EventHandler(this.Main_DragLeave);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.spareBox.ResumeLayout(false);
            this.spareBox.PerformLayout();
            this.faultyBox.ResumeLayout(false);
            this.faultyBox.PerformLayout();
            this.agsBoxContextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.personDetailsDGV)).EndInit();
            this.level1GroupBox.ResumeLayout(false);
            this.level1GroupBox.PerformLayout();
            this.groupBoxMMGGeneral.ResumeLayout(false);
            this.groupBoxMMGGeneral.PerformLayout();
            this.miscTab.ResumeLayout(false);
            this.tabNotes.ResumeLayout(false);
            this.tabNotes.PerformLayout();
            this.tabDataSearch.ResumeLayout(false);
            this.tabDataSearch.PerformLayout();
            this.tabControlData.ResumeLayout(false);
            this.tabLocations.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocations)).EndInit();
            this.tabKits.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kitGrid)).EndInit();
            this.tabAddresses.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.addressDGV)).EndInit();
            this.tabConsumables.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.consumableDGV)).EndInit();
            this.personDetails.ResumeLayout(false);
            this.personDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.personSearchDGV)).EndInit();
            this.tabMisc.ResumeLayout(false);
            this.tabControlMisc.ResumeLayout(false);
            this.sysLogsGroupBox.ResumeLayout(false);
            this.sysLogsGroupBox.PerformLayout();
            this.peopleGroupBox.ResumeLayout(false);
            this.peopleGroupBox.PerformLayout();
            this.rtbRightClick.ResumeLayout(false);
            this.agsLookup.ResumeLayout(false);
            this.agsLookup.PerformLayout();
            this.addressesContextMenu.ResumeLayout(false);
            this.secretGroupBox.ResumeLayout(false);
            this.secretGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.Label tasLabel;
        private System.Windows.Forms.Label workOrderLabel;
        private System.Windows.Forms.Label faultySerialLabel;
        private System.Windows.Forms.Label faultyMPNLabel;
        private System.Windows.Forms.Label spareSerialLabel;
        private System.Windows.Forms.Label spareMPNLabel;
        private System.Windows.Forms.GroupBox spareBox;
        private System.Windows.Forms.GroupBox faultyBox;
        private System.Windows.Forms.Label nodeLabel;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historyToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.TabControl miscTab;
        private System.Windows.Forms.TabPage tabNotes;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.Label courierLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabDataSearch;
        private System.Windows.Forms.TextBox textBoxSearch;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgvLocations;
        public System.Windows.Forms.TextBox tasTextBox;
        public System.Windows.Forms.TextBox workOrderTextBox;
        public System.Windows.Forms.TextBox faultySerialBox;
        public System.Windows.Forms.TextBox faultyMPNBox;
        public System.Windows.Forms.TextBox spareSerialBox;
        public System.Windows.Forms.TextBox spareMPNBox;
        public System.Windows.Forms.TextBox ctTextBox;
        public System.Windows.Forms.TextBox nodeTextBox;
        public System.Windows.Forms.TextBox pudoTextBox;
        public System.Windows.Forms.ComboBox ticketTypeCombo;
        public System.Windows.Forms.ComboBox courierCombo;
        public System.Windows.Forms.TextBox connoteTextBox;
        public System.Windows.Forms.Label saveLabel;
        public System.Windows.Forms.MenuStrip menuStrip1;
        public System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.LinkLabel agsLink;
        private System.Windows.Forms.ToolStripMenuItem alwaysOnTopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hotkeysToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem templateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button additionalAddbutton;
        private System.Windows.Forms.RadioButton additionalSpareradio;
        private System.Windows.Forms.RadioButton additionalFaultradio;
        private System.Windows.Forms.Label additionalSeriallabel;
        private System.Windows.Forms.Label additionalMPNlabel;
        private System.Windows.Forms.TextBox additionalSerial;
        private System.Windows.Forms.TextBox additionalMPN;
        private System.Windows.Forms.Label hotkeyLabel;
        public System.Windows.Forms.ListBox statusListBox;
        private RichTextBoxExtended.RichTextBoxExtended rtbExtended;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.CheckBox hotkeyCheckBox;
        private System.Windows.Forms.ToolStripMenuItem wikiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editTemplateToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip rtbRightClick;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeLogToolStripMenuItem;
        private System.Windows.Forms.TabPage personDetails;
        private System.Windows.Forms.DataGridView personDetailsDGV;
        private System.Windows.Forms.DataGridView personSearchDGV;
        private System.Windows.Forms.Button searchPeopleButton;
        private System.Windows.Forms.TextBox surNameTextBox;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.Label surNameLabel;
        private System.Windows.Forms.Label firstNameLabel;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem1;
        public System.Windows.Forms.GroupBox level1GroupBox;
        private System.Windows.Forms.DateTimePicker dateTimePickerSys;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox ctNameTextBox;
        private System.Windows.Forms.Label ctNameLabel;
        private System.Windows.Forms.TextBox agsTextBoxSys;
        private System.Windows.Forms.LinkLabel agsLinkSys;
        private System.Windows.Forms.TextBox workOrderTextBoxSys;
        private System.Windows.Forms.Label workOrderLabelSys;
        private System.Windows.Forms.TextBox errorMessageTextBox;
        private System.Windows.Forms.Label errorLabel;
        private System.Windows.Forms.LinkLabel userLinkLabelSys;
        private System.Windows.Forms.TextBox rtcTextBoxSys;
        private System.Windows.Forms.Label rtcLabelSys;
        private System.Windows.Forms.ComboBox taskComboBoxSys;
        private System.Windows.Forms.Label taskCatLabelSys;
        private System.Windows.Forms.Label nameLabelSys;
        private System.Windows.Forms.TextBox nameTextBoxSys;
        private System.Windows.Forms.TextBox userAGSBoxSys;
        public System.Windows.Forms.GroupBox sysLogsGroupBox;
        private System.Windows.Forms.TextBox ticketTextBoxSys;
        private System.Windows.Forms.Label ticketLabelSys;
        public System.Windows.Forms.Label hotkeysSysLog;
        private System.Windows.Forms.GroupBox peopleGroupBox;
        private System.Windows.Forms.ToolStripMenuItem sendToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripTextBox importToolStripTextBox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem checkForUpdatesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem errorLogToolStripMenuItem;
        private System.Windows.Forms.ComboBox locationDropDown;
        private System.Windows.Forms.TextBox postCodeTextBox;
        private System.Windows.Forms.TabPage tabMisc;
        private System.Windows.Forms.Label judgementLabel;
        private System.Windows.Forms.ContextMenuStrip agsLookup;
        private System.Windows.Forms.ToolStripTextBox agsContextTextBox;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendToolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip agsBoxContextMenu;
        private System.Windows.Forms.ToolStripMenuItem Undo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem Cut;
        private System.Windows.Forms.ToolStripMenuItem Copy;
        private System.Windows.Forms.ToolStripMenuItem Paste;
        private System.Windows.Forms.ToolStripMenuItem Delete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem SelectAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem NewTicket;
        private System.Windows.Forms.ListView agsListView;
        private System.Windows.Forms.ColumnHeader nameColumn;
        private System.Windows.Forms.ColumnHeader agsColumn;
        private System.Windows.Forms.ContextMenuStrip addressesContextMenu;
        private System.Windows.Forms.ToolStripMenuItem copyFullAddressToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyFullAddressFormattedToolStripMenuItem;
        private System.Windows.Forms.TextBox rtcTextBox;
        private System.Windows.Forms.LinkLabel newTicketLinkLabel;
        private System.Windows.Forms.Label happyLabel;
        private System.Windows.Forms.GroupBox secretGroupBox;
        private System.Windows.Forms.LinkLabel rtcLinkLabel;
        private System.Windows.Forms.RichTextBox hiddenMarioBox;
        private System.Windows.Forms.ToolStripMenuItem toolStripDragHere;
        private System.Windows.Forms.GroupBox groupBoxMMGGeneral;
        private System.Windows.Forms.TabControl tabControlMisc;
        private System.Windows.Forms.TabPage tabAdder;
        private System.Windows.Forms.LinkLabel linkShowResults;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn locationSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn phoneSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn employeeSearch;
        private System.Windows.Forms.ToolStripMenuItem addonsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem complexConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rFSTrackerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem issueTrackerToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn plntColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn slocColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn storageLocationColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nodeSearchColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn E;
        private System.Windows.Forms.DataGridViewTextBoxColumn D;
        private System.Windows.Forms.DataGridViewTextBoxColumn F;
        private System.Windows.Forms.DataGridViewTextBoxColumn latitudeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn longitudeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn locationTypeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn searchTerm2;
        private System.Windows.Forms.LinkLabel linkPod;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueColumn;
        private System.Windows.Forms.ComboBox comboDataSelect;
        private System.Windows.Forms.Label labelClearSearch;
        private System.Windows.Forms.TabControl tabControlData;
        private System.Windows.Forms.TabPage tabLocations;
        private System.Windows.Forms.TabPage tabKits;
        private System.Windows.Forms.TabPage tabAddresses;
        private System.Windows.Forms.TabPage tabConsumables;
        private System.Windows.Forms.DataGridView kitGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn kitColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn CC;
        private System.Windows.Forms.DataGridViewTextBoxColumn nodeColumn;
        private System.Windows.Forms.DataGridView addressDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlntCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn SLoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn addrNoHidden;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn HouseNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn StreetCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn CityCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn PostlCodeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn TranspZoneHidden;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rg;
        private System.Windows.Forms.DataGridViewTextBoxColumn PuDoCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn name4col;
        private System.Windows.Forms.DataGridViewTextBoxColumn cOName;
        private System.Windows.Forms.DataGridView consumableDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.ToolStripMenuItem disasterRecoveryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem puDoAuditsToolStripMenuItem;
        private System.Windows.Forms.TextBox textMMGReservation;
        private System.Windows.Forms.Label labelReservationNumber;
        internal GumshoePuDo.ExtendedStatusStrip extendedStatusStrip;
    }
}

