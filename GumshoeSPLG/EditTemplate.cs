﻿using System.IO;
using System.Windows.Forms;

namespace GumshoeSPLG
{
    public partial class EditTemplate : Form
    {
        private readonly Main _main;
        public EditTemplate(Main main)
        {
            InitializeComponent();
            _main = main;
            Icon = main.Icon;
            Width = main.Width;
            LoadTemplate();
        }

        private void LoadTemplate()
        {
            var templateFile = File.ReadAllText(Properties.Settings.Default.SavePath + @"\GumshoeSPLG\Template.txt");
            txtTemplate.Text = templateFile;
            txtTemplate.Select(0, 0);
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            File.WriteAllText(Properties.Settings.Default.SavePath + @"\GumshoeSPLG\Template.txt", txtTemplate.Text);
            _main.LoadTemplate();
            Dispose();
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            Dispose();
        }
    }
}
